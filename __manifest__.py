{
    'name': 'SmartSens Analytics',
    'version': '1.0',
    'summary': 'SmartSens Analytics Mangement Software',
    'sequence': -100, # Wenn auf -100 eingestellt ist, wird die App auf den ersten Platz, ganz vorne unabhängig von dem Namen im Appstore angezeigt.
    'description': "SmartSens Analytics Software",
    'category': 'Productivity',
    'website': 'http://www.ohs-engineering.de',
    'depends': ['board','base'],
    'data': ['security/ir.model.access.csv',
             'views/analytics.xml',
             #'views/Data-Store.xml',
             'reports/SmartSensReport.xml',
             'reports/report.xml',
             'reports/header.xml',
              #'views/tree_view_asset.xml',
             #'static/src/xml/fileUploadButton.xml',
            'views/SmartSensAnalytics_Assets.xml'
             ],
    'css': ['static/src/css/SmartSensAnalyticsCustomCSS.css'],
    'js': ['static/src/js/SmartSensAnalyticsCustomCSS.js'],
    'demo': [],
    'qweb': [],#'static/src/xml/tree_view_button.xml'],
    'installable': True,
    'application': True,
    'auto_install': False, # wenn auf True gestellt, wird das addon automatisch installiert
    'license': 'LGPL-3',
}

# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
