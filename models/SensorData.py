from odoo import api, fields
from odoo import models

class SensorData(models.Model):
    _name ="smartsens.sensordata.stream"
    _description = "Sensor-Datenstream"
    Launch_id = fields.Many2one("smartsense.launche",string="Launch-ID", ondelete="cascade")
    timeStamp = fields.Integer(string="Milliseconds")
    Tfm_RowID = fields.Integer(string="ID Aufzeichnungspunkts", help="Die ID wird verwendet um daraus geordnet Daten zu erstellen")
    TFM_ID = fields.Integer(string="TFM_ID")
    Altitude = fields.Integer(string="Altitude")
    Cons_Force = fields.Integer(string="TFD_Cons_Force")
    Error_State = fields.Integer(string="Error_State")
    TargetForce = fields.Integer(string="Target-Force")
    PacketsLost = fields.Integer(string="PacketsLost")
    RSSI = fields.Integer(string="RSSI")