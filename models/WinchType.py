from odoo import api, fields, models

class WinchType(models.Model):
    _name = "smartsenese.winchtype"
    _description = "Type des verwendeten Seilzuges"
    name = fields.Char(string="Winchtype", help="Welche Seilwinde wird verwendet?")