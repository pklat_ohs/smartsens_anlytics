import psycopg2
from datetime import datetime ,timedelta
import matplotlib.pyplot as plt
import numpy as np

def connect():
    """ Connect to the PostgreSQL database server """
    conn = None
    try:
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(user="odoo14Grp",
                                password="123123",
                                host="localhost",
                                port="5434",
                                database="odoo")
        cur = conn.cursor()
        print('PostgreSQL database version:')
        cur.execute('SELECT version()')
        db_version = cur.fetchone() # display the PostgreSQL database server version
        print(db_version)
        cur.close() # close the communication with the PostgreSQL
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')

import random
random.seed()

def DataGenerator():
    return random.randrange(0, 5000)

def flughoehe():
    return random.randrange(3500, 5000)

altitudeData= []
def createInsert():
    # public.smartsense_sensordata_stream
    InsertQuerry = """INSERT INTO public.smartsen_sensordata_stream(
    "timeStamp", "TargetForce", "RSSI", "PacketsLost", create_uid, create_date, write_uid, write_date, forcce, "Altitude","Temperatur")
    	VALUES (%s , %s,%s, %s, %s, %s, %s, %s, %s, %s,%s);"""

    InsertQuery= """INSERT INTO public.smartsens_sensordata_stream(
	"StreamID", "timeStamp", "Altitude", forcce, "PacketsLost", "TargetForce", "Temperatur", "RSSI", create_uid, create_date, write_uid, write_date)
	VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);    
    """
    try:
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(user="odoo14Grp",
                                password="123123",
                                host="localhost",
                                port="5434",
                                database="odoo")
        cur = conn.cursor()
        cur.execute("""DELETE FROM public.smartsens_sensordata_stream WHERE True;""")

        currentTime = datetime.fromisoformat(datetime.utcnow().isoformat(sep=' ', timespec='milliseconds'))
        print(currentTime)
        
        timedelta200M = timedelta(milliseconds=200)
        tempTime = ""
        for _ in range(1500): # 1500 Datensätze für 5min SensorDaten bei 5hz
            if tempTime == "":
                tempTime = currentTime
                # INSERT INTO public.smartsense_sensordata_stream(
                # 	"StreamID", "timeStamp", "Altitude", forcce, "PacketsLost", "TargetForce", "Temperatur", "RSSI", create_uid, create_date, write_uid, write_date)
                # 	VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);
                record_to_insert = \
                ("2", tempTime, flughoehe(), 34, random.randint(99, 455), 7, 2, 2, 4, tempTime, 1, tempTime)
                cur.execute(InsertQuery, record_to_insert)
            else:
                tempTime += timedelta200M
                record_to_insert = (
                "2", tempTime, flughoehe(), 34, random.randint(99, 455), 7, 2, 2, 4, tempTime, 1, tempTime)
                cur.execute(InsertQuery, record_to_insert)
        conn.commit()  # save the changes
        cur.close()  # close the connetions
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')

def get_altitude():
    """ query data from the vendors table """
    try:
        conn = psycopg2.connect(user="odoo14Grp",
                                password="123123",
                                host="localhost",
                                port="5434",
                                database="odoo")
        cur = conn.cursor()
        cur.execute(""" Select "Altitude" FROM public.smartsens_sensordata_stream;""")
        print("The number of parts: ", cur.rowcount)
        row = cur.fetchone()
        while row is not None:
            print(row[0])
            altitudeData.append(row[0])
            row = cur.fetchone()
        print(altitudeData)
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

def AltitudePlot():
    plt.plot(altitudeData)
    xmin, xmax, ymin, ymax = 0, 1500, 2, 6000
    plt.axis([xmin, xmax, ymin, ymax])
    plt.xlabel('Time')
    plt.ylabel('Flughöhe')
    plt.show()

if __name__ == '__main__':
    createInsert()
    get_altitude()
    AltitudePlot()