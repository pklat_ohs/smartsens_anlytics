from ftplib import FTP
import os, sys, os.path

# https://docs.python.org/3/library/ftplib.html
# TODO: get more Metadata from the FTP-Files

class RemoteImport:
    def __init__(self, username, password, host, work_dir):
        self.username = username
        self.password = password
        self.work_dir = work_dir
        self.ftp = FTP(host)

    def ftpFileNames(self):
        os.chdir(self.work_dir)
        print('Logging in FTP.')
        self.ftp.login(self.username, self.password)
        filenames = self.ftp.nlst()  # get filenames within the directory
        print(filenames)
        return filenames  # Durch iterieren und copy FTP_file() aufrufen!

    def copyFTP_file(self, filename):
        local_filename = os.path.join(self.work_dir, filename)
        file = open(local_filename, 'wb')
        self.ftp.retrbinary('RETR ' + filename, file.write)
        file.close()

    def closeCon(self):
        self.ftp.quit()
