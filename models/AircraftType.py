from odoo import fields, api
from odoo import models

class AircraftType(models.Model):
    _name = "aircraft.type" # !!! bei der Modellbezeichnung dürfen keine Großen buchstaben verwendet werden !!!
    _description = "Typen von Segel-Flugzeugen"
    name = fields.Char(string="Type Name", help="Welchen Namen trägt das Flugzeug")
    emptyAircraftWeight = fields.Float(string="Empty Aircraft weight",
                                       help="Gewicht des Flugzeugs ohne Besatzung Ladung")