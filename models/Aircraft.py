from odoo import api, fields, models

class aircraft(models.Model):
    _name = "smartsense.aircraft"
    _description = "Aircraft"
    name = fields.Char(name="Name",default="Airfieldname")
    modelbezeichnung = fields.Char(string="Model-bezeichnung")
    tragweitenflaeche = fields.Float(string="Tragweiten-fläche")
    #aircraftype = fields.Many2one('aircraft.type', string='Winch Operator')
    emptyAircraftWeight = fields.Float(string="Empty Aircraft weight", help="Gewicht des Flugzeugs ohne Besatzung Ladung")