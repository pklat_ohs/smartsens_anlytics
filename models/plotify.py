#!./venv/bin/python
# -*- coding: utf-8 -*-

"""
    Created on 19th October 2021
    Copyright by Vadim Grebnev

"""
__version__ = "v1.0"

from PIL import Image
import datetime as dt
import datetime
import matplotlib.pyplot as plt

plt.switch_backend('agg')
import matplotlib.patches as mpatches

import numpy as np
import os
import io
from time import sleep as delay
import base64


# %%
class Plotify:
    """
    Class for plotting of SmartSens datasets for report generation.
    The axis limits and ticks scales automatically.

    """

    def __init__(self, fig: object = None, ax: object = None,
                 fig_size_inches: tuple = (19.80, 10.80), debug: bool = False) -> None:

        """
        Class / instance initialization.

        :param fig: figure object from matplotlib module
        :param ax: axis object from matplotlib module
        :param fi_size_inches: figure size in inches,
            inches=(19.80, 10.80) -> pixel=(1980x1080)
        :param debug: boolean parameter for enable / disable the debug mode

        :return: None

        """
        # figure & axis configurations
        plt.close('all')  # close all figure windows
        plt.rc('xtick', labelsize=12)  # fontsize of tick labels of x axis
        plt.rc('ytick', labelsize=12)  # fontsize of tick labels of y axis
        plt.rc('axes', labelsize=18)  # fontsize of text labels of both axis
        plt.rc('axes', titlesize=24)  # fontsize of the title text
        plt.rc('lines', lw=1.0)  # default linewidth
        plt.rc('legend', fontsize=16)  # fontsize of the legend

        # set / create figure and axis
        if fig is None and ax is None:
            self.fig = plt.figure("Plotify " + __version__, figsize=(20, 10))
            self.set_figsize(figure=self.fig, size=fig_size_inches)
            self.ax = self.fig.add_subplot(projection='rectilinear')
        elif fig is None and isinstance(ax, object):
            self.fig = None
            self.ax = ax
        elif isinstance(fig, object) and ax is None:
            self.fig = fig
            try:
                self.set_figsize(figure=self.fig, size=fig_size_inches)
                self.ax = fig.add_subplot(111, projection='rectilinear')
            except Exception as e:
                print(e)
        elif isinstance(fig, object) and isinstance(ax, object):
            self.fig = fig
            self.set_figsize(figure=self.fig, size=fig_size_inches)
            self.ax = ax
        else:
            print("Unexpected error.")

        # initialize extrema lists
        self.minimas, self.maximas, self.durations = [], [], []

        # set some standarts
        self.set_gridlw(grid_linewidth=0.5)
        self.dtformat = '%Y-%m-%d %H:%M:%S.%s'
        self.figformats = ['png', 'svg', 'pdf']
        # self.figformats = ['svg']
        self.set_debug(debug=debug)

    ##########################################################################

    def set_xtick_labelsize(self, fontsize: float = 12.) -> None:
        """Set fontsize of tick labels of x axis in pt."""
        if isinstance(fontsize, float):
            plt.rc('xtick', labelsize=fontsize)

    def set_ytick_labelsize(self, fontsize: float = 12.) -> None:
        """Set fontsize of tick labels of y axis in pt."""
        if isinstance(fontsize, float):
            plt.rc('ytick', labelsize=fontsize)

    def set_axis_labelsize(self, fontsize: float = 18.) -> None:
        """Set fontsize of text labels of both axis in pt."""
        if isinstance(fontsize, float):
            plt.rc('axes', labelsize=fontsize)

    def set_titlesize(self, fontsize: float = 24.) -> None:
        """Set fontsize of the title text in pt."""
        if isinstance(fontsize, float):
            plt.rc('axes', titlesize=fontsize)

    def set_linewidth(self, linewidth: float = 1.) -> None:
        """Set default linewidth."""
        if isinstance(linewidth, float):
            plt.rc('lines', lw=linewidth)

    def set_legend_labelsize(self, fontsize: float = 16) -> None:
        """Set fontsize of the legend labels in pt."""
        if isinstance(fontsize, float):
            plt.rc('legend', fontsize=fontsize)

    def set_figsize(self, figure: object = None,
                    size: tuple = (19.80, 10.80)) -> None:
        """
        Setting of the figsize in inches. Default is a size of width = 19.80
        and height = 10.80, what causes a picture size of 1980x1080 pixel.

        :param figure: figure object from matplotlib module
        :param size: tuple, desired size for figure in inches
        :return: None

        """
        if figure is None and isinstance(self.fig, object):
            self.fig.set_size_inches(size)
        elif figure is not None and isinstance(figure, object):
            figure.set_size_inches(size)
        else:
            print("Unexpected error.")

    def set_gridlw(self, grid_linewidth: float = 0.5) -> None:
        """
        Setting of grid linewidth. After class init the default value is set
        by default to 1/2.

        :param grid_linewidth: float, grid linewidth
        :return: None

        """
        if isinstance(grid_linewidth, (float, int)):
            self.gridlw = grid_linewidth
        else:
            pass  # error msg

    def add_figformats(self, formats: list) -> None:
        """
        Adds valid filename extensions (suffix).

        :param formats: List with valid filename extensions as type str.
        :return: None

        """
        if isinstance(formats, list):
            self.figformats = [*self.figformats, *formats]
        elif isinstance(formats, str):
            self.figformats.append(formats)
        else:
            print("Unexpected error.")

    def set_debug(self, debug: bool = True) -> None:
        """
        Toggles state of debug mode.

        :param debug: bool, enables / disables the debug mode.
        :return: None

        """
        if debug:
            self.debug = True
        else:
            self.debug = False

    ##########################################################################

    def _prep_xarray(self, x: list) -> list:
        """
        Creates an array x for abscissica axis from list of datetime objects
        wich contains time values > 0s. The time unit is seconds [s].

        :param x: List with datetime objects.
        :return: NumPy array object with time values in seconds [s].

        """
        xarray = []
        for i in range(len(x)):
            # print(x[i])
            # print(x[i]-x[0])
            # print("TotalSeconds: ")
            # print((x[i]-x[0]).total_seconds())
            xarray.append((x[i] - x[0]).total_seconds())
        return np.array(xarray).astype(float)

    def maf(self, y: float, N: int = 8, mode: str = 'same') -> float:
        """
        MAF: N-point Moving Average Filter

        Parameters
        ----------
        y : float
            Value to be filtered.
        N : int, optional, the default is 8.
            Number of values to be averaged.
        mode : str, optional, the default is 'same'.
            NumPy convolution mode. Valid modes are 'full', 'same' and 'valid'.

        Returns
        -------
        float
            Filtered data set.

        """
        from numpy import array, convolve, ones
        return convolve(array(y).astype(float), ones(N) / N, mode=mode)

    def plot(self, label, x: list, y: list, ylabel: str, ax: object = None, maf: bool = False, xtype: str = 'dt',
             *args, **kwargs) -> None:
        """
        Main method for combined plots of multiple datasets. Scales the axis
        ticks and limits automatically.

        :param x: List or array object with data for abscissica axis.
        :param y: List or array object with data for ordinate axis.
        :param ylabel: str, discribtion of the y-data values
        :param ax: axis object from matplotlib module
        :param label: list of labels for the Plot and subplots
        :return: None

        """
        # check axis object
        if ax is None:
            ax = self.ax
        elif ax is not None and not isinstance(ax, object):
            pass  # err msg


        # save extrema
        self.minimas.append(np.min(y))
        self.maximas.append(np.max(y))

        # Original
        self.durations.append((x[-1] - x[0]).total_seconds())
        # self.durations.append((x[-1]-x[0]))

        # plot dataset
        if not isinstance(xtype, str):
            print("Errormsg")
            return
        else:
            xtype = xtype.lower()
        if xtype in ['dt', 'datetime', 'obj', 'object']:
            x = self._prep_xarray(x=x)
        elif xtype not in ['s', 'sec', 'second', 'seconds', 't', 'time']:
            print("Errormsg")
            return

        if isinstance(y, list):
            y = np.array(y)
            if y.shape == (1, len(y)):
                y = y.T
            print(y.shape, len(y))


        ax.plot(x, y, *args, **kwargs)

        #ax.legend("Some Legend")


        # set / autoscale x, y ticks
        xstep = 5
        duration = np.max(self.durations)
        k, xsteps = 2, duration / xstep
        while xsteps > 16:
            xstep = k * 5
            xsteps = duration / xstep
            k += 1
        xticks = np.arange(0, xstep * xsteps + xstep, xstep).astype(int).tolist()
        ax.set_xticks(xticks)
        xticklabels = [str(xtick) for xtick in xticks]
        ax.set_xticklabels(xticklabels)

        minima, maxima = np.min(self.minimas), np.max(self.maximas)
        ystep, dy = 1000, maxima - minima
        ysteps = dy / ystep
        while ysteps < 5:
            if ystep == 0.0:
                ystep.isnull()

            print(ystep)
            ystep = ystep / 10
            ysteps = dy / int(ystep)
        # print(ystep)

        # ymin, ymax = int(maxima/ystep)*ystep, int(minima/ystep)*ystep # Original
        # ymin, ymax = (maxima/ystep).astype(int)*(ystep).astype(int), (minima/ystep).astype(int)*ystep
        ymin, ymax = int(maxima / ystep) * ystep, int(minima / ystep) * ystep

        while ymin > minima:
            ymin -= ystep
        ymin -= ystep
        while ymax < maxima:
            ymax += ystep
        ymax += ystep

        yticks = np.append(
            np.arange(0, ymin - ystep, -ystep).astype(int),
            np.arange(0, ymax + ystep, ystep).astype(int)
        ).tolist()
        ax.set_yticks(yticks)
        yticklabels = [str(ytick) for ytick in yticks]
        ax.set_yticklabels(yticklabels)

        # set / refresh limits
        ax.set_xlim(0, duration)
        ax.set_ylim(ymin, ymax)

        # set x,y labels
        ax.set_xlabel("Time t in seconds [s]")
        ax.set_ylabel(ylabel)

        # grid, legend, scale
        ax.grid(linewidth=self.gridlw)
        ax.legend(loc='best', fancybox=True)
        if self.debug:
            plt.tight_layout(pad=5.0)
        else:
            plt.tight_layout(pad=0.0)

        # save & return
        self.ax = ax
        return ax

    def fig2buff(self, fig: object) -> object:
        """Convert a Matplotlib figure to a PIL Image and return it"""
        import io
        buffer = io.BytesIO()
        fig.savefig(buffer)
        buffer.seek(0)
        return buffer

    def fig2img(self, fig: object) -> object:
        """Convert a Matplotlib figure to a PIL Image and return it"""
        import io
        buffer = io.BytesIO()
        # buffer = io.StringIO()
        img = fig.savefig(buffer)
        buffer.seek(0)
        img = Image.open(buffer)
        return img

    def _check_filepath(self, filepath: str) -> None:
        """
        Checks if filepath is correct and if file already exists.
        If file already exist the savefig method would not overwrite it.
        If directory does not exist, a new one would be created.

        :param filepath: Absolute or relative filepath with filename extension.
        :return: None

        """
        dirpath = filepath.replace(filepath.split('/', -1)[-1], '')
        if os.path.isfile(filepath):
            print("File " + filepath + " already exist. Do not overwrite.")
            return False
        elif not os.path.isdir(dirpath):
            print("Directory does not exist. Creating " + dirpath + " ...")
            os.mkdir(dirpath)
            return True
        elif os.path.exists(dirpath):
            return True
        else:
            return False

    def _check_type(self, filepath: str) -> bool:
        """
        Checks the filename extension (suffix) and validates the format.

        :param filepath: Absolute or relative filepath with filename extension.
        :return: None

        """
        suffix = filepath.split('.', -1)[-1].lower()
        if suffix in self.figformats:
            return True
        else:
            return False

    def savefig(self, filepath: str, figure: object = None) -> None:
        """
        Saves the plotted intern figure (self.fig) or a given figure in desired
        filepath. Validates and create directory if not exist. If file already
        exists, it would not be overwritten.

        :param filepath: Absolute or relative filepath with filename extension.
        :param figure: figure object from matplotlib module
        :return: None

        """
        # check figure object
        if figure is not None and isinstance(figure, object):
            figure.set_size_inches(self.figsize)
            figure.tight_layout(pad=0.0)
        elif figure is None and \
                self.fig is not None and isinstance(self.fig, object):
            figure = self.fig
        else:
            print("Unexpected error.")
        # check filepath and file format, save figure
        if self._check_filepath(filepath) and self._check_type(filepath):
            figure.savefig(filepath)

    def pass_buff_fig(self, figure: object) -> object:
        """
        Comments

        """
        buffer = io.BytesIO()
        figure.savefig(buffer, format="png")
        buffer.seek(0)
        return base64.encodebytes(buffer.getvalue())


# %% Section for demonstrative and debug purposes
if __name__ == '__main__':
    # generate lists with random datetime objects
    T = 60 * 1e3  # periode of fligth time in milliseconds
    mu, sigma = 200, 20  #
    t1_steps = np.random.normal(mu, sigma, int(T / mu))
    t2_steps = np.random.normal(mu, sigma, int(T / mu))
    t3_steps = np.random.normal(mu, sigma, int(T / mu))
    t1_tmp = dt.datetime.now()
    delay(np.random.rand())
    t2_tmp = dt.datetime.now()
    delay(np.random.rand())
    t3_tmp = dt.datetime.now()
    t1, t2, t3 = [t1_tmp], [t2_tmp], [t3_tmp]
    for timedelta in t1_steps:
        delta = dt.timedelta(milliseconds=timedelta)
        t1_tmp += delta
        t1.append(t1_tmp)
    for timedelta in t2_steps:
        delta = dt.timedelta(milliseconds=timedelta)
        t2_tmp += delta
        t2.append(t2_tmp)
    for timedelta in t3_steps:
        delta = dt.timedelta(milliseconds=timedelta)
        t3_tmp += delta
        t3.append(t3_tmp)

    # generate random datasets for ordinate axis
    mu, sigma = 300, 50
    y1 = mu * np.sin(np.linspace(0, 1 * np.pi, len(t1))) + \
         np.sqrt(sigma ** 2) * np.random.rand(len(t1))
    y2 = (100.0 + mu) * np.cos(np.linspace(0, 1 * np.pi, len(t2))) + \
         np.sqrt(2 * sigma ** 2) * np.random.rand(len(t2))
    y3 = (100.0 + mu) * np.cos(np.linspace(0, 1 * np.pi, len(t2))) + \
         np.sqrt(sigma ** 2) * np.random.rand(len(t2))

    # plot demonstration with random generated datasets
    p = Plotify(debug=True)
    # t = [t1, t2, t3]
    t = [datetime.datetime(2021, 10, 20, 12, 55, 4, 92000), datetime.datetime(2021, 10, 20, 12, 55, 4, 292000),
         datetime.datetime(2021, 10, 20, 12, 55, 4, 492000), datetime.datetime(2021, 10, 20, 12, 55, 4, 692000),
         datetime.datetime(2021, 10, 20, 12, 55, 4, 892000), datetime.datetime(2021, 10, 20, 12, 55, 5, 92000),
         datetime.datetime(2021, 10, 20, 12, 55, 5, 292000), datetime.datetime(2021, 10, 20, 12, 55, 5, 492000),
         datetime.datetime(2021, 10, 20, 12, 55, 5, 692000), datetime.datetime(2021, 10, 20, 12, 55, 5, 892000),
         datetime.datetime(2021, 10, 20, 12, 55, 6, 92000), datetime.datetime(2021, 10, 20, 12, 55, 6, 292000),
         datetime.datetime(2021, 10, 20, 12, 55, 6, 492000), datetime.datetime(2021, 10, 20, 12, 55, 6, 692000),
         datetime.datetime(2021, 10, 20, 12, 55, 6, 892000), datetime.datetime(2021, 10, 20, 12, 55, 7, 92000),
         datetime.datetime(2021, 10, 20, 12, 55, 7, 292000), datetime.datetime(2021, 10, 20, 12, 55, 7, 492000),
         datetime.datetime(2021, 10, 20, 12, 55, 7, 692000), datetime.datetime(2021, 10, 20, 12, 55, 7, 892000),
         datetime.datetime(2021, 10, 20, 12, 55, 8, 92000), datetime.datetime(2021, 10, 20, 12, 55, 8, 292000),
         datetime.datetime(2021, 10, 20, 12, 55, 8, 492000), datetime.datetime(2021, 10, 20, 12, 55, 8, 692000),
         datetime.datetime(2021, 10, 20, 12, 55, 8, 892000), datetime.datetime(2021, 10, 20, 12, 55, 9, 92000),
         datetime.datetime(2021, 10, 20, 12, 55, 9, 292000), datetime.datetime(2021, 10, 20, 12, 55, 9, 492000),
         datetime.datetime(2021, 10, 20, 12, 55, 9, 692000), datetime.datetime(2021, 10, 20, 12, 55, 9, 892000),
         datetime.datetime(2021, 10, 20, 12, 55, 10, 92000), datetime.datetime(2021, 10, 20, 12, 55, 10, 292000),
         datetime.datetime(2021, 10, 20, 12, 55, 10, 492000), datetime.datetime(2021, 10, 20, 12, 55, 10, 692000),
         datetime.datetime(2021, 10, 20, 12, 55, 10, 892000), datetime.datetime(2021, 10, 20, 12, 55, 11, 92000),
         datetime.datetime(2021, 10, 20, 12, 55, 11, 292000), datetime.datetime(2021, 10, 20, 12, 55, 11, 492000),
         datetime.datetime(2021, 10, 20, 12, 55, 11, 692000), datetime.datetime(2021, 10, 20, 12, 55, 11, 892000),
         datetime.datetime(2021, 10, 20, 12, 55, 12, 92000), datetime.datetime(2021, 10, 20, 12, 55, 12, 292000),
         datetime.datetime(2021, 10, 20, 12, 55, 12, 492000), datetime.datetime(2021, 10, 20, 12, 55, 12, 692000),
         datetime.datetime(2021, 10, 20, 12, 55, 12, 892000), datetime.datetime(2021, 10, 20, 12, 55, 13, 92000),
         datetime.datetime(2021, 10, 20, 12, 55, 13, 292000), datetime.datetime(2021, 10, 20, 12, 55, 13, 492000),
         datetime.datetime(2021, 10, 20, 12, 55, 13, 692000), datetime.datetime(2021, 10, 20, 12, 55, 13, 892000),
         datetime.datetime(2021, 10, 20, 12, 55, 14, 92000), datetime.datetime(2021, 10, 20, 12, 55, 14, 292000),
         datetime.datetime(2021, 10, 20, 12, 55, 14, 492000), datetime.datetime(2021, 10, 20, 12, 55, 14, 692000),
         datetime.datetime(2021, 10, 20, 12, 55, 14, 892000), datetime.datetime(2021, 10, 20, 12, 55, 15, 92000),
         datetime.datetime(2021, 10, 20, 12, 55, 15, 292000), datetime.datetime(2021, 10, 20, 12, 55, 15, 492000),
         datetime.datetime(2021, 10, 20, 12, 55, 15, 692000), datetime.datetime(2021, 10, 20, 12, 55, 15, 892000),
         datetime.datetime(2021, 10, 20, 12, 55, 16, 92000), datetime.datetime(2021, 10, 20, 12, 55, 16, 292000),
         datetime.datetime(2021, 10, 20, 12, 55, 16, 492000), datetime.datetime(2021, 10, 20, 12, 55, 16, 692000),
         datetime.datetime(2021, 10, 20, 12, 55, 16, 892000), datetime.datetime(2021, 10, 20, 12, 55, 17, 92000),
         datetime.datetime(2021, 10, 20, 12, 55, 17, 292000), datetime.datetime(2021, 10, 20, 12, 55, 17, 492000),
         datetime.datetime(2021, 10, 20, 12, 55, 17, 692000), datetime.datetime(2021, 10, 20, 12, 55, 17, 892000),
         datetime.datetime(2021, 10, 20, 12, 55, 18, 92000), datetime.datetime(2021, 10, 20, 12, 55, 18, 292000),
         datetime.datetime(2021, 10, 20, 12, 55, 18, 492000), datetime.datetime(2021, 10, 20, 12, 55, 18, 692000),
         datetime.datetime(2021, 10, 20, 12, 55, 18, 892000), datetime.datetime(2021, 10, 20, 12, 55, 19, 92000),
         datetime.datetime(2021, 10, 20, 12, 55, 19, 292000), datetime.datetime(2021, 10, 20, 12, 55, 19, 492000),
         datetime.datetime(2021, 10, 20, 12, 55, 19, 692000), datetime.datetime(2021, 10, 20, 12, 55, 19, 892000),
         datetime.datetime(2021, 10, 20, 12, 55, 20, 92000), datetime.datetime(2021, 10, 20, 12, 55, 20, 292000),
         datetime.datetime(2021, 10, 20, 12, 55, 20, 492000), datetime.datetime(2021, 10, 20, 12, 55, 20, 692000),
         datetime.datetime(2021, 10, 20, 12, 55, 20, 892000), datetime.datetime(2021, 10, 20, 12, 55, 21, 92000),
         datetime.datetime(2021, 10, 20, 12, 55, 21, 292000), datetime.datetime(2021, 10, 20, 12, 55, 21, 492000),
         datetime.datetime(2021, 10, 20, 12, 55, 21, 692000), datetime.datetime(2021, 10, 20, 12, 55, 21, 892000),
         datetime.datetime(2021, 10, 20, 12, 55, 22, 92000), datetime.datetime(2021, 10, 20, 12, 55, 22, 292000),
         datetime.datetime(2021, 10, 20, 12, 55, 22, 492000), datetime.datetime(2021, 10, 20, 12, 55, 22, 692000),
         datetime.datetime(2021, 10, 20, 12, 55, 22, 892000), datetime.datetime(2021, 10, 20, 12, 55, 23, 92000),
         datetime.datetime(2021, 10, 20, 12, 55, 23, 292000), datetime.datetime(2021, 10, 20, 12, 55, 23, 492000),
         datetime.datetime(2021, 10, 20, 12, 55, 23, 692000), datetime.datetime(2021, 10, 20, 12, 55, 23, 892000),
         datetime.datetime(2021, 10, 20, 12, 55, 24, 92000), datetime.datetime(2021, 10, 20, 12, 55, 24, 292000),
         datetime.datetime(2021, 10, 20, 12, 55, 24, 492000), datetime.datetime(2021, 10, 20, 12, 55, 24, 692000),
         datetime.datetime(2021, 10, 20, 12, 55, 24, 892000), datetime.datetime(2021, 10, 20, 12, 55, 25, 92000),
         datetime.datetime(2021, 10, 20, 12, 55, 25, 292000), datetime.datetime(2021, 10, 20, 12, 55, 25, 492000),
         datetime.datetime(2021, 10, 20, 12, 55, 25, 692000), datetime.datetime(2021, 10, 20, 12, 55, 25, 892000),
         datetime.datetime(2021, 10, 20, 12, 55, 26, 92000), datetime.datetime(2021, 10, 20, 12, 55, 26, 292000),
         datetime.datetime(2021, 10, 20, 12, 55, 26, 492000), datetime.datetime(2021, 10, 20, 12, 55, 26, 692000),
         datetime.datetime(2021, 10, 20, 12, 55, 26, 892000), datetime.datetime(2021, 10, 20, 12, 55, 27, 92000),
         datetime.datetime(2021, 10, 20, 12, 55, 27, 292000), datetime.datetime(2021, 10, 20, 12, 55, 27, 492000),
         datetime.datetime(2021, 10, 20, 12, 55, 27, 692000), datetime.datetime(2021, 10, 20, 12, 55, 27, 892000),
         datetime.datetime(2021, 10, 20, 12, 55, 28, 92000), datetime.datetime(2021, 10, 20, 12, 55, 28, 292000),
         datetime.datetime(2021, 10, 20, 12, 55, 28, 492000), datetime.datetime(2021, 10, 20, 12, 55, 28, 692000),
         datetime.datetime(2021, 10, 20, 12, 55, 28, 892000), datetime.datetime(2021, 10, 20, 12, 55, 29, 92000),
         datetime.datetime(2021, 10, 20, 12, 55, 29, 292000), datetime.datetime(2021, 10, 20, 12, 55, 29, 492000),
         datetime.datetime(2021, 10, 20, 12, 55, 29, 692000), datetime.datetime(2021, 10, 20, 12, 55, 29, 892000),
         datetime.datetime(2021, 10, 20, 12, 55, 30, 92000), datetime.datetime(2021, 10, 20, 12, 55, 30, 292000),
         datetime.datetime(2021, 10, 20, 12, 55, 30, 492000), datetime.datetime(2021, 10, 20, 12, 55, 30, 692000),
         datetime.datetime(2021, 10, 20, 12, 55, 30, 892000), datetime.datetime(2021, 10, 20, 12, 55, 31, 92000),
         datetime.datetime(2021, 10, 20, 12, 55, 31, 292000), datetime.datetime(2021, 10, 20, 12, 55, 31, 492000),
         datetime.datetime(2021, 10, 20, 12, 55, 31, 692000), datetime.datetime(2021, 10, 20, 12, 55, 31, 892000),
         datetime.datetime(2021, 10, 20, 12, 55, 32, 92000), datetime.datetime(2021, 10, 20, 12, 55, 32, 292000),
         datetime.datetime(2021, 10, 20, 12, 55, 32, 492000), datetime.datetime(2021, 10, 20, 12, 55, 32, 692000),
         datetime.datetime(2021, 10, 20, 12, 55, 32, 892000), datetime.datetime(2021, 10, 20, 12, 55, 33, 92000),
         datetime.datetime(2021, 10, 20, 12, 55, 33, 292000), datetime.datetime(2021, 10, 20, 12, 55, 33, 492000),
         datetime.datetime(2021, 10, 20, 12, 55, 33, 692000), datetime.datetime(2021, 10, 20, 12, 55, 33, 892000),
         datetime.datetime(2021, 10, 20, 12, 55, 34, 92000), datetime.datetime(2021, 10, 20, 12, 55, 34, 292000),
         datetime.datetime(2021, 10, 20, 12, 55, 34, 492000), datetime.datetime(2021, 10, 20, 12, 55, 34, 692000),
         datetime.datetime(2021, 10, 20, 12, 55, 34, 892000), datetime.datetime(2021, 10, 20, 12, 55, 35, 92000),
         datetime.datetime(2021, 10, 20, 12, 55, 35, 292000), datetime.datetime(2021, 10, 20, 12, 55, 35, 492000),
         datetime.datetime(2021, 10, 20, 12, 55, 35, 692000), datetime.datetime(2021, 10, 20, 12, 55, 35, 892000),
         datetime.datetime(2021, 10, 20, 12, 55, 36, 92000), datetime.datetime(2021, 10, 20, 12, 55, 36, 292000),
         datetime.datetime(2021, 10, 20, 12, 55, 36, 492000), datetime.datetime(2021, 10, 20, 12, 55, 36, 692000),
         datetime.datetime(2021, 10, 20, 12, 55, 36, 892000), datetime.datetime(2021, 10, 20, 12, 55, 37, 92000),
         datetime.datetime(2021, 10, 20, 12, 55, 37, 292000), datetime.datetime(2021, 10, 20, 12, 55, 37, 492000),
         datetime.datetime(2021, 10, 20, 12, 55, 37, 692000), datetime.datetime(2021, 10, 20, 12, 55, 37, 892000),
         datetime.datetime(2021, 10, 20, 12, 55, 38, 92000), datetime.datetime(2021, 10, 20, 12, 55, 38, 292000),
         datetime.datetime(2021, 10, 20, 12, 55, 38, 492000), datetime.datetime(2021, 10, 20, 12, 55, 38, 692000),
         datetime.datetime(2021, 10, 20, 12, 55, 38, 892000), datetime.datetime(2021, 10, 20, 12, 55, 39, 92000),
         datetime.datetime(2021, 10, 20, 12, 55, 39, 292000), datetime.datetime(2021, 10, 20, 12, 55, 39, 492000),
         datetime.datetime(2021, 10, 20, 12, 55, 39, 692000), datetime.datetime(2021, 10, 20, 12, 55, 39, 892000),
         datetime.datetime(2021, 10, 20, 12, 55, 40, 92000), datetime.datetime(2021, 10, 20, 12, 55, 40, 292000),
         datetime.datetime(2021, 10, 20, 12, 55, 40, 492000), datetime.datetime(2021, 10, 20, 12, 55, 40, 692000),
         datetime.datetime(2021, 10, 20, 12, 55, 40, 892000), datetime.datetime(2021, 10, 20, 12, 55, 41, 92000),
         datetime.datetime(2021, 10, 20, 12, 55, 41, 292000), datetime.datetime(2021, 10, 20, 12, 55, 41, 492000),
         datetime.datetime(2021, 10, 20, 12, 55, 41, 692000), datetime.datetime(2021, 10, 20, 12, 55, 41, 892000),
         datetime.datetime(2021, 10, 20, 12, 55, 42, 92000), datetime.datetime(2021, 10, 20, 12, 55, 42, 292000),
         datetime.datetime(2021, 10, 20, 12, 55, 42, 492000), datetime.datetime(2021, 10, 20, 12, 55, 42, 692000),
         datetime.datetime(2021, 10, 20, 12, 55, 42, 892000), datetime.datetime(2021, 10, 20, 12, 55, 43, 92000),
         datetime.datetime(2021, 10, 20, 12, 55, 43, 292000), datetime.datetime(2021, 10, 20, 12, 55, 43, 492000),
         datetime.datetime(2021, 10, 20, 12, 55, 43, 692000), datetime.datetime(2021, 10, 20, 12, 55, 43, 892000),
         datetime.datetime(2021, 10, 20, 12, 55, 44, 92000), datetime.datetime(2021, 10, 20, 12, 55, 44, 292000),
         datetime.datetime(2021, 10, 20, 12, 55, 44, 492000), datetime.datetime(2021, 10, 20, 12, 55, 44, 692000),
         datetime.datetime(2021, 10, 20, 12, 55, 44, 892000), datetime.datetime(2021, 10, 20, 12, 55, 45, 92000),
         datetime.datetime(2021, 10, 20, 12, 55, 45, 292000), datetime.datetime(2021, 10, 20, 12, 55, 45, 492000),
         datetime.datetime(2021, 10, 20, 12, 55, 45, 692000), datetime.datetime(2021, 10, 20, 12, 55, 45, 892000),
         datetime.datetime(2021, 10, 20, 12, 55, 46, 92000), datetime.datetime(2021, 10, 20, 12, 55, 46, 292000),
         datetime.datetime(2021, 10, 20, 12, 55, 46, 492000), datetime.datetime(2021, 10, 20, 12, 55, 46, 692000),
         datetime.datetime(2021, 10, 20, 12, 55, 46, 892000), datetime.datetime(2021, 10, 20, 12, 55, 47, 92000),
         datetime.datetime(2021, 10, 20, 12, 55, 47, 292000), datetime.datetime(2021, 10, 20, 12, 55, 47, 492000),
         datetime.datetime(2021, 10, 20, 12, 55, 47, 692000), datetime.datetime(2021, 10, 20, 12, 55, 47, 892000),
         datetime.datetime(2021, 10, 20, 12, 55, 48, 92000), datetime.datetime(2021, 10, 20, 12, 55, 48, 292000),
         datetime.datetime(2021, 10, 20, 12, 55, 48, 492000), datetime.datetime(2021, 10, 20, 12, 55, 48, 692000),
         datetime.datetime(2021, 10, 20, 12, 55, 48, 892000), datetime.datetime(2021, 10, 20, 12, 55, 49, 92000),
         datetime.datetime(2021, 10, 20, 12, 55, 49, 292000), datetime.datetime(2021, 10, 20, 12, 55, 49, 492000),
         datetime.datetime(2021, 10, 20, 12, 55, 49, 692000), datetime.datetime(2021, 10, 20, 12, 55, 49, 892000),
         datetime.datetime(2021, 10, 20, 12, 55, 50, 92000), datetime.datetime(2021, 10, 20, 12, 55, 50, 292000),
         datetime.datetime(2021, 10, 20, 12, 55, 50, 492000), datetime.datetime(2021, 10, 20, 12, 55, 50, 692000),
         datetime.datetime(2021, 10, 20, 12, 55, 50, 892000), datetime.datetime(2021, 10, 20, 12, 55, 51, 92000),
         datetime.datetime(2021, 10, 20, 12, 55, 51, 292000), datetime.datetime(2021, 10, 20, 12, 55, 51, 492000),
         datetime.datetime(2021, 10, 20, 12, 55, 51, 692000), datetime.datetime(2021, 10, 20, 12, 55, 51, 892000),
         datetime.datetime(2021, 10, 20, 12, 55, 52, 92000), datetime.datetime(2021, 10, 20, 12, 55, 52, 292000),
         datetime.datetime(2021, 10, 20, 12, 55, 52, 492000), datetime.datetime(2021, 10, 20, 12, 55, 52, 692000),
         datetime.datetime(2021, 10, 20, 12, 55, 52, 892000), datetime.datetime(2021, 10, 20, 12, 55, 53, 92000),
         datetime.datetime(2021, 10, 20, 12, 55, 53, 292000), datetime.datetime(2021, 10, 20, 12, 55, 53, 492000),
         datetime.datetime(2021, 10, 20, 12, 55, 53, 692000), datetime.datetime(2021, 10, 20, 12, 55, 53, 892000),
         datetime.datetime(2021, 10, 20, 12, 55, 54, 92000), datetime.datetime(2021, 10, 20, 12, 55, 54, 292000),
         datetime.datetime(2021, 10, 20, 12, 55, 54, 492000), datetime.datetime(2021, 10, 20, 12, 55, 54, 692000),
         datetime.datetime(2021, 10, 20, 12, 55, 54, 892000), datetime.datetime(2021, 10, 20, 12, 55, 55, 92000),
         datetime.datetime(2021, 10, 20, 12, 55, 55, 292000), datetime.datetime(2021, 10, 20, 12, 55, 55, 492000),
         datetime.datetime(2021, 10, 20, 12, 55, 55, 692000), datetime.datetime(2021, 10, 20, 12, 55, 55, 892000),
         datetime.datetime(2021, 10, 20, 12, 55, 56, 92000), datetime.datetime(2021, 10, 20, 12, 55, 56, 292000),
         datetime.datetime(2021, 10, 20, 12, 55, 56, 492000), datetime.datetime(2021, 10, 20, 12, 55, 56, 692000),
         datetime.datetime(2021, 10, 20, 12, 55, 56, 892000), datetime.datetime(2021, 10, 20, 12, 55, 57, 92000),
         datetime.datetime(2021, 10, 20, 12, 55, 57, 292000), datetime.datetime(2021, 10, 20, 12, 55, 57, 492000),
         datetime.datetime(2021, 10, 20, 12, 55, 57, 692000), datetime.datetime(2021, 10, 20, 12, 55, 57, 892000),
         datetime.datetime(2021, 10, 20, 12, 55, 58, 92000), datetime.datetime(2021, 10, 20, 12, 55, 58, 292000),
         datetime.datetime(2021, 10, 20, 12, 55, 58, 492000), datetime.datetime(2021, 10, 20, 12, 55, 58, 692000),
         datetime.datetime(2021, 10, 20, 12, 55, 58, 892000), datetime.datetime(2021, 10, 20, 12, 55, 59, 92000),
         datetime.datetime(2021, 10, 20, 12, 55, 59, 292000), datetime.datetime(2021, 10, 20, 12, 55, 59, 492000),
         datetime.datetime(2021, 10, 20, 12, 55, 59, 692000), datetime.datetime(2021, 10, 20, 12, 55, 59, 892000),
         datetime.datetime(2021, 10, 20, 12, 56, 0, 92000), datetime.datetime(2021, 10, 20, 12, 56, 0, 292000),
         datetime.datetime(2021, 10, 20, 12, 56, 0, 492000), datetime.datetime(2021, 10, 20, 12, 56, 0, 692000),
         datetime.datetime(2021, 10, 20, 12, 56, 0, 892000), datetime.datetime(2021, 10, 20, 12, 56, 1, 92000),
         datetime.datetime(2021, 10, 20, 12, 56, 1, 292000), datetime.datetime(2021, 10, 20, 12, 56, 1, 492000),
         datetime.datetime(2021, 10, 20, 12, 56, 1, 692000), datetime.datetime(2021, 10, 20, 12, 56, 1, 892000),
         datetime.datetime(2021, 10, 20, 12, 56, 2, 92000), datetime.datetime(2021, 10, 20, 12, 56, 2, 292000),
         datetime.datetime(2021, 10, 20, 12, 56, 2, 492000), datetime.datetime(2021, 10, 20, 12, 56, 2, 692000),
         datetime.datetime(2021, 10, 20, 12, 56, 2, 892000), datetime.datetime(2021, 10, 20, 12, 56, 3, 92000),
         datetime.datetime(2021, 10, 20, 12, 56, 3, 292000), datetime.datetime(2021, 10, 20, 12, 56, 3, 492000),
         datetime.datetime(2021, 10, 20, 12, 56, 3, 692000), datetime.datetime(2021, 10, 20, 12, 56, 3, 892000),
         datetime.datetime(2021, 10, 20, 12, 56, 4, 92000), datetime.datetime(2021, 10, 20, 12, 56, 4, 292000),
         datetime.datetime(2021, 10, 20, 12, 56, 4, 492000), datetime.datetime(2021, 10, 20, 12, 56, 4, 692000),
         datetime.datetime(2021, 10, 20, 12, 56, 4, 892000), datetime.datetime(2021, 10, 20, 12, 56, 5, 92000),
         datetime.datetime(2021, 10, 20, 12, 56, 5, 292000), datetime.datetime(2021, 10, 20, 12, 56, 5, 492000),
         datetime.datetime(2021, 10, 20, 12, 56, 5, 692000), datetime.datetime(2021, 10, 20, 12, 56, 5, 892000),
         datetime.datetime(2021, 10, 20, 12, 56, 6, 92000), datetime.datetime(2021, 10, 20, 12, 56, 6, 292000),
         datetime.datetime(2021, 10, 20, 12, 56, 6, 492000), datetime.datetime(2021, 10, 20, 12, 56, 6, 692000),
         datetime.datetime(2021, 10, 20, 12, 56, 6, 892000), datetime.datetime(2021, 10, 20, 12, 56, 7, 92000),
         datetime.datetime(2021, 10, 20, 12, 56, 7, 292000), datetime.datetime(2021, 10, 20, 12, 56, 7, 492000),
         datetime.datetime(2021, 10, 20, 12, 56, 7, 692000), datetime.datetime(2021, 10, 20, 12, 56, 7, 892000),
         datetime.datetime(2021, 10, 20, 12, 56, 8, 92000), datetime.datetime(2021, 10, 20, 12, 56, 8, 292000),
         datetime.datetime(2021, 10, 20, 12, 56, 8, 492000), datetime.datetime(2021, 10, 20, 12, 56, 8, 692000),
         datetime.datetime(2021, 10, 20, 12, 56, 8, 892000), datetime.datetime(2021, 10, 20, 12, 56, 9, 92000),
         datetime.datetime(2021, 10, 20, 12, 56, 9, 292000), datetime.datetime(2021, 10, 20, 12, 56, 9, 492000),
         datetime.datetime(2021, 10, 20, 12, 56, 9, 692000), datetime.datetime(2021, 10, 20, 12, 56, 9, 892000),
         datetime.datetime(2021, 10, 20, 12, 56, 10, 92000), datetime.datetime(2021, 10, 20, 12, 56, 10, 292000),
         datetime.datetime(2021, 10, 20, 12, 56, 10, 492000), datetime.datetime(2021, 10, 20, 12, 56, 10, 692000),
         datetime.datetime(2021, 10, 20, 12, 56, 10, 892000), datetime.datetime(2021, 10, 20, 12, 56, 11, 92000),
         datetime.datetime(2021, 10, 20, 12, 56, 11, 292000), datetime.datetime(2021, 10, 20, 12, 56, 11, 492000),
         datetime.datetime(2021, 10, 20, 12, 56, 11, 692000), datetime.datetime(2021, 10, 20, 12, 56, 11, 892000),
         datetime.datetime(2021, 10, 20, 12, 56, 12, 92000), datetime.datetime(2021, 10, 20, 12, 56, 12, 292000),
         datetime.datetime(2021, 10, 20, 12, 56, 12, 492000), datetime.datetime(2021, 10, 20, 12, 56, 12, 692000),
         datetime.datetime(2021, 10, 20, 12, 56, 12, 892000), datetime.datetime(2021, 10, 20, 12, 56, 13, 92000),
         datetime.datetime(2021, 10, 20, 12, 56, 13, 292000), datetime.datetime(2021, 10, 20, 12, 56, 13, 492000),
         datetime.datetime(2021, 10, 20, 12, 56, 13, 692000), datetime.datetime(2021, 10, 20, 12, 56, 13, 892000),
         datetime.datetime(2021, 10, 20, 12, 56, 14, 92000), datetime.datetime(2021, 10, 20, 12, 56, 14, 292000),
         datetime.datetime(2021, 10, 20, 12, 56, 14, 492000), datetime.datetime(2021, 10, 20, 12, 56, 14, 692000),
         datetime.datetime(2021, 10, 20, 12, 56, 14, 892000), datetime.datetime(2021, 10, 20, 12, 56, 15, 92000),
         datetime.datetime(2021, 10, 20, 12, 56, 15, 292000), datetime.datetime(2021, 10, 20, 12, 56, 15, 492000),
         datetime.datetime(2021, 10, 20, 12, 56, 15, 692000), datetime.datetime(2021, 10, 20, 12, 56, 15, 892000),
         datetime.datetime(2021, 10, 20, 12, 56, 16, 92000), datetime.datetime(2021, 10, 20, 12, 56, 16, 292000),
         datetime.datetime(2021, 10, 20, 12, 56, 16, 492000), datetime.datetime(2021, 10, 20, 12, 56, 16, 692000),
         datetime.datetime(2021, 10, 20, 12, 56, 16, 892000), datetime.datetime(2021, 10, 20, 12, 56, 17, 92000),
         datetime.datetime(2021, 10, 20, 12, 56, 17, 292000), datetime.datetime(2021, 10, 20, 12, 56, 17, 492000),
         datetime.datetime(2021, 10, 20, 12, 56, 17, 692000), datetime.datetime(2021, 10, 20, 12, 56, 17, 892000),
         datetime.datetime(2021, 10, 20, 12, 56, 18, 92000), datetime.datetime(2021, 10, 20, 12, 56, 18, 292000),
         datetime.datetime(2021, 10, 20, 12, 56, 18, 492000), datetime.datetime(2021, 10, 20, 12, 56, 18, 692000),
         datetime.datetime(2021, 10, 20, 12, 56, 18, 892000), datetime.datetime(2021, 10, 20, 12, 56, 19, 92000),
         datetime.datetime(2021, 10, 20, 12, 56, 19, 292000), datetime.datetime(2021, 10, 20, 12, 56, 19, 492000),
         datetime.datetime(2021, 10, 20, 12, 56, 19, 692000), datetime.datetime(2021, 10, 20, 12, 56, 19, 892000),
         datetime.datetime(2021, 10, 20, 12, 56, 20, 92000), datetime.datetime(2021, 10, 20, 12, 56, 20, 292000),
         datetime.datetime(2021, 10, 20, 12, 56, 20, 492000), datetime.datetime(2021, 10, 20, 12, 56, 20, 692000),
         datetime.datetime(2021, 10, 20, 12, 56, 20, 892000), datetime.datetime(2021, 10, 20, 12, 56, 21, 92000),
         datetime.datetime(2021, 10, 20, 12, 56, 21, 292000), datetime.datetime(2021, 10, 20, 12, 56, 21, 492000),
         datetime.datetime(2021, 10, 20, 12, 56, 21, 692000), datetime.datetime(2021, 10, 20, 12, 56, 21, 892000),
         datetime.datetime(2021, 10, 20, 12, 56, 22, 92000), datetime.datetime(2021, 10, 20, 12, 56, 22, 292000),
         datetime.datetime(2021, 10, 20, 12, 56, 22, 492000), datetime.datetime(2021, 10, 20, 12, 56, 22, 692000),
         datetime.datetime(2021, 10, 20, 12, 56, 22, 892000), datetime.datetime(2021, 10, 20, 12, 56, 23, 92000),
         datetime.datetime(2021, 10, 20, 12, 56, 23, 292000), datetime.datetime(2021, 10, 20, 12, 56, 23, 492000),
         datetime.datetime(2021, 10, 20, 12, 56, 23, 692000), datetime.datetime(2021, 10, 20, 12, 56, 23, 892000),
         datetime.datetime(2021, 10, 20, 12, 56, 24, 92000), datetime.datetime(2021, 10, 20, 12, 56, 24, 292000),
         datetime.datetime(2021, 10, 20, 12, 56, 24, 492000), datetime.datetime(2021, 10, 20, 12, 56, 24, 692000),
         datetime.datetime(2021, 10, 20, 12, 56, 24, 892000), datetime.datetime(2021, 10, 20, 12, 56, 25, 92000),
         datetime.datetime(2021, 10, 20, 12, 56, 25, 292000), datetime.datetime(2021, 10, 20, 12, 56, 25, 492000),
         datetime.datetime(2021, 10, 20, 12, 56, 25, 692000), datetime.datetime(2021, 10, 20, 12, 56, 25, 892000),
         datetime.datetime(2021, 10, 20, 12, 56, 26, 92000), datetime.datetime(2021, 10, 20, 12, 56, 26, 292000),
         datetime.datetime(2021, 10, 20, 12, 56, 26, 492000), datetime.datetime(2021, 10, 20, 12, 56, 26, 692000),
         datetime.datetime(2021, 10, 20, 12, 56, 26, 892000), datetime.datetime(2021, 10, 20, 12, 56, 27, 92000),
         datetime.datetime(2021, 10, 20, 12, 56, 27, 292000), datetime.datetime(2021, 10, 20, 12, 56, 27, 492000),
         datetime.datetime(2021, 10, 20, 12, 56, 27, 692000), datetime.datetime(2021, 10, 20, 12, 56, 27, 892000),
         datetime.datetime(2021, 10, 20, 12, 56, 28, 92000), datetime.datetime(2021, 10, 20, 12, 56, 28, 292000),
         datetime.datetime(2021, 10, 20, 12, 56, 28, 492000), datetime.datetime(2021, 10, 20, 12, 56, 28, 692000),
         datetime.datetime(2021, 10, 20, 12, 56, 28, 892000), datetime.datetime(2021, 10, 20, 12, 56, 29, 92000),
         datetime.datetime(2021, 10, 20, 12, 56, 29, 292000), datetime.datetime(2021, 10, 20, 12, 56, 29, 492000),
         datetime.datetime(2021, 10, 20, 12, 56, 29, 692000), datetime.datetime(2021, 10, 20, 12, 56, 29, 892000),
         datetime.datetime(2021, 10, 20, 12, 56, 30, 92000), datetime.datetime(2021, 10, 20, 12, 56, 30, 292000),
         datetime.datetime(2021, 10, 20, 12, 56, 30, 492000), datetime.datetime(2021, 10, 20, 12, 56, 30, 692000),
         datetime.datetime(2021, 10, 20, 12, 56, 30, 892000), datetime.datetime(2021, 10, 20, 12, 56, 31, 92000),
         datetime.datetime(2021, 10, 20, 12, 56, 31, 292000), datetime.datetime(2021, 10, 20, 12, 56, 31, 492000),
         datetime.datetime(2021, 10, 20, 12, 56, 31, 692000), datetime.datetime(2021, 10, 20, 12, 56, 31, 892000),
         datetime.datetime(2021, 10, 20, 12, 56, 32, 92000), datetime.datetime(2021, 10, 20, 12, 56, 32, 292000),
         datetime.datetime(2021, 10, 20, 12, 56, 32, 492000), datetime.datetime(2021, 10, 20, 12, 56, 32, 692000),
         datetime.datetime(2021, 10, 20, 12, 56, 32, 892000), datetime.datetime(2021, 10, 20, 12, 56, 33, 92000),
         datetime.datetime(2021, 10, 20, 12, 56, 33, 292000), datetime.datetime(2021, 10, 20, 12, 56, 33, 492000),
         datetime.datetime(2021, 10, 20, 12, 56, 33, 692000), datetime.datetime(2021, 10, 20, 12, 56, 33, 892000),
         datetime.datetime(2021, 10, 20, 12, 56, 34, 92000), datetime.datetime(2021, 10, 20, 12, 56, 34, 292000),
         datetime.datetime(2021, 10, 20, 12, 56, 34, 492000), datetime.datetime(2021, 10, 20, 12, 56, 34, 692000),
         datetime.datetime(2021, 10, 20, 12, 56, 34, 892000), datetime.datetime(2021, 10, 20, 12, 56, 35, 92000),
         datetime.datetime(2021, 10, 20, 12, 56, 35, 292000), datetime.datetime(2021, 10, 20, 12, 56, 35, 492000),
         datetime.datetime(2021, 10, 20, 12, 56, 35, 692000), datetime.datetime(2021, 10, 20, 12, 56, 35, 892000),
         datetime.datetime(2021, 10, 20, 12, 56, 36, 92000), datetime.datetime(2021, 10, 20, 12, 56, 36, 292000),
         datetime.datetime(2021, 10, 20, 12, 56, 36, 492000), datetime.datetime(2021, 10, 20, 12, 56, 36, 692000),
         datetime.datetime(2021, 10, 20, 12, 56, 36, 892000), datetime.datetime(2021, 10, 20, 12, 56, 37, 92000),
         datetime.datetime(2021, 10, 20, 12, 56, 37, 292000), datetime.datetime(2021, 10, 20, 12, 56, 37, 492000),
         datetime.datetime(2021, 10, 20, 12, 56, 37, 692000), datetime.datetime(2021, 10, 20, 12, 56, 37, 892000),
         datetime.datetime(2021, 10, 20, 12, 56, 38, 92000), datetime.datetime(2021, 10, 20, 12, 56, 38, 292000),
         datetime.datetime(2021, 10, 20, 12, 56, 38, 492000), datetime.datetime(2021, 10, 20, 12, 56, 38, 692000),
         datetime.datetime(2021, 10, 20, 12, 56, 38, 892000), datetime.datetime(2021, 10, 20, 12, 56, 39, 92000),
         datetime.datetime(2021, 10, 20, 12, 56, 39, 292000), datetime.datetime(2021, 10, 20, 12, 56, 39, 492000),
         datetime.datetime(2021, 10, 20, 12, 56, 39, 692000), datetime.datetime(2021, 10, 20, 12, 56, 39, 892000),
         datetime.datetime(2021, 10, 20, 12, 56, 40, 92000), datetime.datetime(2021, 10, 20, 12, 56, 40, 292000),
         datetime.datetime(2021, 10, 20, 12, 56, 40, 492000), datetime.datetime(2021, 10, 20, 12, 56, 40, 692000),
         datetime.datetime(2021, 10, 20, 12, 56, 40, 892000), datetime.datetime(2021, 10, 20, 12, 56, 41, 92000),
         datetime.datetime(2021, 10, 20, 12, 56, 41, 292000), datetime.datetime(2021, 10, 20, 12, 56, 41, 492000),
         datetime.datetime(2021, 10, 20, 12, 56, 41, 692000), datetime.datetime(2021, 10, 20, 12, 56, 41, 892000),
         datetime.datetime(2021, 10, 20, 12, 56, 42, 92000), datetime.datetime(2021, 10, 20, 12, 56, 42, 292000),
         datetime.datetime(2021, 10, 20, 12, 56, 42, 492000), datetime.datetime(2021, 10, 20, 12, 56, 42, 692000),
         datetime.datetime(2021, 10, 20, 12, 56, 42, 892000), datetime.datetime(2021, 10, 20, 12, 56, 43, 92000),
         datetime.datetime(2021, 10, 20, 12, 56, 43, 292000), datetime.datetime(2021, 10, 20, 12, 56, 43, 492000),
         datetime.datetime(2021, 10, 20, 12, 56, 43, 692000), datetime.datetime(2021, 10, 20, 12, 56, 43, 892000),
         datetime.datetime(2021, 10, 20, 12, 56, 44, 92000), datetime.datetime(2021, 10, 20, 12, 56, 44, 292000),
         datetime.datetime(2021, 10, 20, 12, 56, 44, 492000), datetime.datetime(2021, 10, 20, 12, 56, 44, 692000),
         datetime.datetime(2021, 10, 20, 12, 56, 44, 892000), datetime.datetime(2021, 10, 20, 12, 56, 45, 92000),
         datetime.datetime(2021, 10, 20, 12, 56, 45, 292000), datetime.datetime(2021, 10, 20, 12, 56, 45, 492000),
         datetime.datetime(2021, 10, 20, 12, 56, 45, 692000), datetime.datetime(2021, 10, 20, 12, 56, 45, 892000),
         datetime.datetime(2021, 10, 20, 12, 56, 46, 92000), datetime.datetime(2021, 10, 20, 12, 56, 46, 292000),
         datetime.datetime(2021, 10, 20, 12, 56, 46, 492000), datetime.datetime(2021, 10, 20, 12, 56, 46, 692000),
         datetime.datetime(2021, 10, 20, 12, 56, 46, 892000), datetime.datetime(2021, 10, 20, 12, 56, 47, 92000),
         datetime.datetime(2021, 10, 20, 12, 56, 47, 292000), datetime.datetime(2021, 10, 20, 12, 56, 47, 492000),
         datetime.datetime(2021, 10, 20, 12, 56, 47, 692000), datetime.datetime(2021, 10, 20, 12, 56, 47, 892000),
         datetime.datetime(2021, 10, 20, 12, 56, 48, 92000), datetime.datetime(2021, 10, 20, 12, 56, 48, 292000),
         datetime.datetime(2021, 10, 20, 12, 56, 48, 492000), datetime.datetime(2021, 10, 20, 12, 56, 48, 692000),
         datetime.datetime(2021, 10, 20, 12, 56, 48, 892000), datetime.datetime(2021, 10, 20, 12, 56, 49, 92000),
         datetime.datetime(2021, 10, 20, 12, 56, 49, 292000), datetime.datetime(2021, 10, 20, 12, 56, 49, 492000),
         datetime.datetime(2021, 10, 20, 12, 56, 49, 692000), datetime.datetime(2021, 10, 20, 12, 56, 49, 892000),
         datetime.datetime(2021, 10, 20, 12, 56, 50, 92000), datetime.datetime(2021, 10, 20, 12, 56, 50, 292000),
         datetime.datetime(2021, 10, 20, 12, 56, 50, 492000), datetime.datetime(2021, 10, 20, 12, 56, 50, 692000),
         datetime.datetime(2021, 10, 20, 12, 56, 50, 892000), datetime.datetime(2021, 10, 20, 12, 56, 51, 92000),
         datetime.datetime(2021, 10, 20, 12, 56, 51, 292000), datetime.datetime(2021, 10, 20, 12, 56, 51, 492000),
         datetime.datetime(2021, 10, 20, 12, 56, 51, 692000), datetime.datetime(2021, 10, 20, 12, 56, 51, 892000),
         datetime.datetime(2021, 10, 20, 12, 56, 52, 92000), datetime.datetime(2021, 10, 20, 12, 56, 52, 292000),
         datetime.datetime(2021, 10, 20, 12, 56, 52, 492000), datetime.datetime(2021, 10, 20, 12, 56, 52, 692000),
         datetime.datetime(2021, 10, 20, 12, 56, 52, 892000), datetime.datetime(2021, 10, 20, 12, 56, 53, 92000),
         datetime.datetime(2021, 10, 20, 12, 56, 53, 292000), datetime.datetime(2021, 10, 20, 12, 56, 53, 492000),
         datetime.datetime(2021, 10, 20, 12, 56, 53, 692000), datetime.datetime(2021, 10, 20, 12, 56, 53, 892000),
         datetime.datetime(2021, 10, 20, 12, 56, 54, 92000), datetime.datetime(2021, 10, 20, 12, 56, 54, 292000),
         datetime.datetime(2021, 10, 20, 12, 56, 54, 492000), datetime.datetime(2021, 10, 20, 12, 56, 54, 692000),
         datetime.datetime(2021, 10, 20, 12, 56, 54, 892000), datetime.datetime(2021, 10, 20, 12, 56, 55, 92000),
         datetime.datetime(2021, 10, 20, 12, 56, 55, 292000), datetime.datetime(2021, 10, 20, 12, 56, 55, 492000),
         datetime.datetime(2021, 10, 20, 12, 56, 55, 692000), datetime.datetime(2021, 10, 20, 12, 56, 55, 892000),
         datetime.datetime(2021, 10, 20, 12, 56, 56, 92000), datetime.datetime(2021, 10, 20, 12, 56, 56, 292000),
         datetime.datetime(2021, 10, 20, 12, 56, 56, 492000), datetime.datetime(2021, 10, 20, 12, 56, 56, 692000),
         datetime.datetime(2021, 10, 20, 12, 56, 56, 892000), datetime.datetime(2021, 10, 20, 12, 56, 57, 92000),
         datetime.datetime(2021, 10, 20, 12, 56, 57, 292000), datetime.datetime(2021, 10, 20, 12, 56, 57, 492000),
         datetime.datetime(2021, 10, 20, 12, 56, 57, 692000), datetime.datetime(2021, 10, 20, 12, 56, 57, 892000),
         datetime.datetime(2021, 10, 20, 12, 56, 58, 92000), datetime.datetime(2021, 10, 20, 12, 56, 58, 292000),
         datetime.datetime(2021, 10, 20, 12, 56, 58, 492000), datetime.datetime(2021, 10, 20, 12, 56, 58, 692000),
         datetime.datetime(2021, 10, 20, 12, 56, 58, 892000), datetime.datetime(2021, 10, 20, 12, 56, 59, 92000),
         datetime.datetime(2021, 10, 20, 12, 56, 59, 292000), datetime.datetime(2021, 10, 20, 12, 56, 59, 492000),
         datetime.datetime(2021, 10, 20, 12, 56, 59, 692000), datetime.datetime(2021, 10, 20, 12, 56, 59, 892000),
         datetime.datetime(2021, 10, 20, 12, 57, 0, 92000), datetime.datetime(2021, 10, 20, 12, 57, 0, 292000),
         datetime.datetime(2021, 10, 20, 12, 57, 0, 492000), datetime.datetime(2021, 10, 20, 12, 57, 0, 692000),
         datetime.datetime(2021, 10, 20, 12, 57, 0, 892000), datetime.datetime(2021, 10, 20, 12, 57, 1, 92000),
         datetime.datetime(2021, 10, 20, 12, 57, 1, 292000), datetime.datetime(2021, 10, 20, 12, 57, 1, 492000),
         datetime.datetime(2021, 10, 20, 12, 57, 1, 692000), datetime.datetime(2021, 10, 20, 12, 57, 1, 892000),
         datetime.datetime(2021, 10, 20, 12, 57, 2, 92000), datetime.datetime(2021, 10, 20, 12, 57, 2, 292000),
         datetime.datetime(2021, 10, 20, 12, 57, 2, 492000), datetime.datetime(2021, 10, 20, 12, 57, 2, 692000),
         datetime.datetime(2021, 10, 20, 12, 57, 2, 892000), datetime.datetime(2021, 10, 20, 12, 57, 3, 92000),
         datetime.datetime(2021, 10, 20, 12, 57, 3, 292000), datetime.datetime(2021, 10, 20, 12, 57, 3, 492000),
         datetime.datetime(2021, 10, 20, 12, 57, 3, 692000), datetime.datetime(2021, 10, 20, 12, 57, 3, 892000),
         datetime.datetime(2021, 10, 20, 12, 57, 4, 92000), datetime.datetime(2021, 10, 20, 12, 57, 4, 292000),
         datetime.datetime(2021, 10, 20, 12, 57, 4, 492000), datetime.datetime(2021, 10, 20, 12, 57, 4, 692000),
         datetime.datetime(2021, 10, 20, 12, 57, 4, 892000), datetime.datetime(2021, 10, 20, 12, 57, 5, 92000),
         datetime.datetime(2021, 10, 20, 12, 57, 5, 292000), datetime.datetime(2021, 10, 20, 12, 57, 5, 492000),
         datetime.datetime(2021, 10, 20, 12, 57, 5, 692000), datetime.datetime(2021, 10, 20, 12, 57, 5, 892000),
         datetime.datetime(2021, 10, 20, 12, 57, 6, 92000), datetime.datetime(2021, 10, 20, 12, 57, 6, 292000),
         datetime.datetime(2021, 10, 20, 12, 57, 6, 492000), datetime.datetime(2021, 10, 20, 12, 57, 6, 692000),
         datetime.datetime(2021, 10, 20, 12, 57, 6, 892000), datetime.datetime(2021, 10, 20, 12, 57, 7, 92000),
         datetime.datetime(2021, 10, 20, 12, 57, 7, 292000), datetime.datetime(2021, 10, 20, 12, 57, 7, 492000),
         datetime.datetime(2021, 10, 20, 12, 57, 7, 692000), datetime.datetime(2021, 10, 20, 12, 57, 7, 892000),
         datetime.datetime(2021, 10, 20, 12, 57, 8, 92000), datetime.datetime(2021, 10, 20, 12, 57, 8, 292000),
         datetime.datetime(2021, 10, 20, 12, 57, 8, 492000), datetime.datetime(2021, 10, 20, 12, 57, 8, 692000),
         datetime.datetime(2021, 10, 20, 12, 57, 8, 892000), datetime.datetime(2021, 10, 20, 12, 57, 9, 92000),
         datetime.datetime(2021, 10, 20, 12, 57, 9, 292000), datetime.datetime(2021, 10, 20, 12, 57, 9, 492000),
         datetime.datetime(2021, 10, 20, 12, 57, 9, 692000), datetime.datetime(2021, 10, 20, 12, 57, 9, 892000),
         datetime.datetime(2021, 10, 20, 12, 57, 10, 92000), datetime.datetime(2021, 10, 20, 12, 57, 10, 292000),
         datetime.datetime(2021, 10, 20, 12, 57, 10, 492000), datetime.datetime(2021, 10, 20, 12, 57, 10, 692000),
         datetime.datetime(2021, 10, 20, 12, 57, 10, 892000), datetime.datetime(2021, 10, 20, 12, 57, 11, 92000),
         datetime.datetime(2021, 10, 20, 12, 57, 11, 292000), datetime.datetime(2021, 10, 20, 12, 57, 11, 492000),
         datetime.datetime(2021, 10, 20, 12, 57, 11, 692000), datetime.datetime(2021, 10, 20, 12, 57, 11, 892000),
         datetime.datetime(2021, 10, 20, 12, 57, 12, 92000), datetime.datetime(2021, 10, 20, 12, 57, 12, 292000),
         datetime.datetime(2021, 10, 20, 12, 57, 12, 492000), datetime.datetime(2021, 10, 20, 12, 57, 12, 692000),
         datetime.datetime(2021, 10, 20, 12, 57, 12, 892000), datetime.datetime(2021, 10, 20, 12, 57, 13, 92000),
         datetime.datetime(2021, 10, 20, 12, 57, 13, 292000), datetime.datetime(2021, 10, 20, 12, 57, 13, 492000),
         datetime.datetime(2021, 10, 20, 12, 57, 13, 692000), datetime.datetime(2021, 10, 20, 12, 57, 13, 892000),
         datetime.datetime(2021, 10, 20, 12, 57, 14, 92000), datetime.datetime(2021, 10, 20, 12, 57, 14, 292000),
         datetime.datetime(2021, 10, 20, 12, 57, 14, 492000), datetime.datetime(2021, 10, 20, 12, 57, 14, 692000),
         datetime.datetime(2021, 10, 20, 12, 57, 14, 892000), datetime.datetime(2021, 10, 20, 12, 57, 15, 92000),
         datetime.datetime(2021, 10, 20, 12, 57, 15, 292000), datetime.datetime(2021, 10, 20, 12, 57, 15, 492000),
         datetime.datetime(2021, 10, 20, 12, 57, 15, 692000), datetime.datetime(2021, 10, 20, 12, 57, 15, 892000),
         datetime.datetime(2021, 10, 20, 12, 57, 16, 92000), datetime.datetime(2021, 10, 20, 12, 57, 16, 292000),
         datetime.datetime(2021, 10, 20, 12, 57, 16, 492000), datetime.datetime(2021, 10, 20, 12, 57, 16, 692000),
         datetime.datetime(2021, 10, 20, 12, 57, 16, 892000), datetime.datetime(2021, 10, 20, 12, 57, 17, 92000),
         datetime.datetime(2021, 10, 20, 12, 57, 17, 292000), datetime.datetime(2021, 10, 20, 12, 57, 17, 492000),
         datetime.datetime(2021, 10, 20, 12, 57, 17, 692000), datetime.datetime(2021, 10, 20, 12, 57, 17, 892000),
         datetime.datetime(2021, 10, 20, 12, 57, 18, 92000), datetime.datetime(2021, 10, 20, 12, 57, 18, 292000),
         datetime.datetime(2021, 10, 20, 12, 57, 18, 492000), datetime.datetime(2021, 10, 20, 12, 57, 18, 692000),
         datetime.datetime(2021, 10, 20, 12, 57, 18, 892000), datetime.datetime(2021, 10, 20, 12, 57, 19, 92000),
         datetime.datetime(2021, 10, 20, 12, 57, 19, 292000), datetime.datetime(2021, 10, 20, 12, 57, 19, 492000),
         datetime.datetime(2021, 10, 20, 12, 57, 19, 692000), datetime.datetime(2021, 10, 20, 12, 57, 19, 892000),
         datetime.datetime(2021, 10, 20, 12, 57, 20, 92000), datetime.datetime(2021, 10, 20, 12, 57, 20, 292000),
         datetime.datetime(2021, 10, 20, 12, 57, 20, 492000), datetime.datetime(2021, 10, 20, 12, 57, 20, 692000),
         datetime.datetime(2021, 10, 20, 12, 57, 20, 892000), datetime.datetime(2021, 10, 20, 12, 57, 21, 92000),
         datetime.datetime(2021, 10, 20, 12, 57, 21, 292000), datetime.datetime(2021, 10, 20, 12, 57, 21, 492000),
         datetime.datetime(2021, 10, 20, 12, 57, 21, 692000), datetime.datetime(2021, 10, 20, 12, 57, 21, 892000),
         datetime.datetime(2021, 10, 20, 12, 57, 22, 92000), datetime.datetime(2021, 10, 20, 12, 57, 22, 292000),
         datetime.datetime(2021, 10, 20, 12, 57, 22, 492000), datetime.datetime(2021, 10, 20, 12, 57, 22, 692000),
         datetime.datetime(2021, 10, 20, 12, 57, 22, 892000), datetime.datetime(2021, 10, 20, 12, 57, 23, 92000),
         datetime.datetime(2021, 10, 20, 12, 57, 23, 292000), datetime.datetime(2021, 10, 20, 12, 57, 23, 492000),
         datetime.datetime(2021, 10, 20, 12, 57, 23, 692000), datetime.datetime(2021, 10, 20, 12, 57, 23, 892000),
         datetime.datetime(2021, 10, 20, 12, 57, 24, 92000), datetime.datetime(2021, 10, 20, 12, 57, 24, 292000),
         datetime.datetime(2021, 10, 20, 12, 57, 24, 492000), datetime.datetime(2021, 10, 20, 12, 57, 24, 692000),
         datetime.datetime(2021, 10, 20, 12, 57, 24, 892000), datetime.datetime(2021, 10, 20, 12, 57, 25, 92000),
         datetime.datetime(2021, 10, 20, 12, 57, 25, 292000), datetime.datetime(2021, 10, 20, 12, 57, 25, 492000),
         datetime.datetime(2021, 10, 20, 12, 57, 25, 692000), datetime.datetime(2021, 10, 20, 12, 57, 25, 892000),
         datetime.datetime(2021, 10, 20, 12, 57, 26, 92000), datetime.datetime(2021, 10, 20, 12, 57, 26, 292000),
         datetime.datetime(2021, 10, 20, 12, 57, 26, 492000), datetime.datetime(2021, 10, 20, 12, 57, 26, 692000),
         datetime.datetime(2021, 10, 20, 12, 57, 26, 892000), datetime.datetime(2021, 10, 20, 12, 57, 27, 92000),
         datetime.datetime(2021, 10, 20, 12, 57, 27, 292000), datetime.datetime(2021, 10, 20, 12, 57, 27, 492000),
         datetime.datetime(2021, 10, 20, 12, 57, 27, 692000), datetime.datetime(2021, 10, 20, 12, 57, 27, 892000),
         datetime.datetime(2021, 10, 20, 12, 57, 28, 92000), datetime.datetime(2021, 10, 20, 12, 57, 28, 292000),
         datetime.datetime(2021, 10, 20, 12, 57, 28, 492000), datetime.datetime(2021, 10, 20, 12, 57, 28, 692000),
         datetime.datetime(2021, 10, 20, 12, 57, 28, 892000), datetime.datetime(2021, 10, 20, 12, 57, 29, 92000),
         datetime.datetime(2021, 10, 20, 12, 57, 29, 292000), datetime.datetime(2021, 10, 20, 12, 57, 29, 492000),
         datetime.datetime(2021, 10, 20, 12, 57, 29, 692000), datetime.datetime(2021, 10, 20, 12, 57, 29, 892000),
         datetime.datetime(2021, 10, 20, 12, 57, 30, 92000), datetime.datetime(2021, 10, 20, 12, 57, 30, 292000),
         datetime.datetime(2021, 10, 20, 12, 57, 30, 492000), datetime.datetime(2021, 10, 20, 12, 57, 30, 692000),
         datetime.datetime(2021, 10, 20, 12, 57, 30, 892000), datetime.datetime(2021, 10, 20, 12, 57, 31, 92000),
         datetime.datetime(2021, 10, 20, 12, 57, 31, 292000), datetime.datetime(2021, 10, 20, 12, 57, 31, 492000),
         datetime.datetime(2021, 10, 20, 12, 57, 31, 692000), datetime.datetime(2021, 10, 20, 12, 57, 31, 892000),
         datetime.datetime(2021, 10, 20, 12, 57, 32, 92000), datetime.datetime(2021, 10, 20, 12, 57, 32, 292000),
         datetime.datetime(2021, 10, 20, 12, 57, 32, 492000), datetime.datetime(2021, 10, 20, 12, 57, 32, 692000),
         datetime.datetime(2021, 10, 20, 12, 57, 32, 892000), datetime.datetime(2021, 10, 20, 12, 57, 33, 92000),
         datetime.datetime(2021, 10, 20, 12, 57, 33, 292000), datetime.datetime(2021, 10, 20, 12, 57, 33, 492000),
         datetime.datetime(2021, 10, 20, 12, 57, 33, 692000), datetime.datetime(2021, 10, 20, 12, 57, 33, 892000),
         datetime.datetime(2021, 10, 20, 12, 57, 34, 92000), datetime.datetime(2021, 10, 20, 12, 57, 34, 292000),
         datetime.datetime(2021, 10, 20, 12, 57, 34, 492000), datetime.datetime(2021, 10, 20, 12, 57, 34, 692000),
         datetime.datetime(2021, 10, 20, 12, 57, 34, 892000), datetime.datetime(2021, 10, 20, 12, 57, 35, 92000),
         datetime.datetime(2021, 10, 20, 12, 57, 35, 292000), datetime.datetime(2021, 10, 20, 12, 57, 35, 492000),
         datetime.datetime(2021, 10, 20, 12, 57, 35, 692000), datetime.datetime(2021, 10, 20, 12, 57, 35, 892000),
         datetime.datetime(2021, 10, 20, 12, 57, 36, 92000), datetime.datetime(2021, 10, 20, 12, 57, 36, 292000),
         datetime.datetime(2021, 10, 20, 12, 57, 36, 492000), datetime.datetime(2021, 10, 20, 12, 57, 36, 692000),
         datetime.datetime(2021, 10, 20, 12, 57, 36, 892000), datetime.datetime(2021, 10, 20, 12, 57, 37, 92000),
         datetime.datetime(2021, 10, 20, 12, 57, 37, 292000), datetime.datetime(2021, 10, 20, 12, 57, 37, 492000),
         datetime.datetime(2021, 10, 20, 12, 57, 37, 692000), datetime.datetime(2021, 10, 20, 12, 57, 37, 892000),
         datetime.datetime(2021, 10, 20, 12, 57, 38, 92000), datetime.datetime(2021, 10, 20, 12, 57, 38, 292000),
         datetime.datetime(2021, 10, 20, 12, 57, 38, 492000), datetime.datetime(2021, 10, 20, 12, 57, 38, 692000),
         datetime.datetime(2021, 10, 20, 12, 57, 38, 892000), datetime.datetime(2021, 10, 20, 12, 57, 39, 92000),
         datetime.datetime(2021, 10, 20, 12, 57, 39, 292000), datetime.datetime(2021, 10, 20, 12, 57, 39, 492000),
         datetime.datetime(2021, 10, 20, 12, 57, 39, 692000), datetime.datetime(2021, 10, 20, 12, 57, 39, 892000),
         datetime.datetime(2021, 10, 20, 12, 57, 40, 92000), datetime.datetime(2021, 10, 20, 12, 57, 40, 292000),
         datetime.datetime(2021, 10, 20, 12, 57, 40, 492000), datetime.datetime(2021, 10, 20, 12, 57, 40, 692000),
         datetime.datetime(2021, 10, 20, 12, 57, 40, 892000), datetime.datetime(2021, 10, 20, 12, 57, 41, 92000),
         datetime.datetime(2021, 10, 20, 12, 57, 41, 292000), datetime.datetime(2021, 10, 20, 12, 57, 41, 492000),
         datetime.datetime(2021, 10, 20, 12, 57, 41, 692000), datetime.datetime(2021, 10, 20, 12, 57, 41, 892000),
         datetime.datetime(2021, 10, 20, 12, 57, 42, 92000), datetime.datetime(2021, 10, 20, 12, 57, 42, 292000),
         datetime.datetime(2021, 10, 20, 12, 57, 42, 492000), datetime.datetime(2021, 10, 20, 12, 57, 42, 692000),
         datetime.datetime(2021, 10, 20, 12, 57, 42, 892000), datetime.datetime(2021, 10, 20, 12, 57, 43, 92000),
         datetime.datetime(2021, 10, 20, 12, 57, 43, 292000), datetime.datetime(2021, 10, 20, 12, 57, 43, 492000),
         datetime.datetime(2021, 10, 20, 12, 57, 43, 692000), datetime.datetime(2021, 10, 20, 12, 57, 43, 892000),
         datetime.datetime(2021, 10, 20, 12, 57, 44, 92000), datetime.datetime(2021, 10, 20, 12, 57, 44, 292000),
         datetime.datetime(2021, 10, 20, 12, 57, 44, 492000), datetime.datetime(2021, 10, 20, 12, 57, 44, 692000),
         datetime.datetime(2021, 10, 20, 12, 57, 44, 892000), datetime.datetime(2021, 10, 20, 12, 57, 45, 92000),
         datetime.datetime(2021, 10, 20, 12, 57, 45, 292000), datetime.datetime(2021, 10, 20, 12, 57, 45, 492000),
         datetime.datetime(2021, 10, 20, 12, 57, 45, 692000), datetime.datetime(2021, 10, 20, 12, 57, 45, 892000),
         datetime.datetime(2021, 10, 20, 12, 57, 46, 92000), datetime.datetime(2021, 10, 20, 12, 57, 46, 292000),
         datetime.datetime(2021, 10, 20, 12, 57, 46, 492000), datetime.datetime(2021, 10, 20, 12, 57, 46, 692000),
         datetime.datetime(2021, 10, 20, 12, 57, 46, 892000), datetime.datetime(2021, 10, 20, 12, 57, 47, 92000),
         datetime.datetime(2021, 10, 20, 12, 57, 47, 292000), datetime.datetime(2021, 10, 20, 12, 57, 47, 492000),
         datetime.datetime(2021, 10, 20, 12, 57, 47, 692000), datetime.datetime(2021, 10, 20, 12, 57, 47, 892000),
         datetime.datetime(2021, 10, 20, 12, 57, 48, 92000), datetime.datetime(2021, 10, 20, 12, 57, 48, 292000),
         datetime.datetime(2021, 10, 20, 12, 57, 48, 492000), datetime.datetime(2021, 10, 20, 12, 57, 48, 692000),
         datetime.datetime(2021, 10, 20, 12, 57, 48, 892000), datetime.datetime(2021, 10, 20, 12, 57, 49, 92000),
         datetime.datetime(2021, 10, 20, 12, 57, 49, 292000), datetime.datetime(2021, 10, 20, 12, 57, 49, 492000),
         datetime.datetime(2021, 10, 20, 12, 57, 49, 692000), datetime.datetime(2021, 10, 20, 12, 57, 49, 892000),
         datetime.datetime(2021, 10, 20, 12, 57, 50, 92000), datetime.datetime(2021, 10, 20, 12, 57, 50, 292000),
         datetime.datetime(2021, 10, 20, 12, 57, 50, 492000), datetime.datetime(2021, 10, 20, 12, 57, 50, 692000),
         datetime.datetime(2021, 10, 20, 12, 57, 50, 892000), datetime.datetime(2021, 10, 20, 12, 57, 51, 92000),
         datetime.datetime(2021, 10, 20, 12, 57, 51, 292000), datetime.datetime(2021, 10, 20, 12, 57, 51, 492000),
         datetime.datetime(2021, 10, 20, 12, 57, 51, 692000), datetime.datetime(2021, 10, 20, 12, 57, 51, 892000),
         datetime.datetime(2021, 10, 20, 12, 57, 52, 92000), datetime.datetime(2021, 10, 20, 12, 57, 52, 292000),
         datetime.datetime(2021, 10, 20, 12, 57, 52, 492000), datetime.datetime(2021, 10, 20, 12, 57, 52, 692000),
         datetime.datetime(2021, 10, 20, 12, 57, 52, 892000), datetime.datetime(2021, 10, 20, 12, 57, 53, 92000),
         datetime.datetime(2021, 10, 20, 12, 57, 53, 292000), datetime.datetime(2021, 10, 20, 12, 57, 53, 492000),
         datetime.datetime(2021, 10, 20, 12, 57, 53, 692000), datetime.datetime(2021, 10, 20, 12, 57, 53, 892000),
         datetime.datetime(2021, 10, 20, 12, 57, 54, 92000), datetime.datetime(2021, 10, 20, 12, 57, 54, 292000),
         datetime.datetime(2021, 10, 20, 12, 57, 54, 492000), datetime.datetime(2021, 10, 20, 12, 57, 54, 692000),
         datetime.datetime(2021, 10, 20, 12, 57, 54, 892000), datetime.datetime(2021, 10, 20, 12, 57, 55, 92000),
         datetime.datetime(2021, 10, 20, 12, 57, 55, 292000), datetime.datetime(2021, 10, 20, 12, 57, 55, 492000),
         datetime.datetime(2021, 10, 20, 12, 57, 55, 692000), datetime.datetime(2021, 10, 20, 12, 57, 55, 892000),
         datetime.datetime(2021, 10, 20, 12, 57, 56, 92000), datetime.datetime(2021, 10, 20, 12, 57, 56, 292000),
         datetime.datetime(2021, 10, 20, 12, 57, 56, 492000), datetime.datetime(2021, 10, 20, 12, 57, 56, 692000),
         datetime.datetime(2021, 10, 20, 12, 57, 56, 892000), datetime.datetime(2021, 10, 20, 12, 57, 57, 92000),
         datetime.datetime(2021, 10, 20, 12, 57, 57, 292000), datetime.datetime(2021, 10, 20, 12, 57, 57, 492000),
         datetime.datetime(2021, 10, 20, 12, 57, 57, 692000), datetime.datetime(2021, 10, 20, 12, 57, 57, 892000),
         datetime.datetime(2021, 10, 20, 12, 57, 58, 92000), datetime.datetime(2021, 10, 20, 12, 57, 58, 292000),
         datetime.datetime(2021, 10, 20, 12, 57, 58, 492000), datetime.datetime(2021, 10, 20, 12, 57, 58, 692000),
         datetime.datetime(2021, 10, 20, 12, 57, 58, 892000), datetime.datetime(2021, 10, 20, 12, 57, 59, 92000),
         datetime.datetime(2021, 10, 20, 12, 57, 59, 292000), datetime.datetime(2021, 10, 20, 12, 57, 59, 492000),
         datetime.datetime(2021, 10, 20, 12, 57, 59, 692000), datetime.datetime(2021, 10, 20, 12, 57, 59, 892000),
         datetime.datetime(2021, 10, 20, 12, 58, 0, 92000), datetime.datetime(2021, 10, 20, 12, 58, 0, 292000),
         datetime.datetime(2021, 10, 20, 12, 58, 0, 492000), datetime.datetime(2021, 10, 20, 12, 58, 0, 692000),
         datetime.datetime(2021, 10, 20, 12, 58, 0, 892000), datetime.datetime(2021, 10, 20, 12, 58, 1, 92000),
         datetime.datetime(2021, 10, 20, 12, 58, 1, 292000), datetime.datetime(2021, 10, 20, 12, 58, 1, 492000),
         datetime.datetime(2021, 10, 20, 12, 58, 1, 692000), datetime.datetime(2021, 10, 20, 12, 58, 1, 892000),
         datetime.datetime(2021, 10, 20, 12, 58, 2, 92000), datetime.datetime(2021, 10, 20, 12, 58, 2, 292000),
         datetime.datetime(2021, 10, 20, 12, 58, 2, 492000), datetime.datetime(2021, 10, 20, 12, 58, 2, 692000),
         datetime.datetime(2021, 10, 20, 12, 58, 2, 892000), datetime.datetime(2021, 10, 20, 12, 58, 3, 92000),
         datetime.datetime(2021, 10, 20, 12, 58, 3, 292000), datetime.datetime(2021, 10, 20, 12, 58, 3, 492000),
         datetime.datetime(2021, 10, 20, 12, 58, 3, 692000), datetime.datetime(2021, 10, 20, 12, 58, 3, 892000),
         datetime.datetime(2021, 10, 20, 12, 58, 4, 92000), datetime.datetime(2021, 10, 20, 12, 58, 4, 292000),
         datetime.datetime(2021, 10, 20, 12, 58, 4, 492000), datetime.datetime(2021, 10, 20, 12, 58, 4, 692000),
         datetime.datetime(2021, 10, 20, 12, 58, 4, 892000), datetime.datetime(2021, 10, 20, 12, 58, 5, 92000),
         datetime.datetime(2021, 10, 20, 12, 58, 5, 292000), datetime.datetime(2021, 10, 20, 12, 58, 5, 492000),
         datetime.datetime(2021, 10, 20, 12, 58, 5, 692000), datetime.datetime(2021, 10, 20, 12, 58, 5, 892000),
         datetime.datetime(2021, 10, 20, 12, 58, 6, 92000), datetime.datetime(2021, 10, 20, 12, 58, 6, 292000),
         datetime.datetime(2021, 10, 20, 12, 58, 6, 492000), datetime.datetime(2021, 10, 20, 12, 58, 6, 692000),
         datetime.datetime(2021, 10, 20, 12, 58, 6, 892000), datetime.datetime(2021, 10, 20, 12, 58, 7, 92000),
         datetime.datetime(2021, 10, 20, 12, 58, 7, 292000), datetime.datetime(2021, 10, 20, 12, 58, 7, 492000),
         datetime.datetime(2021, 10, 20, 12, 58, 7, 692000), datetime.datetime(2021, 10, 20, 12, 58, 7, 892000),
         datetime.datetime(2021, 10, 20, 12, 58, 8, 92000), datetime.datetime(2021, 10, 20, 12, 58, 8, 292000),
         datetime.datetime(2021, 10, 20, 12, 58, 8, 492000), datetime.datetime(2021, 10, 20, 12, 58, 8, 692000),
         datetime.datetime(2021, 10, 20, 12, 58, 8, 892000), datetime.datetime(2021, 10, 20, 12, 58, 9, 92000),
         datetime.datetime(2021, 10, 20, 12, 58, 9, 292000), datetime.datetime(2021, 10, 20, 12, 58, 9, 492000),
         datetime.datetime(2021, 10, 20, 12, 58, 9, 692000), datetime.datetime(2021, 10, 20, 12, 58, 9, 892000),
         datetime.datetime(2021, 10, 20, 12, 58, 10, 92000), datetime.datetime(2021, 10, 20, 12, 58, 10, 292000),
         datetime.datetime(2021, 10, 20, 12, 58, 10, 492000), datetime.datetime(2021, 10, 20, 12, 58, 10, 692000),
         datetime.datetime(2021, 10, 20, 12, 58, 10, 892000), datetime.datetime(2021, 10, 20, 12, 58, 11, 92000),
         datetime.datetime(2021, 10, 20, 12, 58, 11, 292000), datetime.datetime(2021, 10, 20, 12, 58, 11, 492000),
         datetime.datetime(2021, 10, 20, 12, 58, 11, 692000), datetime.datetime(2021, 10, 20, 12, 58, 11, 892000),
         datetime.datetime(2021, 10, 20, 12, 58, 12, 92000), datetime.datetime(2021, 10, 20, 12, 58, 12, 292000),
         datetime.datetime(2021, 10, 20, 12, 58, 12, 492000), datetime.datetime(2021, 10, 20, 12, 58, 12, 692000),
         datetime.datetime(2021, 10, 20, 12, 58, 12, 892000), datetime.datetime(2021, 10, 20, 12, 58, 13, 92000),
         datetime.datetime(2021, 10, 20, 12, 58, 13, 292000), datetime.datetime(2021, 10, 20, 12, 58, 13, 492000),
         datetime.datetime(2021, 10, 20, 12, 58, 13, 692000), datetime.datetime(2021, 10, 20, 12, 58, 13, 892000),
         datetime.datetime(2021, 10, 20, 12, 58, 14, 92000), datetime.datetime(2021, 10, 20, 12, 58, 14, 292000),
         datetime.datetime(2021, 10, 20, 12, 58, 14, 492000), datetime.datetime(2021, 10, 20, 12, 58, 14, 692000),
         datetime.datetime(2021, 10, 20, 12, 58, 14, 892000), datetime.datetime(2021, 10, 20, 12, 58, 15, 92000),
         datetime.datetime(2021, 10, 20, 12, 58, 15, 292000), datetime.datetime(2021, 10, 20, 12, 58, 15, 492000),
         datetime.datetime(2021, 10, 20, 12, 58, 15, 692000), datetime.datetime(2021, 10, 20, 12, 58, 15, 892000),
         datetime.datetime(2021, 10, 20, 12, 58, 16, 92000), datetime.datetime(2021, 10, 20, 12, 58, 16, 292000),
         datetime.datetime(2021, 10, 20, 12, 58, 16, 492000), datetime.datetime(2021, 10, 20, 12, 58, 16, 692000),
         datetime.datetime(2021, 10, 20, 12, 58, 16, 892000), datetime.datetime(2021, 10, 20, 12, 58, 17, 92000),
         datetime.datetime(2021, 10, 20, 12, 58, 17, 292000), datetime.datetime(2021, 10, 20, 12, 58, 17, 492000),
         datetime.datetime(2021, 10, 20, 12, 58, 17, 692000), datetime.datetime(2021, 10, 20, 12, 58, 17, 892000),
         datetime.datetime(2021, 10, 20, 12, 58, 18, 92000), datetime.datetime(2021, 10, 20, 12, 58, 18, 292000),
         datetime.datetime(2021, 10, 20, 12, 58, 18, 492000), datetime.datetime(2021, 10, 20, 12, 58, 18, 692000),
         datetime.datetime(2021, 10, 20, 12, 58, 18, 892000), datetime.datetime(2021, 10, 20, 12, 58, 19, 92000),
         datetime.datetime(2021, 10, 20, 12, 58, 19, 292000), datetime.datetime(2021, 10, 20, 12, 58, 19, 492000),
         datetime.datetime(2021, 10, 20, 12, 58, 19, 692000), datetime.datetime(2021, 10, 20, 12, 58, 19, 892000),
         datetime.datetime(2021, 10, 20, 12, 58, 20, 92000), datetime.datetime(2021, 10, 20, 12, 58, 20, 292000),
         datetime.datetime(2021, 10, 20, 12, 58, 20, 492000), datetime.datetime(2021, 10, 20, 12, 58, 20, 692000),
         datetime.datetime(2021, 10, 20, 12, 58, 20, 892000), datetime.datetime(2021, 10, 20, 12, 58, 21, 92000),
         datetime.datetime(2021, 10, 20, 12, 58, 21, 292000), datetime.datetime(2021, 10, 20, 12, 58, 21, 492000),
         datetime.datetime(2021, 10, 20, 12, 58, 21, 692000), datetime.datetime(2021, 10, 20, 12, 58, 21, 892000),
         datetime.datetime(2021, 10, 20, 12, 58, 22, 92000), datetime.datetime(2021, 10, 20, 12, 58, 22, 292000),
         datetime.datetime(2021, 10, 20, 12, 58, 22, 492000), datetime.datetime(2021, 10, 20, 12, 58, 22, 692000),
         datetime.datetime(2021, 10, 20, 12, 58, 22, 892000), datetime.datetime(2021, 10, 20, 12, 58, 23, 92000),
         datetime.datetime(2021, 10, 20, 12, 58, 23, 292000), datetime.datetime(2021, 10, 20, 12, 58, 23, 492000),
         datetime.datetime(2021, 10, 20, 12, 58, 23, 692000), datetime.datetime(2021, 10, 20, 12, 58, 23, 892000),
         datetime.datetime(2021, 10, 20, 12, 58, 24, 92000), datetime.datetime(2021, 10, 20, 12, 58, 24, 292000),
         datetime.datetime(2021, 10, 20, 12, 58, 24, 492000), datetime.datetime(2021, 10, 20, 12, 58, 24, 692000),
         datetime.datetime(2021, 10, 20, 12, 58, 24, 892000), datetime.datetime(2021, 10, 20, 12, 58, 25, 92000),
         datetime.datetime(2021, 10, 20, 12, 58, 25, 292000), datetime.datetime(2021, 10, 20, 12, 58, 25, 492000),
         datetime.datetime(2021, 10, 20, 12, 58, 25, 692000), datetime.datetime(2021, 10, 20, 12, 58, 25, 892000),
         datetime.datetime(2021, 10, 20, 12, 58, 26, 92000), datetime.datetime(2021, 10, 20, 12, 58, 26, 292000),
         datetime.datetime(2021, 10, 20, 12, 58, 26, 492000), datetime.datetime(2021, 10, 20, 12, 58, 26, 692000),
         datetime.datetime(2021, 10, 20, 12, 58, 26, 892000), datetime.datetime(2021, 10, 20, 12, 58, 27, 92000),
         datetime.datetime(2021, 10, 20, 12, 58, 27, 292000), datetime.datetime(2021, 10, 20, 12, 58, 27, 492000),
         datetime.datetime(2021, 10, 20, 12, 58, 27, 692000), datetime.datetime(2021, 10, 20, 12, 58, 27, 892000),
         datetime.datetime(2021, 10, 20, 12, 58, 28, 92000), datetime.datetime(2021, 10, 20, 12, 58, 28, 292000),
         datetime.datetime(2021, 10, 20, 12, 58, 28, 492000), datetime.datetime(2021, 10, 20, 12, 58, 28, 692000),
         datetime.datetime(2021, 10, 20, 12, 58, 28, 892000), datetime.datetime(2021, 10, 20, 12, 58, 29, 92000),
         datetime.datetime(2021, 10, 20, 12, 58, 29, 292000), datetime.datetime(2021, 10, 20, 12, 58, 29, 492000),
         datetime.datetime(2021, 10, 20, 12, 58, 29, 692000), datetime.datetime(2021, 10, 20, 12, 58, 29, 892000),
         datetime.datetime(2021, 10, 20, 12, 58, 30, 92000), datetime.datetime(2021, 10, 20, 12, 58, 30, 292000),
         datetime.datetime(2021, 10, 20, 12, 58, 30, 492000), datetime.datetime(2021, 10, 20, 12, 58, 30, 692000),
         datetime.datetime(2021, 10, 20, 12, 58, 30, 892000), datetime.datetime(2021, 10, 20, 12, 58, 31, 92000),
         datetime.datetime(2021, 10, 20, 12, 58, 31, 292000), datetime.datetime(2021, 10, 20, 12, 58, 31, 492000),
         datetime.datetime(2021, 10, 20, 12, 58, 31, 692000), datetime.datetime(2021, 10, 20, 12, 58, 31, 892000),
         datetime.datetime(2021, 10, 20, 12, 58, 32, 92000), datetime.datetime(2021, 10, 20, 12, 58, 32, 292000),
         datetime.datetime(2021, 10, 20, 12, 58, 32, 492000), datetime.datetime(2021, 10, 20, 12, 58, 32, 692000),
         datetime.datetime(2021, 10, 20, 12, 58, 32, 892000), datetime.datetime(2021, 10, 20, 12, 58, 33, 92000),
         datetime.datetime(2021, 10, 20, 12, 58, 33, 292000), datetime.datetime(2021, 10, 20, 12, 58, 33, 492000),
         datetime.datetime(2021, 10, 20, 12, 58, 33, 692000), datetime.datetime(2021, 10, 20, 12, 58, 33, 892000),
         datetime.datetime(2021, 10, 20, 12, 58, 34, 92000), datetime.datetime(2021, 10, 20, 12, 58, 34, 292000),
         datetime.datetime(2021, 10, 20, 12, 58, 34, 492000), datetime.datetime(2021, 10, 20, 12, 58, 34, 692000),
         datetime.datetime(2021, 10, 20, 12, 58, 34, 892000), datetime.datetime(2021, 10, 20, 12, 58, 35, 92000),
         datetime.datetime(2021, 10, 20, 12, 58, 35, 292000), datetime.datetime(2021, 10, 20, 12, 58, 35, 492000),
         datetime.datetime(2021, 10, 20, 12, 58, 35, 692000), datetime.datetime(2021, 10, 20, 12, 58, 35, 892000),
         datetime.datetime(2021, 10, 20, 12, 58, 36, 92000), datetime.datetime(2021, 10, 20, 12, 58, 36, 292000),
         datetime.datetime(2021, 10, 20, 12, 58, 36, 492000), datetime.datetime(2021, 10, 20, 12, 58, 36, 692000),
         datetime.datetime(2021, 10, 20, 12, 58, 36, 892000), datetime.datetime(2021, 10, 20, 12, 58, 37, 92000),
         datetime.datetime(2021, 10, 20, 12, 58, 37, 292000), datetime.datetime(2021, 10, 20, 12, 58, 37, 492000),
         datetime.datetime(2021, 10, 20, 12, 58, 37, 692000), datetime.datetime(2021, 10, 20, 12, 58, 37, 892000),
         datetime.datetime(2021, 10, 20, 12, 58, 38, 92000), datetime.datetime(2021, 10, 20, 12, 58, 38, 292000),
         datetime.datetime(2021, 10, 20, 12, 58, 38, 492000), datetime.datetime(2021, 10, 20, 12, 58, 38, 692000),
         datetime.datetime(2021, 10, 20, 12, 58, 38, 892000), datetime.datetime(2021, 10, 20, 12, 58, 39, 92000),
         datetime.datetime(2021, 10, 20, 12, 58, 39, 292000), datetime.datetime(2021, 10, 20, 12, 58, 39, 492000),
         datetime.datetime(2021, 10, 20, 12, 58, 39, 692000), datetime.datetime(2021, 10, 20, 12, 58, 39, 892000),
         datetime.datetime(2021, 10, 20, 12, 58, 40, 92000), datetime.datetime(2021, 10, 20, 12, 58, 40, 292000),
         datetime.datetime(2021, 10, 20, 12, 58, 40, 492000), datetime.datetime(2021, 10, 20, 12, 58, 40, 692000),
         datetime.datetime(2021, 10, 20, 12, 58, 40, 892000), datetime.datetime(2021, 10, 20, 12, 58, 41, 92000),
         datetime.datetime(2021, 10, 20, 12, 58, 41, 292000), datetime.datetime(2021, 10, 20, 12, 58, 41, 492000),
         datetime.datetime(2021, 10, 20, 12, 58, 41, 692000), datetime.datetime(2021, 10, 20, 12, 58, 41, 892000),
         datetime.datetime(2021, 10, 20, 12, 58, 42, 92000), datetime.datetime(2021, 10, 20, 12, 58, 42, 292000),
         datetime.datetime(2021, 10, 20, 12, 58, 42, 492000), datetime.datetime(2021, 10, 20, 12, 58, 42, 692000),
         datetime.datetime(2021, 10, 20, 12, 58, 42, 892000), datetime.datetime(2021, 10, 20, 12, 58, 43, 92000),
         datetime.datetime(2021, 10, 20, 12, 58, 43, 292000), datetime.datetime(2021, 10, 20, 12, 58, 43, 492000),
         datetime.datetime(2021, 10, 20, 12, 58, 43, 692000), datetime.datetime(2021, 10, 20, 12, 58, 43, 892000),
         datetime.datetime(2021, 10, 20, 12, 58, 44, 92000), datetime.datetime(2021, 10, 20, 12, 58, 44, 292000),
         datetime.datetime(2021, 10, 20, 12, 58, 44, 492000), datetime.datetime(2021, 10, 20, 12, 58, 44, 692000),
         datetime.datetime(2021, 10, 20, 12, 58, 44, 892000), datetime.datetime(2021, 10, 20, 12, 58, 45, 92000),
         datetime.datetime(2021, 10, 20, 12, 58, 45, 292000), datetime.datetime(2021, 10, 20, 12, 58, 45, 492000),
         datetime.datetime(2021, 10, 20, 12, 58, 45, 692000), datetime.datetime(2021, 10, 20, 12, 58, 45, 892000),
         datetime.datetime(2021, 10, 20, 12, 58, 46, 92000), datetime.datetime(2021, 10, 20, 12, 58, 46, 292000),
         datetime.datetime(2021, 10, 20, 12, 58, 46, 492000), datetime.datetime(2021, 10, 20, 12, 58, 46, 692000),
         datetime.datetime(2021, 10, 20, 12, 58, 46, 892000), datetime.datetime(2021, 10, 20, 12, 58, 47, 92000),
         datetime.datetime(2021, 10, 20, 12, 58, 47, 292000), datetime.datetime(2021, 10, 20, 12, 58, 47, 492000),
         datetime.datetime(2021, 10, 20, 12, 58, 47, 692000), datetime.datetime(2021, 10, 20, 12, 58, 47, 892000),
         datetime.datetime(2021, 10, 20, 12, 58, 48, 92000), datetime.datetime(2021, 10, 20, 12, 58, 48, 292000),
         datetime.datetime(2021, 10, 20, 12, 58, 48, 492000), datetime.datetime(2021, 10, 20, 12, 58, 48, 692000),
         datetime.datetime(2021, 10, 20, 12, 58, 48, 892000), datetime.datetime(2021, 10, 20, 12, 58, 49, 92000),
         datetime.datetime(2021, 10, 20, 12, 58, 49, 292000), datetime.datetime(2021, 10, 20, 12, 58, 49, 492000),
         datetime.datetime(2021, 10, 20, 12, 58, 49, 692000), datetime.datetime(2021, 10, 20, 12, 58, 49, 892000),
         datetime.datetime(2021, 10, 20, 12, 58, 50, 92000), datetime.datetime(2021, 10, 20, 12, 58, 50, 292000),
         datetime.datetime(2021, 10, 20, 12, 58, 50, 492000), datetime.datetime(2021, 10, 20, 12, 58, 50, 692000),
         datetime.datetime(2021, 10, 20, 12, 58, 50, 892000), datetime.datetime(2021, 10, 20, 12, 58, 51, 92000),
         datetime.datetime(2021, 10, 20, 12, 58, 51, 292000), datetime.datetime(2021, 10, 20, 12, 58, 51, 492000),
         datetime.datetime(2021, 10, 20, 12, 58, 51, 692000), datetime.datetime(2021, 10, 20, 12, 58, 51, 892000),
         datetime.datetime(2021, 10, 20, 12, 58, 52, 92000), datetime.datetime(2021, 10, 20, 12, 58, 52, 292000),
         datetime.datetime(2021, 10, 20, 12, 58, 52, 492000), datetime.datetime(2021, 10, 20, 12, 58, 52, 692000),
         datetime.datetime(2021, 10, 20, 12, 58, 52, 892000), datetime.datetime(2021, 10, 20, 12, 58, 53, 92000),
         datetime.datetime(2021, 10, 20, 12, 58, 53, 292000), datetime.datetime(2021, 10, 20, 12, 58, 53, 492000),
         datetime.datetime(2021, 10, 20, 12, 58, 53, 692000), datetime.datetime(2021, 10, 20, 12, 58, 53, 892000),
         datetime.datetime(2021, 10, 20, 12, 58, 54, 92000), datetime.datetime(2021, 10, 20, 12, 58, 54, 292000),
         datetime.datetime(2021, 10, 20, 12, 58, 54, 492000), datetime.datetime(2021, 10, 20, 12, 58, 54, 692000),
         datetime.datetime(2021, 10, 20, 12, 58, 54, 892000), datetime.datetime(2021, 10, 20, 12, 58, 55, 92000),
         datetime.datetime(2021, 10, 20, 12, 58, 55, 292000), datetime.datetime(2021, 10, 20, 12, 58, 55, 492000),
         datetime.datetime(2021, 10, 20, 12, 58, 55, 692000), datetime.datetime(2021, 10, 20, 12, 58, 55, 892000),
         datetime.datetime(2021, 10, 20, 12, 58, 56, 92000), datetime.datetime(2021, 10, 20, 12, 58, 56, 292000),
         datetime.datetime(2021, 10, 20, 12, 58, 56, 492000), datetime.datetime(2021, 10, 20, 12, 58, 56, 692000),
         datetime.datetime(2021, 10, 20, 12, 58, 56, 892000), datetime.datetime(2021, 10, 20, 12, 58, 57, 92000),
         datetime.datetime(2021, 10, 20, 12, 58, 57, 292000), datetime.datetime(2021, 10, 20, 12, 58, 57, 492000),
         datetime.datetime(2021, 10, 20, 12, 58, 57, 692000), datetime.datetime(2021, 10, 20, 12, 58, 57, 892000),
         datetime.datetime(2021, 10, 20, 12, 58, 58, 92000), datetime.datetime(2021, 10, 20, 12, 58, 58, 292000),
         datetime.datetime(2021, 10, 20, 12, 58, 58, 492000), datetime.datetime(2021, 10, 20, 12, 58, 58, 692000),
         datetime.datetime(2021, 10, 20, 12, 58, 58, 892000), datetime.datetime(2021, 10, 20, 12, 58, 59, 92000),
         datetime.datetime(2021, 10, 20, 12, 58, 59, 292000), datetime.datetime(2021, 10, 20, 12, 58, 59, 492000),
         datetime.datetime(2021, 10, 20, 12, 58, 59, 692000), datetime.datetime(2021, 10, 20, 12, 58, 59, 892000),
         datetime.datetime(2021, 10, 20, 12, 59, 0, 92000), datetime.datetime(2021, 10, 20, 12, 59, 0, 292000),
         datetime.datetime(2021, 10, 20, 12, 59, 0, 492000), datetime.datetime(2021, 10, 20, 12, 59, 0, 692000),
         datetime.datetime(2021, 10, 20, 12, 59, 0, 892000), datetime.datetime(2021, 10, 20, 12, 59, 1, 92000),
         datetime.datetime(2021, 10, 20, 12, 59, 1, 292000), datetime.datetime(2021, 10, 20, 12, 59, 1, 492000),
         datetime.datetime(2021, 10, 20, 12, 59, 1, 692000), datetime.datetime(2021, 10, 20, 12, 59, 1, 892000),
         datetime.datetime(2021, 10, 20, 12, 59, 2, 92000), datetime.datetime(2021, 10, 20, 12, 59, 2, 292000),
         datetime.datetime(2021, 10, 20, 12, 59, 2, 492000), datetime.datetime(2021, 10, 20, 12, 59, 2, 692000),
         datetime.datetime(2021, 10, 20, 12, 59, 2, 892000), datetime.datetime(2021, 10, 20, 12, 59, 3, 92000),
         datetime.datetime(2021, 10, 20, 12, 59, 3, 292000), datetime.datetime(2021, 10, 20, 12, 59, 3, 492000),
         datetime.datetime(2021, 10, 20, 12, 59, 3, 692000), datetime.datetime(2021, 10, 20, 12, 59, 3, 892000),
         datetime.datetime(2021, 10, 20, 12, 59, 4, 92000), datetime.datetime(2021, 10, 20, 12, 59, 4, 292000),
         datetime.datetime(2021, 10, 20, 12, 59, 4, 492000), datetime.datetime(2021, 10, 20, 12, 59, 4, 692000),
         datetime.datetime(2021, 10, 20, 12, 59, 4, 892000), datetime.datetime(2021, 10, 20, 12, 59, 5, 92000),
         datetime.datetime(2021, 10, 20, 12, 59, 5, 292000), datetime.datetime(2021, 10, 20, 12, 59, 5, 492000),
         datetime.datetime(2021, 10, 20, 12, 59, 5, 692000), datetime.datetime(2021, 10, 20, 12, 59, 5, 892000),
         datetime.datetime(2021, 10, 20, 12, 59, 6, 92000), datetime.datetime(2021, 10, 20, 12, 59, 6, 292000),
         datetime.datetime(2021, 10, 20, 12, 59, 6, 492000), datetime.datetime(2021, 10, 20, 12, 59, 6, 692000),
         datetime.datetime(2021, 10, 20, 12, 59, 6, 892000), datetime.datetime(2021, 10, 20, 12, 59, 7, 92000),
         datetime.datetime(2021, 10, 20, 12, 59, 7, 292000), datetime.datetime(2021, 10, 20, 12, 59, 7, 492000),
         datetime.datetime(2021, 10, 20, 12, 59, 7, 692000), datetime.datetime(2021, 10, 20, 12, 59, 7, 892000),
         datetime.datetime(2021, 10, 20, 12, 59, 8, 92000), datetime.datetime(2021, 10, 20, 12, 59, 8, 292000),
         datetime.datetime(2021, 10, 20, 12, 59, 8, 492000), datetime.datetime(2021, 10, 20, 12, 59, 8, 692000),
         datetime.datetime(2021, 10, 20, 12, 59, 8, 892000), datetime.datetime(2021, 10, 20, 12, 59, 9, 92000),
         datetime.datetime(2021, 10, 20, 12, 59, 9, 292000), datetime.datetime(2021, 10, 20, 12, 59, 9, 492000),
         datetime.datetime(2021, 10, 20, 12, 59, 9, 692000), datetime.datetime(2021, 10, 20, 12, 59, 9, 892000),
         datetime.datetime(2021, 10, 20, 12, 59, 10, 92000), datetime.datetime(2021, 10, 20, 12, 59, 10, 292000),
         datetime.datetime(2021, 10, 20, 12, 59, 10, 492000), datetime.datetime(2021, 10, 20, 12, 59, 10, 692000),
         datetime.datetime(2021, 10, 20, 12, 59, 10, 892000), datetime.datetime(2021, 10, 20, 12, 59, 11, 92000),
         datetime.datetime(2021, 10, 20, 12, 59, 11, 292000), datetime.datetime(2021, 10, 20, 12, 59, 11, 492000),
         datetime.datetime(2021, 10, 20, 12, 59, 11, 692000), datetime.datetime(2021, 10, 20, 12, 59, 11, 892000),
         datetime.datetime(2021, 10, 20, 12, 59, 12, 92000), datetime.datetime(2021, 10, 20, 12, 59, 12, 292000),
         datetime.datetime(2021, 10, 20, 12, 59, 12, 492000), datetime.datetime(2021, 10, 20, 12, 59, 12, 692000),
         datetime.datetime(2021, 10, 20, 12, 59, 12, 892000), datetime.datetime(2021, 10, 20, 12, 59, 13, 92000),
         datetime.datetime(2021, 10, 20, 12, 59, 13, 292000), datetime.datetime(2021, 10, 20, 12, 59, 13, 492000),
         datetime.datetime(2021, 10, 20, 12, 59, 13, 692000), datetime.datetime(2021, 10, 20, 12, 59, 13, 892000),
         datetime.datetime(2021, 10, 20, 12, 59, 14, 92000), datetime.datetime(2021, 10, 20, 12, 59, 14, 292000),
         datetime.datetime(2021, 10, 20, 12, 59, 14, 492000), datetime.datetime(2021, 10, 20, 12, 59, 14, 692000),
         datetime.datetime(2021, 10, 20, 12, 59, 14, 892000), datetime.datetime(2021, 10, 20, 12, 59, 15, 92000),
         datetime.datetime(2021, 10, 20, 12, 59, 15, 292000), datetime.datetime(2021, 10, 20, 12, 59, 15, 492000),
         datetime.datetime(2021, 10, 20, 12, 59, 15, 692000), datetime.datetime(2021, 10, 20, 12, 59, 15, 892000),
         datetime.datetime(2021, 10, 20, 12, 59, 16, 92000), datetime.datetime(2021, 10, 20, 12, 59, 16, 292000),
         datetime.datetime(2021, 10, 20, 12, 59, 16, 492000), datetime.datetime(2021, 10, 20, 12, 59, 16, 692000),
         datetime.datetime(2021, 10, 20, 12, 59, 16, 892000), datetime.datetime(2021, 10, 20, 12, 59, 17, 92000),
         datetime.datetime(2021, 10, 20, 12, 59, 17, 292000), datetime.datetime(2021, 10, 20, 12, 59, 17, 492000),
         datetime.datetime(2021, 10, 20, 12, 59, 17, 692000), datetime.datetime(2021, 10, 20, 12, 59, 17, 892000),
         datetime.datetime(2021, 10, 20, 12, 59, 18, 92000), datetime.datetime(2021, 10, 20, 12, 59, 18, 292000),
         datetime.datetime(2021, 10, 20, 12, 59, 18, 492000), datetime.datetime(2021, 10, 20, 12, 59, 18, 692000),
         datetime.datetime(2021, 10, 20, 12, 59, 18, 892000), datetime.datetime(2021, 10, 20, 12, 59, 19, 92000),
         datetime.datetime(2021, 10, 20, 12, 59, 19, 292000), datetime.datetime(2021, 10, 20, 12, 59, 19, 492000),
         datetime.datetime(2021, 10, 20, 12, 59, 19, 692000), datetime.datetime(2021, 10, 20, 12, 59, 19, 892000),
         datetime.datetime(2021, 10, 20, 12, 59, 20, 92000), datetime.datetime(2021, 10, 20, 12, 59, 20, 292000),
         datetime.datetime(2021, 10, 20, 12, 59, 20, 492000), datetime.datetime(2021, 10, 20, 12, 59, 20, 692000),
         datetime.datetime(2021, 10, 20, 12, 59, 20, 892000), datetime.datetime(2021, 10, 20, 12, 59, 21, 92000),
         datetime.datetime(2021, 10, 20, 12, 59, 21, 292000), datetime.datetime(2021, 10, 20, 12, 59, 21, 492000),
         datetime.datetime(2021, 10, 20, 12, 59, 21, 692000), datetime.datetime(2021, 10, 20, 12, 59, 21, 892000),
         datetime.datetime(2021, 10, 20, 12, 59, 22, 92000), datetime.datetime(2021, 10, 20, 12, 59, 22, 292000),
         datetime.datetime(2021, 10, 20, 12, 59, 22, 492000), datetime.datetime(2021, 10, 20, 12, 59, 22, 692000),
         datetime.datetime(2021, 10, 20, 12, 59, 22, 892000), datetime.datetime(2021, 10, 20, 12, 59, 23, 92000),
         datetime.datetime(2021, 10, 20, 12, 59, 23, 292000), datetime.datetime(2021, 10, 20, 12, 59, 23, 492000),
         datetime.datetime(2021, 10, 20, 12, 59, 23, 692000), datetime.datetime(2021, 10, 20, 12, 59, 23, 892000),
         datetime.datetime(2021, 10, 20, 12, 59, 24, 92000), datetime.datetime(2021, 10, 20, 12, 59, 24, 292000),
         datetime.datetime(2021, 10, 20, 12, 59, 24, 492000), datetime.datetime(2021, 10, 20, 12, 59, 24, 692000),
         datetime.datetime(2021, 10, 20, 12, 59, 24, 892000), datetime.datetime(2021, 10, 20, 12, 59, 25, 92000),
         datetime.datetime(2021, 10, 20, 12, 59, 25, 292000), datetime.datetime(2021, 10, 20, 12, 59, 25, 492000),
         datetime.datetime(2021, 10, 20, 12, 59, 25, 692000), datetime.datetime(2021, 10, 20, 12, 59, 25, 892000),
         datetime.datetime(2021, 10, 20, 12, 59, 26, 92000), datetime.datetime(2021, 10, 20, 12, 59, 26, 292000),
         datetime.datetime(2021, 10, 20, 12, 59, 26, 492000), datetime.datetime(2021, 10, 20, 12, 59, 26, 692000),
         datetime.datetime(2021, 10, 20, 12, 59, 26, 892000), datetime.datetime(2021, 10, 20, 12, 59, 27, 92000),
         datetime.datetime(2021, 10, 20, 12, 59, 27, 292000), datetime.datetime(2021, 10, 20, 12, 59, 27, 492000),
         datetime.datetime(2021, 10, 20, 12, 59, 27, 692000), datetime.datetime(2021, 10, 20, 12, 59, 27, 892000),
         datetime.datetime(2021, 10, 20, 12, 59, 28, 92000), datetime.datetime(2021, 10, 20, 12, 59, 28, 292000),
         datetime.datetime(2021, 10, 20, 12, 59, 28, 492000), datetime.datetime(2021, 10, 20, 12, 59, 28, 692000),
         datetime.datetime(2021, 10, 20, 12, 59, 28, 892000), datetime.datetime(2021, 10, 20, 12, 59, 29, 92000),
         datetime.datetime(2021, 10, 20, 12, 59, 29, 292000), datetime.datetime(2021, 10, 20, 12, 59, 29, 492000),
         datetime.datetime(2021, 10, 20, 12, 59, 29, 692000), datetime.datetime(2021, 10, 20, 12, 59, 29, 892000),
         datetime.datetime(2021, 10, 20, 12, 59, 30, 92000), datetime.datetime(2021, 10, 20, 12, 59, 30, 292000),
         datetime.datetime(2021, 10, 20, 12, 59, 30, 492000), datetime.datetime(2021, 10, 20, 12, 59, 30, 692000),
         datetime.datetime(2021, 10, 20, 12, 59, 30, 892000), datetime.datetime(2021, 10, 20, 12, 59, 31, 92000),
         datetime.datetime(2021, 10, 20, 12, 59, 31, 292000), datetime.datetime(2021, 10, 20, 12, 59, 31, 492000),
         datetime.datetime(2021, 10, 20, 12, 59, 31, 692000), datetime.datetime(2021, 10, 20, 12, 59, 31, 892000),
         datetime.datetime(2021, 10, 20, 12, 59, 32, 92000), datetime.datetime(2021, 10, 20, 12, 59, 32, 292000),
         datetime.datetime(2021, 10, 20, 12, 59, 32, 492000), datetime.datetime(2021, 10, 20, 12, 59, 32, 692000),
         datetime.datetime(2021, 10, 20, 12, 59, 32, 892000), datetime.datetime(2021, 10, 20, 12, 59, 33, 92000),
         datetime.datetime(2021, 10, 20, 12, 59, 33, 292000), datetime.datetime(2021, 10, 20, 12, 59, 33, 492000),
         datetime.datetime(2021, 10, 20, 12, 59, 33, 692000), datetime.datetime(2021, 10, 20, 12, 59, 33, 892000),
         datetime.datetime(2021, 10, 20, 12, 59, 34, 92000), datetime.datetime(2021, 10, 20, 12, 59, 34, 292000),
         datetime.datetime(2021, 10, 20, 12, 59, 34, 492000), datetime.datetime(2021, 10, 20, 12, 59, 34, 692000),
         datetime.datetime(2021, 10, 20, 12, 59, 34, 892000), datetime.datetime(2021, 10, 20, 12, 59, 35, 92000),
         datetime.datetime(2021, 10, 20, 12, 59, 35, 292000), datetime.datetime(2021, 10, 20, 12, 59, 35, 492000),
         datetime.datetime(2021, 10, 20, 12, 59, 35, 692000), datetime.datetime(2021, 10, 20, 12, 59, 35, 892000),
         datetime.datetime(2021, 10, 20, 12, 59, 36, 92000), datetime.datetime(2021, 10, 20, 12, 59, 36, 292000),
         datetime.datetime(2021, 10, 20, 12, 59, 36, 492000), datetime.datetime(2021, 10, 20, 12, 59, 36, 692000),
         datetime.datetime(2021, 10, 20, 12, 59, 36, 892000), datetime.datetime(2021, 10, 20, 12, 59, 37, 92000),
         datetime.datetime(2021, 10, 20, 12, 59, 37, 292000), datetime.datetime(2021, 10, 20, 12, 59, 37, 492000),
         datetime.datetime(2021, 10, 20, 12, 59, 37, 692000), datetime.datetime(2021, 10, 20, 12, 59, 37, 892000),
         datetime.datetime(2021, 10, 20, 12, 59, 38, 92000), datetime.datetime(2021, 10, 20, 12, 59, 38, 292000),
         datetime.datetime(2021, 10, 20, 12, 59, 38, 492000), datetime.datetime(2021, 10, 20, 12, 59, 38, 692000),
         datetime.datetime(2021, 10, 20, 12, 59, 38, 892000), datetime.datetime(2021, 10, 20, 12, 59, 39, 92000),
         datetime.datetime(2021, 10, 20, 12, 59, 39, 292000), datetime.datetime(2021, 10, 20, 12, 59, 39, 492000),
         datetime.datetime(2021, 10, 20, 12, 59, 39, 692000), datetime.datetime(2021, 10, 20, 12, 59, 39, 892000),
         datetime.datetime(2021, 10, 20, 12, 59, 40, 92000), datetime.datetime(2021, 10, 20, 12, 59, 40, 292000),
         datetime.datetime(2021, 10, 20, 12, 59, 40, 492000), datetime.datetime(2021, 10, 20, 12, 59, 40, 692000),
         datetime.datetime(2021, 10, 20, 12, 59, 40, 892000), datetime.datetime(2021, 10, 20, 12, 59, 41, 92000),
         datetime.datetime(2021, 10, 20, 12, 59, 41, 292000), datetime.datetime(2021, 10, 20, 12, 59, 41, 492000),
         datetime.datetime(2021, 10, 20, 12, 59, 41, 692000), datetime.datetime(2021, 10, 20, 12, 59, 41, 892000),
         datetime.datetime(2021, 10, 20, 12, 59, 42, 92000), datetime.datetime(2021, 10, 20, 12, 59, 42, 292000),
         datetime.datetime(2021, 10, 20, 12, 59, 42, 492000), datetime.datetime(2021, 10, 20, 12, 59, 42, 692000),
         datetime.datetime(2021, 10, 20, 12, 59, 42, 892000), datetime.datetime(2021, 10, 20, 12, 59, 43, 92000),
         datetime.datetime(2021, 10, 20, 12, 59, 43, 292000), datetime.datetime(2021, 10, 20, 12, 59, 43, 492000),
         datetime.datetime(2021, 10, 20, 12, 59, 43, 692000), datetime.datetime(2021, 10, 20, 12, 59, 43, 892000),
         datetime.datetime(2021, 10, 20, 12, 59, 44, 92000), datetime.datetime(2021, 10, 20, 12, 59, 44, 292000),
         datetime.datetime(2021, 10, 20, 12, 59, 44, 492000), datetime.datetime(2021, 10, 20, 12, 59, 44, 692000),
         datetime.datetime(2021, 10, 20, 12, 59, 44, 892000), datetime.datetime(2021, 10, 20, 12, 59, 45, 92000),
         datetime.datetime(2021, 10, 20, 12, 59, 45, 292000), datetime.datetime(2021, 10, 20, 12, 59, 45, 492000),
         datetime.datetime(2021, 10, 20, 12, 59, 45, 692000), datetime.datetime(2021, 10, 20, 12, 59, 45, 892000),
         datetime.datetime(2021, 10, 20, 12, 59, 46, 92000), datetime.datetime(2021, 10, 20, 12, 59, 46, 292000),
         datetime.datetime(2021, 10, 20, 12, 59, 46, 492000), datetime.datetime(2021, 10, 20, 12, 59, 46, 692000),
         datetime.datetime(2021, 10, 20, 12, 59, 46, 892000), datetime.datetime(2021, 10, 20, 12, 59, 47, 92000),
         datetime.datetime(2021, 10, 20, 12, 59, 47, 292000), datetime.datetime(2021, 10, 20, 12, 59, 47, 492000),
         datetime.datetime(2021, 10, 20, 12, 59, 47, 692000), datetime.datetime(2021, 10, 20, 12, 59, 47, 892000),
         datetime.datetime(2021, 10, 20, 12, 59, 48, 92000), datetime.datetime(2021, 10, 20, 12, 59, 48, 292000),
         datetime.datetime(2021, 10, 20, 12, 59, 48, 492000), datetime.datetime(2021, 10, 20, 12, 59, 48, 692000),
         datetime.datetime(2021, 10, 20, 12, 59, 48, 892000), datetime.datetime(2021, 10, 20, 12, 59, 49, 92000),
         datetime.datetime(2021, 10, 20, 12, 59, 49, 292000), datetime.datetime(2021, 10, 20, 12, 59, 49, 492000),
         datetime.datetime(2021, 10, 20, 12, 59, 49, 692000), datetime.datetime(2021, 10, 20, 12, 59, 49, 892000),
         datetime.datetime(2021, 10, 20, 12, 59, 50, 92000), datetime.datetime(2021, 10, 20, 12, 59, 50, 292000),
         datetime.datetime(2021, 10, 20, 12, 59, 50, 492000), datetime.datetime(2021, 10, 20, 12, 59, 50, 692000),
         datetime.datetime(2021, 10, 20, 12, 59, 50, 892000), datetime.datetime(2021, 10, 20, 12, 59, 51, 92000),
         datetime.datetime(2021, 10, 20, 12, 59, 51, 292000), datetime.datetime(2021, 10, 20, 12, 59, 51, 492000),
         datetime.datetime(2021, 10, 20, 12, 59, 51, 692000), datetime.datetime(2021, 10, 20, 12, 59, 51, 892000),
         datetime.datetime(2021, 10, 20, 12, 59, 52, 92000), datetime.datetime(2021, 10, 20, 12, 59, 52, 292000),
         datetime.datetime(2021, 10, 20, 12, 59, 52, 492000), datetime.datetime(2021, 10, 20, 12, 59, 52, 692000),
         datetime.datetime(2021, 10, 20, 12, 59, 52, 892000), datetime.datetime(2021, 10, 20, 12, 59, 53, 92000),
         datetime.datetime(2021, 10, 20, 12, 59, 53, 292000), datetime.datetime(2021, 10, 20, 12, 59, 53, 492000),
         datetime.datetime(2021, 10, 20, 12, 59, 53, 692000), datetime.datetime(2021, 10, 20, 12, 59, 53, 892000),
         datetime.datetime(2021, 10, 20, 12, 59, 54, 92000), datetime.datetime(2021, 10, 20, 12, 59, 54, 292000),
         datetime.datetime(2021, 10, 20, 12, 59, 54, 492000), datetime.datetime(2021, 10, 20, 12, 59, 54, 692000),
         datetime.datetime(2021, 10, 20, 12, 59, 54, 892000), datetime.datetime(2021, 10, 20, 12, 59, 55, 92000),
         datetime.datetime(2021, 10, 20, 12, 59, 55, 292000), datetime.datetime(2021, 10, 20, 12, 59, 55, 492000),
         datetime.datetime(2021, 10, 20, 12, 59, 55, 692000), datetime.datetime(2021, 10, 20, 12, 59, 55, 892000),
         datetime.datetime(2021, 10, 20, 12, 59, 56, 92000), datetime.datetime(2021, 10, 20, 12, 59, 56, 292000),
         datetime.datetime(2021, 10, 20, 12, 59, 56, 492000), datetime.datetime(2021, 10, 20, 12, 59, 56, 692000),
         datetime.datetime(2021, 10, 20, 12, 59, 56, 892000), datetime.datetime(2021, 10, 20, 12, 59, 57, 92000),
         datetime.datetime(2021, 10, 20, 12, 59, 57, 292000), datetime.datetime(2021, 10, 20, 12, 59, 57, 492000),
         datetime.datetime(2021, 10, 20, 12, 59, 57, 692000), datetime.datetime(2021, 10, 20, 12, 59, 57, 892000),
         datetime.datetime(2021, 10, 20, 12, 59, 58, 92000), datetime.datetime(2021, 10, 20, 12, 59, 58, 292000),
         datetime.datetime(2021, 10, 20, 12, 59, 58, 492000), datetime.datetime(2021, 10, 20, 12, 59, 58, 692000),
         datetime.datetime(2021, 10, 20, 12, 59, 58, 892000), datetime.datetime(2021, 10, 20, 12, 59, 59, 92000),
         datetime.datetime(2021, 10, 20, 12, 59, 59, 292000), datetime.datetime(2021, 10, 20, 12, 59, 59, 492000),
         datetime.datetime(2021, 10, 20, 12, 59, 59, 692000), datetime.datetime(2021, 10, 20, 12, 59, 59, 892000),
         datetime.datetime(2021, 10, 20, 13, 0, 0, 92000), datetime.datetime(2021, 10, 20, 13, 0, 0, 292000),
         datetime.datetime(2021, 10, 20, 13, 0, 0, 492000), datetime.datetime(2021, 10, 20, 13, 0, 0, 692000),
         datetime.datetime(2021, 10, 20, 13, 0, 0, 892000), datetime.datetime(2021, 10, 20, 13, 0, 1, 92000),
         datetime.datetime(2021, 10, 20, 13, 0, 1, 292000), datetime.datetime(2021, 10, 20, 13, 0, 1, 492000),
         datetime.datetime(2021, 10, 20, 13, 0, 1, 692000), datetime.datetime(2021, 10, 20, 13, 0, 1, 892000),
         datetime.datetime(2021, 10, 20, 13, 0, 2, 92000), datetime.datetime(2021, 10, 20, 13, 0, 2, 292000),
         datetime.datetime(2021, 10, 20, 13, 0, 2, 492000), datetime.datetime(2021, 10, 20, 13, 0, 2, 692000),
         datetime.datetime(2021, 10, 20, 13, 0, 2, 892000), datetime.datetime(2021, 10, 20, 13, 0, 3, 92000),
         datetime.datetime(2021, 10, 20, 13, 0, 3, 292000), datetime.datetime(2021, 10, 20, 13, 0, 3, 492000),
         datetime.datetime(2021, 10, 20, 13, 0, 3, 692000), datetime.datetime(2021, 10, 20, 13, 0, 3, 892000)]
    # y = [y1, y2, y3]
    y = [4726.0, 3519.0, 4626.0, 3795.0, 4310.0, 4332.0, 4613.0, 4393.0, 4406.0, 4285.0, 4132.0, 4395.0, 3883.0, 3963.0,
         4146.0, 4645.0, 4688.0, 3740.0, 3964.0, 4361.0, 4613.0, 4648.0, 3700.0, 4904.0, 4343.0, 3940.0, 3836.0, 3610.0,
         4327.0, 3898.0, 4921.0, 4819.0, 4287.0, 4426.0, 4981.0, 3551.0, 4110.0, 4737.0, 3830.0, 3524.0, 4113.0, 4972.0,
         4372.0, 3740.0, 3738.0, 4630.0, 3927.0, 4289.0, 3576.0, 4033.0, 4747.0, 4861.0, 4007.0, 4097.0, 4148.0, 4700.0,
         4726.0, 4915.0, 4636.0, 4017.0, 3582.0, 4972.0, 3554.0, 3663.0, 4472.0, 3641.0, 4723.0, 3571.0, 3559.0, 3528.0,
         4083.0, 4248.0, 3939.0, 4475.0, 4359.0, 4215.0, 3951.0, 3880.0, 3959.0, 3750.0, 4233.0, 4660.0, 4727.0, 4722.0,
         3812.0, 3882.0, 4642.0, 3882.0, 4254.0, 4469.0, 3746.0, 4616.0, 3647.0, 4970.0, 4900.0, 4710.0, 4200.0, 3564.0,
         4933.0, 4771.0, 3817.0, 3875.0, 3724.0, 4374.0, 3558.0, 3810.0, 3577.0, 4003.0, 3828.0, 4124.0, 4730.0, 4663.0,
         3814.0, 4251.0, 3826.0, 4551.0, 4075.0, 4267.0, 4924.0, 3730.0, 4253.0, 3790.0, 3780.0, 4483.0, 4313.0, 3744.0,
         4402.0, 4251.0, 3888.0, 4260.0, 4593.0, 4824.0, 4751.0, 3625.0, 4893.0, 4304.0, 4394.0, 3920.0, 4901.0, 4158.0,
         4722.0, 4095.0, 4979.0, 4660.0, 4816.0, 3726.0, 3839.0, 3915.0, 3948.0, 4353.0, 3657.0, 4482.0, 3736.0, 4194.0,
         3766.0, 4919.0, 4989.0, 4419.0, 4297.0, 3847.0, 3871.0, 4821.0, 3610.0, 4454.0, 4266.0, 3695.0, 4733.0, 3904.0,
         4519.0, 4115.0, 4121.0, 4924.0, 4520.0, 3732.0, 4981.0, 3980.0, 4388.0, 4065.0, 4770.0, 4145.0, 4682.0, 4656.0,
         3884.0, 4794.0, 3660.0, 3953.0, 4145.0, 4716.0, 4286.0, 4528.0, 4180.0, 4166.0, 3936.0, 3525.0, 3688.0, 3990.0,
         4299.0, 4260.0, 4388.0, 4395.0, 4039.0, 4853.0, 4722.0, 3519.0, 4259.0, 4138.0, 4636.0, 4619.0, 4022.0, 3786.0,
         4304.0, 4545.0, 4557.0, 3985.0, 4447.0, 3995.0, 4817.0, 4134.0, 3580.0, 3598.0, 4694.0, 4961.0, 4777.0, 3788.0,
         4818.0, 4340.0, 3574.0, 4579.0, 3698.0, 3922.0, 4267.0, 4513.0, 4278.0, 3605.0, 4140.0, 3687.0, 3640.0, 3732.0,
         4391.0, 4066.0, 4371.0, 3871.0, 4756.0, 3678.0, 4773.0, 3799.0, 3652.0, 3641.0, 4579.0, 3952.0, 4832.0, 3555.0,
         4847.0, 4406.0, 4194.0, 4248.0, 4491.0, 3623.0, 3540.0, 4102.0, 4037.0, 3832.0, 4303.0, 3991.0, 3519.0, 4422.0,
         4160.0, 3695.0, 4143.0, 3622.0, 4027.0, 3889.0, 3824.0, 4687.0, 4955.0, 4769.0, 3959.0, 4267.0, 4588.0, 4731.0,
         4261.0, 4208.0, 3672.0, 4007.0, 4391.0, 4361.0, 3869.0, 3735.0, 4456.0, 3655.0, 3510.0, 4796.0, 4505.0, 3733.0,
         4749.0, 3683.0, 4459.0, 4633.0, 4386.0, 4085.0, 3600.0, 4186.0, 4024.0, 4058.0, 3866.0, 4877.0, 4899.0, 4207.0,
         4162.0, 4613.0, 3785.0, 4695.0, 4047.0, 4837.0, 3913.0, 4407.0, 4729.0, 4989.0, 4317.0, 3810.0, 3674.0, 4627.0,
         3561.0, 3562.0, 4432.0, 4006.0, 4269.0, 4974.0, 4409.0, 3554.0, 3771.0, 3744.0, 4524.0, 4648.0, 3618.0, 4139.0,
         3727.0, 4064.0, 3540.0, 3576.0, 4999.0, 3592.0, 4890.0, 4187.0, 4717.0, 3897.0, 4721.0, 4830.0, 3893.0, 4477.0,
         3860.0, 4235.0, 4226.0, 4109.0, 4695.0, 4848.0, 4795.0, 4229.0, 4645.0, 4769.0, 4946.0, 4202.0, 4093.0, 4381.0,
         4941.0, 3973.0, 4071.0, 4414.0, 4416.0, 4830.0, 4578.0, 4234.0, 4216.0, 4382.0, 4981.0, 3584.0, 4779.0, 3503.0,
         4996.0, 3656.0, 4798.0, 4372.0, 4406.0, 3659.0, 4495.0, 4290.0, 4946.0, 3963.0, 3503.0, 4657.0, 4355.0, 3584.0,
         4342.0, 3945.0, 4700.0, 3939.0, 4134.0, 4088.0, 4593.0, 4663.0, 4464.0, 3717.0, 3520.0, 4743.0, 4357.0, 3606.0,
         4364.0, 4116.0, 4799.0, 4734.0, 4915.0, 4542.0, 4197.0, 3823.0, 4771.0, 4982.0, 4731.0, 4872.0, 4601.0, 4536.0,
         3569.0, 3693.0, 4986.0, 4547.0, 3904.0, 4458.0, 3860.0, 4543.0, 4470.0, 4405.0, 4992.0, 4132.0, 4506.0, 4283.0,
         4276.0, 3518.0, 4856.0, 4225.0, 4197.0, 3639.0, 3536.0, 4801.0, 4558.0, 4657.0, 3638.0, 4781.0, 4965.0, 3709.0,
         4313.0, 4044.0, 4301.0, 3757.0, 3515.0, 4754.0, 4388.0, 3931.0, 4141.0, 4595.0, 4579.0, 3729.0, 4671.0, 4394.0,
         4817.0, 4986.0, 4261.0, 4924.0, 4619.0, 4583.0, 4553.0, 4654.0, 4040.0, 3535.0, 4181.0, 3675.0, 3639.0, 4985.0,
         4308.0, 4028.0, 4207.0, 3611.0, 3647.0, 4671.0, 4715.0, 4798.0, 4406.0, 4511.0, 4528.0, 4494.0, 3750.0, 4782.0,
         4866.0, 3587.0, 4191.0, 4412.0, 4925.0, 4617.0, 4783.0, 4141.0, 4843.0, 4576.0, 4246.0, 4567.0, 4419.0, 3692.0,
         4964.0, 3723.0, 4231.0, 3779.0, 4207.0, 4574.0, 3593.0, 4630.0, 4405.0, 3686.0, 4326.0, 4082.0, 4584.0, 4705.0,
         3596.0, 4404.0, 4657.0, 4475.0, 4642.0, 3799.0, 4577.0, 3894.0, 3850.0, 4639.0, 4235.0, 4177.0, 3602.0, 4457.0,
         4416.0, 4494.0, 4263.0, 3626.0, 3808.0, 4806.0, 4833.0, 4446.0, 4050.0, 3735.0, 3785.0, 4730.0, 4776.0, 3506.0,
         4937.0, 4319.0, 3735.0, 4416.0, 4033.0, 3512.0, 4106.0, 4097.0, 4009.0, 4119.0, 3643.0, 3555.0, 4899.0, 4148.0,
         4523.0, 3658.0, 3684.0, 4444.0, 3984.0, 3732.0, 3787.0, 4938.0, 4450.0, 3712.0, 3644.0, 3717.0, 3920.0, 4420.0,
         3780.0, 4048.0, 4702.0, 3548.0, 4540.0, 4264.0, 4468.0, 3606.0, 4550.0, 4504.0, 4490.0, 4624.0, 4225.0, 3908.0,
         4466.0, 4803.0, 3704.0, 4144.0, 4132.0, 3649.0, 3641.0, 4118.0, 3993.0, 3710.0, 4484.0, 3893.0, 3628.0, 3997.0,
         4889.0, 4891.0, 3924.0, 4834.0, 4332.0, 4271.0, 4800.0, 3982.0, 4296.0, 3912.0, 4744.0, 4357.0, 3967.0, 4734.0,
         4093.0, 4807.0, 4599.0, 4804.0, 3735.0, 4549.0, 4992.0, 4372.0, 4998.0, 3788.0, 3904.0, 4752.0, 3969.0, 3760.0,
         4729.0, 4754.0, 4586.0, 4065.0, 3552.0, 4697.0, 3922.0, 3935.0, 4722.0, 3849.0, 3540.0, 4111.0, 4766.0, 3987.0,
         3817.0, 4786.0, 4929.0, 3807.0, 4924.0, 4338.0, 4812.0, 4521.0, 4758.0, 4354.0, 4873.0, 4697.0, 4359.0, 4650.0,
         4289.0, 4092.0, 4128.0, 4102.0, 4297.0, 4650.0, 4072.0, 4421.0, 3532.0, 4145.0, 3568.0, 3753.0, 4957.0, 4942.0,
         4971.0, 4973.0, 4434.0, 4602.0, 4228.0, 4585.0, 4460.0, 4181.0, 4612.0, 4082.0, 4167.0, 3582.0, 3690.0, 3537.0,
         4510.0, 4325.0, 3820.0, 3751.0, 4089.0, 3771.0, 3596.0, 3981.0, 4006.0, 3615.0, 4013.0, 3982.0, 3674.0, 4920.0,
         3558.0, 4531.0, 3732.0, 4665.0, 4014.0, 4264.0, 4156.0, 4305.0, 4383.0, 3810.0, 3757.0, 4585.0, 4515.0, 4011.0,
         4336.0, 3566.0, 3856.0, 4898.0, 4370.0, 3836.0, 4432.0, 3800.0, 4931.0, 3853.0, 4743.0, 3835.0, 4015.0, 4755.0,
         4843.0, 4657.0, 3570.0, 4631.0, 4916.0, 3753.0, 4127.0, 3769.0, 3561.0, 4095.0, 3855.0, 3704.0, 4967.0, 4921.0,
         3750.0, 3672.0, 3910.0, 4174.0, 4096.0, 4053.0, 4871.0, 3939.0, 3700.0, 4145.0, 3658.0, 4457.0, 3540.0, 3630.0,
         3900.0, 3938.0, 3542.0, 4897.0, 4648.0, 4251.0, 3744.0, 4863.0, 4522.0, 3548.0, 3793.0, 4494.0, 4382.0, 3550.0,
         4538.0, 4705.0, 3621.0, 3647.0, 4407.0, 4122.0, 3838.0, 4179.0, 4773.0, 4284.0, 4952.0, 4364.0, 4203.0, 4402.0,
         4278.0, 4458.0, 3733.0, 3561.0, 4256.0, 4009.0, 3547.0, 4227.0, 4963.0, 3849.0, 4212.0, 3720.0, 3740.0, 3604.0,
         4272.0, 3543.0, 3698.0, 4734.0, 4733.0, 4688.0, 4810.0, 4556.0, 4352.0, 4666.0, 4752.0, 4529.0, 3751.0, 3878.0,
         3829.0, 4725.0, 4545.0, 3878.0, 4858.0, 4022.0, 4719.0, 4654.0, 3652.0, 4578.0, 3673.0, 3603.0, 3773.0, 3819.0,
         4599.0, 4452.0, 3709.0, 3961.0, 4060.0, 4373.0, 4995.0, 4110.0, 4196.0, 4791.0, 3764.0, 4646.0, 4553.0, 4326.0,
         4995.0, 4216.0, 4345.0, 3917.0, 4128.0, 4397.0, 4186.0, 3796.0, 3570.0, 3717.0, 3976.0, 4102.0, 4557.0, 4070.0,
         3798.0, 3620.0, 4507.0, 4875.0, 4053.0, 4151.0, 4243.0, 4979.0, 4985.0, 3768.0, 3631.0, 3506.0, 4137.0, 4069.0,
         4545.0, 4963.0, 4266.0, 4697.0, 3716.0, 4614.0, 3875.0, 4034.0, 3780.0, 3846.0, 3871.0, 4735.0, 3794.0, 4947.0,
         4651.0, 4189.0, 3780.0, 4710.0, 3986.0, 4531.0, 4609.0, 4105.0, 3565.0, 4249.0, 3815.0, 4551.0, 4862.0, 4984.0,
         4137.0, 4865.0, 4024.0, 3841.0, 4121.0, 3744.0, 4671.0, 4479.0, 3517.0, 3800.0, 3913.0, 3755.0, 3910.0, 4559.0,
         4218.0, 4669.0, 4341.0, 4919.0, 4997.0, 3528.0, 4100.0, 4041.0, 4873.0, 3580.0, 3938.0, 4046.0, 3552.0, 4340.0,
         4046.0, 3731.0, 3756.0, 3561.0, 4164.0, 4571.0, 3739.0, 4218.0, 4091.0, 3888.0, 4799.0, 4036.0, 4467.0, 4074.0,
         4413.0, 4731.0, 4083.0, 3668.0, 4386.0, 4663.0, 4404.0, 4372.0, 4274.0, 3823.0, 4690.0, 4564.0, 4690.0, 3803.0,
         3822.0, 4160.0, 4110.0, 4890.0, 3787.0, 3890.0, 4730.0, 4253.0, 3682.0, 4830.0, 4829.0, 3911.0, 4172.0, 4083.0,
         4990.0, 4356.0, 4999.0, 3668.0, 4752.0, 3762.0, 4669.0, 4446.0, 4035.0, 4096.0, 4596.0, 4361.0, 4981.0, 3941.0,
         4960.0, 3779.0, 3509.0, 3651.0, 4956.0, 4457.0, 3705.0, 4150.0, 4001.0, 4237.0, 4875.0, 4182.0, 4717.0, 3678.0,
         4418.0, 4205.0, 4397.0, 4962.0, 4541.0, 4167.0, 4911.0, 4111.0, 4660.0, 4197.0, 4399.0, 4581.0, 4847.0, 4513.0,
         3895.0, 3970.0, 3629.0, 3760.0, 4312.0, 3898.0, 4915.0, 3791.0, 4771.0, 4666.0, 3516.0, 3996.0, 4387.0, 3693.0,
         3744.0, 4092.0, 4022.0, 3685.0, 3645.0, 4634.0, 3682.0, 4947.0, 4453.0, 4057.0, 3865.0, 3867.0, 3727.0, 3884.0,
         3807.0, 3766.0, 4020.0, 4424.0, 3577.0, 3970.0, 3746.0, 4709.0, 4377.0, 4607.0, 4642.0, 4391.0, 4062.0, 4219.0,
         4300.0, 4468.0, 3678.0, 4548.0, 4557.0, 3920.0, 4649.0, 4951.0, 4343.0, 4232.0, 4619.0, 3856.0, 4474.0, 4585.0,
         4021.0, 3865.0, 4818.0, 3974.0, 4438.0, 4674.0, 4289.0, 4615.0, 4035.0, 4065.0, 4672.0, 3572.0, 3611.0, 4581.0,
         4657.0, 4092.0, 3788.0, 4116.0, 4608.0, 3746.0, 4093.0, 4242.0, 4728.0, 4145.0, 4465.0, 4636.0, 4786.0, 4764.0,
         4515.0, 3925.0, 4155.0, 4580.0, 4954.0, 4738.0, 4628.0, 4973.0, 3656.0, 4028.0, 4052.0, 4251.0, 4855.0, 3626.0,
         4920.0, 4326.0, 3837.0, 4849.0, 3922.0, 4854.0, 3904.0, 3539.0, 3605.0, 3741.0, 4758.0, 3999.0, 4808.0, 4248.0,
         3811.0, 4665.0, 3980.0, 3899.0, 3596.0, 4006.0, 3518.0, 4541.0, 4315.0, 4918.0, 3837.0, 4685.0, 4637.0, 4630.0,
         4901.0, 4842.0, 4412.0, 4841.0, 4457.0, 4214.0, 3535.0, 3529.0, 3745.0, 3546.0, 4915.0, 4936.0, 4780.0, 4397.0,
         4119.0, 4373.0, 4036.0, 4482.0, 4878.0, 4201.0, 4878.0, 4659.0, 4436.0, 3932.0, 4215.0, 4385.0, 4013.0, 4855.0,
         4757.0, 4148.0, 4255.0, 4714.0, 3521.0, 4553.0, 4215.0, 4467.0, 3731.0, 4248.0, 4635.0, 3620.0, 4102.0, 4754.0,
         4253.0, 3611.0, 4037.0, 4134.0, 4185.0, 4308.0, 4469.0, 3869.0, 3666.0, 3528.0, 4943.0, 4518.0, 3669.0, 4708.0,
         4007.0, 4917.0, 3956.0, 3828.0, 4021.0, 3577.0, 4165.0, 3915.0, 4778.0, 3911.0, 3784.0, 4612.0, 4519.0, 3914.0,
         3728.0, 4444.0, 3806.0, 4679.0, 4008.0, 3900.0, 3836.0, 4807.0, 4819.0, 3893.0, 4975.0, 4978.0, 3593.0, 3980.0,
         4098.0, 3736.0, 4632.0, 4078.0, 3672.0, 4939.0, 3769.0, 4170.0, 3645.0, 4641.0, 4678.0, 4118.0, 4780.0, 3507.0,
         4321.0, 3801.0, 4190.0, 3688.0, 4885.0, 4683.0, 3892.0, 3534.0, 4012.0, 4490.0, 4177.0, 4079.0, 4037.0, 3910.0,
         4180.0, 3613.0, 4284.0, 4112.0, 3904.0, 4895.0, 4417.0, 4363.0, 3981.0, 4575.0, 4291.0, 4586.0, 4883.0, 3556.0,
         3797.0, 3690.0, 4181.0, 4511.0, 3706.0, 4010.0, 4845.0, 4832.0, 4663.0, 4443.0, 4331.0, 4090.0, 3902.0, 4971.0,
         3654.0, 4966.0, 4602.0, 4968.0, 4114.0, 4906.0, 4488.0, 4534.0, 3975.0, 3685.0, 4747.0, 4873.0, 4023.0, 3768.0,
         3670.0, 4295.0, 3835.0, 4134.0, 4474.0, 4715.0, 4586.0, 3979.0, 4996.0, 4128.0, 4210.0, 4183.0, 4523.0, 4663.0,
         3955.0, 3744.0, 3720.0, 3709.0, 3670.0, 4388.0, 4523.0, 4224.0, 4682.0, 4525.0, 4297.0, 4382.0, 4500.0, 4581.0,
         3657.0, 4261.0, 3779.0, 4604.0, 4431.0, 3683.0, 4090.0, 3665.0, 4078.0, 4699.0, 4540.0, 4883.0, 4309.0, 4018.0,
         3604.0, 4891.0, 4566.0, 4513.0, 4497.0, 4332.0, 3892.0, 4199.0, 4605.0, 4080.0, 4418.0, 4227.0, 4225.0, 4033.0,
         4955.0, 4587.0, 3825.0, 3752.0, 3632.0, 4907.0, 4857.0, 4595.0, 3940.0, 4295.0, 3872.0, 3991.0, 4036.0, 4744.0,
         4234.0, 4895.0, 4103.0, 3897.0, 4081.0, 4541.0, 4020.0, 4800.0, 4751.0, 3830.0, 4877.0, 3827.0, 4447.0, 4704.0,
         3603.0, 4760.0, 3525.0, 3600.0, 4867.0, 4792.0, 3613.0, 4302.0, 4704.0, 4366.0, 4214.0, 3838.0, 4714.0, 4943.0,
         3928.0, 4679.0, 3694.0, 3674.0, 4939.0, 4794.0, 4150.0, 4428.0, 3916.0, 3681.0, 4810.0, 4248.0, 4543.0, 4120.0,
         4429.0, 4372.0, 4602.0, 4817.0, 4882.0, 4163.0, 3959.0, 4573.0, 4043.0, 3599.0, 4403.0, 3579.0, 3523.0, 4166.0,
         3956.0, 4160.0, 3898.0, 3618.0, 3553.0, 4595.0, 4971.0, 4056.0, 3672.0, 4421.0, 3863.0, 4530.0, 4079.0, 3947.0,
         4144.0, 4869.0, 3532.0, 3989.0, 4802.0, 3561.0, 4670.0, 3953.0, 3519.0, 3596.0, 4008.0, 4946.0, 4770.0, 3965.0,
         4664.0, 3701.0, 4122.0, 3730.0, 4288.0, 4434.0, 4964.0, 3959.0, 4092.0, 3697.0, 3615.0, 4749.0, 4918.0, 4904.0,
         4949.0, 3779.0, 4227.0, 4003.0, 4261.0, 4016.0, 4827.0, 3819.0, 3528.0, 3766.0, 4515.0, 4968.0, 4796.0, 4910.0,
         4471.0, 3710.0, 4081.0, 4311.0, 4520.0, 3655.0, 4808.0, 3980.0, 4124.0, 4582.0, 4881.0, 3771.0, 3589.0, 4315.0,
         3585.0, 4003.0, 3644.0, 4423.0, 4059.0, 4074.0, 4756.0, 4973.0, 4706.0, 4482.0, 4782.0, 4302.0, 4262.0, 4624.0,
         3571.0, 4057.0]
    ax = []
    p.plot(
        x=t, y=y, ax=None,
        marker='.', markersize=2.25, linestyle='-', lw=1.125,
        label="", ylabel='Altitude in meter [m]'
    )
    for k in range(len(y)):
        label = r'Graph Num.: ' + str(k)
        ax.append(
            p.plot(
                x=t, y=y[k], ax=None,
                # x=t, y=y, ax=None,
                marker='.', markersize=2.25, linestyle='-', lw=1.125,
                label=k, ylabel='Altitude in meter [m]'
            )
        )
    plt.show()
    # save plotted figure as .png file
    filepath = './fig/demo.svg'
    p.savefig(filepath=filepath)

# %% EOF
