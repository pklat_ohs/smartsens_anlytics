from re import A
from odoo import api, fields, models
from odoo.exceptions import Warning, ValidationError
from .plotify import Plotify
# import matplotlib.pyplot as plt
# plt.switch_backend('agg')
from PIL import Image
import numpy as np
import base64
import glob, os, csv, io
from io import BytesIO, StringIO
from shutil import copyfile, copy, move
from datetime import datetime,timedelta, date  # used to change the format of datetime field
import psycopg2
import colorama
from colorama import Fore
from .RemoteImporters import RemoteImport
from .ImportActivity.CSVintigrtaetsChecker import CsvIntegrity

# Schedule action in Odoo einfügen und  mit dem Button test
# folgender Befehl löst die Function zum Synchronisieren der der aus
# model.action_SynchronizeData()

class Launche(models.Model):
    _name = "smartsense.launche" # Tabel names müssen immer kleingeschrieben sein und dürfen keine großen Buchstaben beinhalten 
    _description = 'SmartSense Analytics Launches'
    # Hie kommet ein context mit dem alle Attribute an das Frontend weiter geben werden könenn.
    # LaunchID = fields.Char(string="Test-ID", help="Die ID wird generiert")
    
    
    file_LocalPath = fields.Char(string="File local path")
    file_RemotePath = fields.Char(string="File Remote-path")
    scheduleActivityID = fields.One2many("smartsens.import_schedule_activity","id")
        
    TFM_RowID = fields.Integer(string="TF_RowID", help="Das ist eine ID die aus der Datenbank von dem FFT kommt und als Identifiere dient um Datensätze nicht doppelt ei zu fügen " )
    name = fields.Char(compute="_genName",store=False)
    

    # Launch
    launchDate = fields.Date(string='Date', help="Wann wurde der Start ausgeführt?")
    launchTime = fields.Datetime(string="Time", help="Um welche Zeit ist das Gleiter abgehoben")
    duration = fields.Float(string="Duration", help="Dauer der Aufzeichnung")

    # Airfield-data
    airfield = fields.Many2one('smartsense.analytics.airfield', string="Airfield Name", help="Auf welchen Flugplatz wurde der Test ausgeführt")
    AirfieldrunWayLength = fields.Integer(related='airfield.runWayLength', string='Length [m]')
    AirfieldAltitude = fields.Float(related='airfield.altitude', string="Mean sea level [m]", help="Wie hoch über dem Meeresspiegel ist das Airfield?")
    AirfieldrunwayDirection = fields.Float(related="airfield.runwayDirection",string="Direction °", help="Nach wie viel Grad ist das Feld ausgerichtet?")

    # Pilot Data
    pilot = fields.Many2one('res.partner', string='Name', help="Wer ist der Pilot?")
    pilotRemark = fields.Text(string='Pilot Remark', help="Gibt es von Pilot irgendwelche Kommentare zu dem start?")
    # ---
    remarkState = fields.Selection([('RemarkOK', 'Remark OK'), ('pilotRemarkTooFast', 'Remark Too Fast'), ('pilotRemarkTooSlow', 'Remark Too Slow')],
                             string="Remark Status", default="RemarkOK")
    
    winchOperator = fields.Many2one('res.partner', string='Winch Operator', help="Welche Person bediente die Seilwinde")
    winchType = fields.Many2one('smartsenese.winchtype', string="Winch Type", help="Welcher Seilzug von Type wird verwendet")
    # Die Attribute aus "AIRCRAFT" und ihren Attributen
    aircraft = fields.Many2one("smartsense.aircraft", string="Aircraft",help="Welches Luftfahrzeug wird gestartet?")
    aircraftCallSign = fields.Char(string="Aircraft Call Sign", help="Ein Flugzeugrufzeichen ist eine Gruppe Luft-Boden-Kommunikation verwendet werden.")
    # AircraftModelbezeichnung = fields.Char(related="aircraft.modelbezeichnung",string="Modellbezeichnung", help="Welche Modellbezeichnung das Flugzeug trägt.")
    # AircraftTragweitenflaeche = fields.Float(related="aircraft.tragweitenflaeche", string="Tragweitenflaeche", help="Welchen Tragweitenflächen das Flugzeug besitzt")
    AircraftWeight = fields.Float(related="aircraft.emptyAircraftWeight", string="Aircraft Weight", help="Welches Gewicht besitzt das Flugzeug")
    doubleSeated = fields.Boolean(string="Double Seated",help="Handelt es sich um ein Zweisitzter oder um einen Einsitzter ") # doppelsitzter
    # calcAircraftWeight = fields.Float(string="Calc aircraft weight")

    # Tabelle AIRCRAFTTYPE
    #aircrafType = fields.Integer(related="aircraft.aircraftype.id", string="ID-AircraftType", help="Welche ID hat der AircraftType")
    #aircrafTypename = fields.Char(related="aircraft.name", string="ID-AircraftType", help="Welche ID hat der AircraftType" ) # ID
    # aircrafemptyAircraftWeight = fields.Integer(related="aircrafType.emptyAircraftWeight", string="ID-AircraftType", help="Welche ID hat der AircraftType" ) # ID

    #aircrafemptyAircraftWeight = fields.Integer(related="aircrafType.emptyAircraftWeight", string="ID-AircraftType", help="Welche ID hat der AircraftType" ) # ID
    # aircraftTypeName = fields.Char(related="aircrafType.name")
    # aircraftEmptyAircraftWeight = fields.Float(related="aircrafType.emptyAircraftWeight",string="Flugzeuggewicht",help="Welche Gewicht besitzt das Flugzeug ohne Besatzung")

    # record = fields.One2many("data.record","StreamID",string="Data-Records")
    data = fields.One2many("smartsens.sensordata.stream","Altitude",string="Data-Records")
    # Force attribute
    forceMax = fields.Float(string="Force Max", help="Höchster erhaltener Seilkraft-Wert")
    forceMin = fields.Float(compute="_ForceMinDn",string="Force Min", help="Niedrigster erhaltener Seilkraft-Wert",store=True)
    forceStart = fields.Float(compute="_ForceStart",string="Force Start", help="Niedrigster erhaltener Seilkraft-Wert",store=True)
    forceEnd = fields.Float(compute="_ForceEnd",string="Force End", help="Niedrigster erhaltener Seilkraft-Wert",store=True)
    acceleration = fields.Float(string="Acceleration", help="Beschleunigung")  # Beschleunigung

    # Altitudewerte
    altitudeMax = fields.Float(compute="_altitudeMax", string="Altitude Max",
                               help="Maximal erreichte höhe während des starts",store=True)  # GPS-Daten?
    altitudeMin = fields.Float(compute="_altitudeMin", string="Altitude Min", help="Minimale Höhe die bei der aufzeichnung recorded wurde",store=True)
    altitudeStart = fields.Float(compute="_altitudeStart", string="Altitude Start", help="",store=True)
    altitudeEnd = fields.Float(compute="_altitudeEnd", string="Altitude End", help="",store=True)
    totalImpulse = fields.Float(compute="_totalImpulse", string="Total Impulse", help="GesamtImpulse",store=True)    

    # Windcomponents
    windDirection = fields.Float(string="Direction °", help="Windrichtung in Grad")  # degree
    windSpeed = fields.Float(string="Speed m/s", help="Windgeschwindigkeit")  # m/s
    windComponent = fields.Float(string="Component m/s")  # m/s

    # RSSI -- Received Signal Strength Indicator
    rssiMin = fields.Integer(compute="_RSSI_Min", string="RSSI Min", help="Niedrigster empfangene Signalstärke",store=True)
    rssiMax = fields.Integer(compute="_RSSI_Max", string="RSSI Max", help="Höchster empfangene Signalstärke",store=True)
    rssiAverage = fields.Float(compute="_RSSI_Avg", string="Average", help="Was war der durchschnittliche RSSI-Wert",store=True)
    packetsLost = fields.Float(string="Packets Lost", help="Packet lost percentage")
    failureCode = fields.Integer(string='Failure Code')

    temperatur = fields.Float(string="Temperature C°")
    flightID = fields.Integer(string='FlightID', help="Um welche Flugidentifikationsnummer handelt es sich?")
    sensorID = fields.Integer(string="SensorID", help="Welcher Sensor mit welche ID wird verwendet?")

    # PLots aller Diagramme zu denn erstellten Reports
    plotAltitude_Force_ConForce = fields.Binary(compute='_plotAltitude_Force_ConForce', store=False)
    PlotRSSI = fields.Binary(compute='_PlotRSSI', store=False)    
    SensorRecords = fields.One2many("smartsens.sensordata.stream", "Launch_id", string="Sensor-Data", index=True)
    
    
    # your_field = field.Integer(string="SST", default=lambda self: self.env['ir.sequence'].next_by_code('increment_your_field')
    
    

    #data = fields.Binary(string="CSV-File: Sensor-Data")
    sambaFilePaths = []
    HostDestinationFiles = []
    #FileDestinationPath = r"C:\Program Files\Odoo 14.0.20210907\custom\SmartSenseAnalytics\models\Data\**\*"
    #FileDestinationPath4Transfer = r"C:\Program Files\Odoo 14.0.20210907\custom\SmartSenseAnalytics\models\Data"
    #sambaSourcePath = r'\\192.168.178.39\home\Workspace\Projects\SmartSenseAnalytics\SmartSensAnalytics\SmartSenseDataStore\**\*'
    missingFiles = []

    def _genName(self):
        for r in self:
            r.name = str(r.airfield.name) +" --> "+ str(r.launchDate)

    def action_SynchronizeData(self):
        #SyncData()
        self.getMissingSambaFiles()
        self.importCsvData()
        print("Synchronized Data")

    def GenerateFromDataNewDatetimeObjects(self, data, date):
        LaunchDateDateObjekt = datetime.strptime(date, "%Y-%m-%d")  # hier wird das Datetime objekt erstellt
        # newDateTimeForOdoo = LaunchDateDateObjekt.strftime("%Y-%m-%d %H:%M:%S.%f")
        tempTime = ""
        newDataFrame = []
        for j in data:
            tempTime = LaunchDateDateObjekt + timedelta(milliseconds=int(j))
            newDataFrame.append(tempTime)
        return newDataFrame

    def _plotAltitude_Force_ConForce(self, **args):
        for r in self:        
            selectSensorData = self.env['smartsens.sensordata.stream'].search([('Launch_id','=',r.id)])        
            SensorData = []
            tStampX = []
            for i in selectSensorData:
                #print(f"ID:{i.id}",f"das ist nur i {i}","LEN:", len(self.SensorRecords),i.Tfm_RowID,i.Altitude,i.TargetForce,i.Cons_Force)
                SensorData.append([i.Tfm_RowID,
                                i.timeStamp,
                                i.Altitude,
                                i.TargetForce,
                                i.Cons_Force])
            # Daten sortieren nach der RowID aus den CSV-Files
            def takeRowID(elem):
                return elem[0]
            SensorData.sort(key=takeRowID)

            p = Plotify(debug=True)
            AltitudeY, TargetForce, Con_Force = [], [], []
            ax = []
            for i in SensorData:
                # print(i)
                tStampX.append(i[1])
                AltitudeY.append(i[2])
                TargetForce.append(i[3])
                Con_Force.append(i[4])

            # Umwandeln der TimeSerie in DatetimeObjekte
            tStampX2 = self.GenerateFromDataNewDatetimeObjects(data=tStampX, date=str(self.launchDate))
            yDataSet = [AltitudeY, TargetForce, Con_Force]
            dataLabelsSet = ["Altitude", "Target Force", "Consolidated Force"]

            for k in range(len(yDataSet)):
                # label = r'Graph Num.: ' + str(k)
                label = dataLabelsSet[k]
                ax.append(
                    p.plot(
                        x=tStampX2, y=yDataSet[k], ax=None,
                        # x=t, y=y, ax=None,
                        marker='.', markersize=2.25, linestyle='-', lw=1.125,
                        label=label, ylabel='Altitude in meter [m] | Force [daN]'
                    )
                )
            r.write({'plotAltitude_Force_ConForce': p.pass_buff_fig(p.fig)})

    def _plotClimbingRate(self):
        for r in self:
            p = Plotify(debug=True)
            SensorData = []
            tStampX = []
            for i in r.SensorRecords:
                # print(i, len(self.SensorRecords),i.Tfm_RowID,i.Altitude,i.TargetForce,)
                SensorData.append([i.Tfm_RowID, i.timeStamp, i.Altitude])
            def takeRowID(elem):
                return elem[0]
            SensorData.sort(key=takeRowID)
            Altitude = []
            ClimbingRate = []
            ax = []
            for i in SensorData:
                # print("das ist der ClimbingRate-algorithms:", i)
                tStampX.append(i[1])
                Altitude.append(i[2])
            tStampX2 = r.GenerateFromDataNewDatetimeObjects(data=tStampX, date=str(self.launchDate))
            # Calculate the Climbingrate from Data and find an algo for named RoC(Rate of Climbing-Algrorythmen)
            # self.roCAlgorythmus(self):
            yDataSet = [Altitude]
            for k in range(len(yDataSet)):
                label = r'Graph Num.: ' + str(k)
                ax.append(
                    p.plot(
                        x=tStampX2, y=yDataSet[k], ax=None,
                        # x=t, y=y, ax=None,
                        marker='.', markersize=2.25, linestyle='-', lw=1.125,
                        label=k, ylabel='Climbingrate in meter [m/s]'
                    )
                )
            r.write({'plotClimbingRate': p.pass_buff_fig(p.fig)})

    def _PlotRSSI(self):
        SensorData = []
        tStampX = []
        for r in self:
            for i in r.SensorRecords:
                # print(i, len(self.SensorRecords),i.Tfm_RowID,i.Altitude,i.TargetForce,)
                SensorData.append([i.Tfm_RowID, i.timeStamp, i.RSSI, i.PacketsLost])
            def takeRowID(elem):
                return elem[0]
            SensorData.sort(key=takeRowID)
            p = Plotify(debug=True)
            RSSI, PacketsLost = [], []
            ax = []
            for i in SensorData:
                # print("das ist der RSSI-plot:", i)
                tStampX.append(i[1])
                RSSI.append(i[2])
                PacketsLost.append(i[3])
            tStampX2 = r.GenerateFromDataNewDatetimeObjects(data=tStampX, date=str(r.launchDate))
            label = ["RSSI","Packets Lost"]
            yDataSet = [RSSI, PacketsLost]
            for k in range(len(yDataSet)):
                label = r'Graph Num.: ' + str(k)
                ax.append(
                    p.plot(label=label[k],
                        x=tStampX2, y=yDataSet[k], ax=None,
                        # x=t, y=y, ax=None,
                        marker='.', markersize=2.25, linestyle='-', lw=1.125,
                        ylabel='RSSI,Packetsvc in meter [m]'
                    )
                )
            r.write({'PlotRSSI': p.pass_buff_fig(p.fig)})

    def _ForceMinDn(self):
        for r in self:
            selectSensorData = self.env['smartsens.sensordata.stream'].search([('Launch_id', '=', r.id)])
            Force = []
            for i in selectSensorData:
                Force.append(i.TargetForce)
            try:
                r.forceMin = min(Force)
            except ValueError:
                 r.forceMin = 0

    def _ForceEnd(self):
        for r in self:
            SensorData = self.env['smartsens.sensordata.stream'].search([('Launch_id', '=', r.id)])
            Force = []
            for i in SensorData:
                Force.append(i.TargetForce)
                # print(i.TargetForce)
            # print("ForceEnd",Force)
            try:        
                if len(Force) > 0:
                    r.forceEnd = sum(Force[-10:-1:1]) / len(Force[-10:-1:1])                             
            except ZeroDivisionError:
                r.forceEnd = 0
                raise
            
       

    def _ForceStart(self):
        for r in self:
            SensorData = self.env['smartsens.sensordata.stream'].search([('Launch_id', '=', r.id)])
            Force = []
            for i in SensorData:
                Force.append(i.TargetForce)
            try:        
                if len(Force) > 0:
                    r.forceStart = sum(Force[0:9:1]) / len(Force[0:9:1])                              
            except ZeroDivisionError:
                r.forceStart = 0
                raise


    # Computefields um die Altitude auszurechnen Beta
    def _altitudeMax(self):
        for r in self:
            altitude = []
            SensorData = self.env['smartsens.sensordata.stream'].search([('TFM_ID', '=', r.TFM_RowID)])
            for i in SensorData:         
                altitude.append(i.Altitude)
            try :
                r.altitudeMax = max(altitude)
                altitude.clear()
            except ValueError as a:
                r.altitudeMax = 0


    def _altitudeMin(self):                  
        for r in self:
            SensorData = self.env['smartsens.sensordata.stream'].search([('Launch_id', '=', r.id)])
            altitude = []
            for i in SensorData:
                altitude.append(i.Altitude)
            try :
                if len(altitude) > 0:
                    r.altitudeMin = min(altitude)
                    altitude.clear()
                else:
                    r.altitudeMin = 0
            except ValueError as a:
                r.altitudeMin = 0

    def _altitudeStart(self):
        for r in self:
            SensorData = self.env['smartsens.sensordata.stream'].search([('Launch_id', '=', r.id)])
            altitude = []
            for i in SensorData:
                altitude.append(i.Altitude)
            if len(altitude) > 0:
                r.altitudeStart = sum(altitude[0:10:1]) / len(altitude[0:10:1])
                altitude.clear()
            else:
                r.altitudeStart = 0

    def _altitudeEnd(self):
        for r in self:
            SensorData = self.env['smartsens.sensordata.stream'].search([('Launch_id', '=', r.id)])
            altitude = []
            for i in SensorData:
                altitude.append(i.Altitude)
            if len(altitude) >0:                
                r.altitudeEnd = sum(altitude[-10:-1:1]) / len(altitude[-10:-1:1])
            else:
                r.altitudeEnd = 0
                


    def _totalImpulse(self):        
            for r in self:
                SensorData = self.env['smartsens.sensordata.stream'].search([('Launch_id', '=', r.id)])
                impulse = []
                for i in SensorData:
                    impulse.append(i.RSSI)
                if len(impulse) > 0:
                    r.totalImpulse = len(impulse)
                    impulse.clear()
                else:
                    r.totalImpulse = 0
                
                

    def _RSSI_Max(self):                
        for r in self:
            SensorData = self.env['smartsens.sensordata.stream'].search([('Launch_id', '=', r.id)])
            Rssi = []
            for i in SensorData:
                Rssi.append(i.RSSI)
            if len(Rssi) > 0:
                r.rssiMax = len(Rssi)
                Rssi.clear()
            else:
                r.rssiMax = 0
            

 
    def _RSSI_Min(self):
        for r in self:
            SensorData = self.env['smartsens.sensordata.stream'].search([('Launch_id', '=', r.id)])
            RSSI = []
            for i in SensorData:
                RSSI.append(i.RSSI)
            if len(RSSI) > 0:
                r.rssiMin = min(RSSI)
                RSSI.clear()
            else:
                r.rssiMin = 0


    def _RSSI_Avg(self):
        for r in self:
            SensorData = self.env['smartsens.sensordata.stream'].search([('Launch_id', '=', r.id)])
            rssi = []
            for i in SensorData:
                rssi.append(i.RSSI)            
            if len(rssi) > 0:
                r.rssiAverage = sum(rssi)/ len(rssi)
                rssi.clear()
            else:
                r.rssiAverage = 0
            
           


    ####################### START -- NEW METHODE FOR IMPORT -- START ###############################
    ####################### START -- NEW METHODE FOR IMPORT -- START ###############################
    ####################### START -- NEW METHODE FOR IMPORT -- START ###############################
    def SyncCsvFileToDatabase(self):
        csv_sensor_recordstream = []
        csv_header = []
        sambaFilePaths = []
        HostDestinationFiles = []
        missingFiles = []
        # :TODO Get Name of the Folder from the Modul automatical and add it to the config 
        FileDestinationPath = r"C:\Program Files\Odoo14_Master\customAddons\SmartSenseAnalytics\models\Data\**\*"
        FileDestinationPath4Transfer = r"C:\Program Files\Odoo14_Master\customAddons\SmartSenseAnalytics\models\Data"
        sambaSourcePath = r'\\192.168.178.39\home\Workspace\Projects\SmartSenseAnalytics\SmartSensAnalytics\SmartSenseDataStore\**\*'
        
        def InsertHeader(row):
            #self.env.cr.savepoint() # set an Savepoint for rollbacks
            CsvIntegrity().checkHeader(row)
            try:
                " ['161', ' 2021-04-15 20:08', ' RheineTest', ' 41', ' 5', ' 131', ' 41', ' 0.997589', ' 0', ' 20936', ' 18.209', ' 50', ' 20', ' '] "
                # newDateTimeForOdoo = LaunchDateDateObjekt.strftime("%Y-%m-%d %H:%M:%S.%f") # ist mit Milliseconds
                # print("Das ist de Aufruf der InsertHeaderfunktion")
                TFM_ROW_ID = int(row[0])
                LaunchDate = row[1].split(" ")
                date = LaunchDate[1]
                Time = LaunchDate[-1]
                # print("Datetime aufgeteilt:",date,Time)
                # Generate Datetimobjects
                TimeObjekt = datetime.strptime(Time, "%H:%M")
                DateObjekt = datetime.strptime(date, "%Y-%m-%d")
                date = DateObjekt.strftime("%Y-%m-%d %H:%M:%S")
                # Time = TimeObjekt.strftime("%Y-%m-%d %H:%M:%S")
                Time = TimeObjekt.strftime("%Y-%m-%d %H:%M:%S")
                # Generate DatetimeStrings
                AirfieldName = row[2]
                AircraftType = row[3]
                TFM_FL_ID = row[4]  # FlugIDj
                #print(TFM_FL_ID)
                TFM_FMAX = row[5]  # Force Max
                #print(TFM_FMAX)
                TFM_WEIGHT = row[6]  # Gewicht Flugzeug
                #print("TFM_WEIGHT", TFM_WEIGHT)
                TFM_ACCELERATION = row[7]  # Accel
                #print(TFM_ACCELERATION)
                TFM_ALTITUDE = row[8]  # Flugplatzhöhe über dem Meeresboden
                TFM_TOTIMPULSE = row[9]  #
                #print(TFM_TOTIMPULSE)
                TFM_DURATION = row[10]
                TFM_PCKTSLOST = row[11]
                #print(TFM_PCKTSLOST)
                TFM_TEMPERATURE = row[12]
                #print(TFM_TEMPERATURE)

                # Tabellen die gebraucht werden
                recs = self.env['smartsense.analytics.airfield']
                launchRecs = self.env['smartsense.launche']
                winchtypes = self.env['smartsenese.winchtype']
                aircraftType = self.env['aircraft.type']

                # Searches die gebraucht werden um die Tabellenbeziehungen bei zu halten.
                launchRecsSearch = launchRecs.search([("TFM_RowID", "=", f'{TFM_ROW_ID}')])
                Airfieldsearch = recs.search([('name', '=', f'{AirfieldName}')])
                AircraftTypeSearch = aircraftType.search([('name', "=", f"{AircraftType}")])                
                aircraftTypeID = 0
                if Airfieldsearch.name is False:
                    Airfieldsearch.create({'name': AirfieldName, 'altitude': TFM_ALTITUDE})
                    self.env.cr.commit()
                    #self.env.cr.flush()
                    AirfieldID = Airfieldsearch.id
                else:
                    AirfieldID = Airfieldsearch.id
                    print("Die AirfieldID ist vorhanden:", AirfieldID)
                # wenn die der Record mit derTFM_Row id existiert muss diese nicht mehr erstellt werden
                if launchRecsSearch.TFM_RowID is False or launchRecsSearch.TFM_RowID is None or launchRecsSearch.TFM_RowID is 0:
                    # print("Die condition wird diese object angewendet:", launchRecsSearch.TFM_RowID)
                    launchRecsSearch.create({"TFM_RowID": TFM_ROW_ID,
                                            "forceMax": TFM_FMAX,
                                            "launchDate": date,
                                            "launchTime": Time,
                                            'airfield': AirfieldID,
                                            'packetsLost': TFM_PCKTSLOST,
                                            'temperatur': TFM_TEMPERATURE,
                                            'flightID': TFM_FL_ID,
                                            'acceleration': TFM_ACCELERATION,
                                            'totalImpulse': TFM_TOTIMPULSE,
                                            'duration': TFM_DURATION})
                    try:
                        print("Header commit")
                        self.env.cr.commit()                        
                        # self.env.cr.flush()
                        print("Header nach dem Commit")
                        # hier sollte man validieren was der letzte record ist
                        lastROWID = int(launchRecs.search([])[-1].id)
                        print("Erster lastROWID")
                        lastROWID2 = launchRecs.search([("TFM_RowID", "=", f'{TFM_ROW_ID}')])[-1].id
                        print("LastRowid2:",lastROWID2,"Old LastRowId:",lastROWID)
                    except:
                        print("Beim schreiben mit self.env.cr.commit() trat ein fehler auf")                        
                        # print(f"Die ID des letzten launches den du erstellt hast {int(launchRecs.search([])[-1].id)}")
                    return lastROWID
                else:
                    print("Der Launch existiert schon")

                if AircraftTypeSearch.name is False:
                    AircraftTypeSearch.create({"emptyAircraftWeight": TFM_WEIGHT, "name": AircraftType})
                    self.env.cr.commit()
                else:
                    aircraftTypeID = AircraftTypeSearch.id
                    # print("Die aircraftTypeID ist vorhanden:", AirfieldID)
            except:                
                return False
                

        

        def getFilesFromSourcePath():
            for f in glob.glob(sambaSourcePath, recursive=True):
                # print(f)
                sambaFilePaths.append(f)
                if os.path.isfile(f):
                    print(os.path.getmtime(f))
            return sambaFilePaths

        def getFilesFromDestinationPath():
            for f in glob.glob(FileDestinationPath, recursive=True):
                print(f)
                HostDestinationFiles.append(f)
            return HostDestinationFiles

        def getMissingSambaFiles():
            #all_samba_files = getFilesFromSourcePath()
            #print(all_samba_files)
            ftpCon = RemoteImport(username="web93f1",
                                  password="Smart#Sens#2020",
                                  host='server23.webgo24.de',
                                  work_dir="C:\Program Files\Odoo14_Master\customAddons\SmartSenseAnalytics\models\Data")
            ftp_file_names = ftpCon.ftpFileNames()
            #print("FTP_Files")
            #print(ftp_file_names)
            #print("FileDestinationPath")
            #print(FileDestinationPath)

            #import time
            #time.sleep(2.4)
            completed_file_names = []
            failed_file_names = []

            error_file_names = []
            FilesOnServer = glob.glob(FileDestinationPath, recursive=True)
            for path in FilesOnServer:
                if os.path.isfile(path):  # Nur auf die Files
                    #print("All ServerFiles:", os.path.getmtime(path), path)
                    if "_completed_" in path:
                        completed_file_names.append(path.rsplit("\\")[-1].rsplit("_completed_")[-2]+".txt")
                        print("Returned completed File too old Name",path.rsplit("\\")[-1].rsplit("_completed_")[-2]+".txt")
                    if "_failed_" in path:
                        failed_file_names.append(path.rsplit("\\")[-1].rsplit("_failed_")[-2]+".txt")
                        print("Returned failed File too old Name",path.rsplit("\\")[-1].rsplit("_failed_")[-2]+".txt")
                    #else:
                    #    error_file_names.append(path.rsplit("\\")[-1])
                    #    print("Error while convert too the old Name", path.rsplit("\\")[-1])
            reverted_file_names = list(set(failed_file_names)) + list(set(completed_file_names))
            print(len(list(set(failed_file_names))),len(list(set(completed_file_names))))
            print(reverted_file_names)
            # Match Serverfiles with the Remotepath

            #missing_samba_remote_files = []
            missing_ftp_remote_files = []


            #for samba_file in all_samba_files:
            #    if samba_file.rsplit("\\")[-1] not in reverted_file_names:
            #        missing_samba_remote_files.append(samba_file.rsplit("\\")[-1])
            #        print("Die Datei Fehlt", samba_file)
            #    else:
            #        print("Die Samba-File existiert schon auf dem Server",samba_file.rsplit("\\")[-1])

            for ftp_file in ftp_file_names:
                if ftp_file not in reverted_file_names:
                    missing_ftp_remote_files.append(ftp_file)
                    print("Die Datei Fehlt", ftp_file)
                else:
                    print("Die FTP-File existiert schon auf dem Server", ftp_file)

            missing_files_from_remotepaths = missing_ftp_remote_files #missing_samba_remote_files+


            #time.sleep(20.4)
            print("Alle nicht vorhanden Dateien")
            print(missing_files_from_remotepaths)
            print("Anzahl der Fehlenden Daten auf dem Localen Dateisystem:",len(missing_files_from_remotepaths))

            return missing_files_from_remotepaths, ftpCon, ftp_file_names, FilesOnServer
        
        def InsertTelemetryData(dataSet, LastrowID):            
            Lastrow_ID = LastrowID
            """ Dataset from the CSV-Files """
            "Dataset: ['10793', ' 37', ' 27542', ' 546', ' 0', ' 663', ' 384', ' 0', ' 0', ' ']"
            print(f"Es werden Sensordaten-Sätze geschrieben ANFANG! mit der LAST_ID:{Lastrow_ID}")
            for a in dataSet:
                TFD_ROW_ID = a[0]
                TFD_TFM_ID = a[1]
                TFD_TIME_MS = a[2]
                if int(TFD_TIME_MS) > 3000000000:
                    print(f"TFD_TIME_MS ist größer als 3000000000, es handel sich um die CSV TFD_TFM_ID{TFD_TFM_ID}")
                    return False
                TFD_CONS_FORCE = a[3]
                TFD_ERROR_STATE = a[4]
                TFD_TARGET_FORCE = a[5]
                TFD_ALTITUDE = a[6]
                TFD_RSSI = a[7]
                TFD_PCKTSLOST = a[8]
                sensordata_stream = self.env['smartsens.sensordata.stream']
                # Wenn diese Daten schon vorhanden sind müssen hier keine imports stat finden.
                # sensordata_streamSearch = sensordata_stream.search([("Launch_id","like",self.i)])

                if Lastrow_ID is not None:
                    try:
                        sensordata_stream.create({"Launch_id": Lastrow_ID,
                                                  "timeStamp": TFD_TIME_MS,
                                                  "Tfm_RowID": TFD_ROW_ID,
                                                  "TFM_ID": TFD_TFM_ID,
                                                  "Altitude": TFD_ALTITUDE,
                                                  "Cons_Force": TFD_CONS_FORCE,
                                                  "Error_State": TFD_ERROR_STATE,
                                                  "TargetForce": TFD_TARGET_FORCE,
                                                  "PacketsLost": TFD_PCKTSLOST,
                                                  "RSSI": TFD_RSSI})
                        self.env.cr.commit()
                    except ValueError as e:
                        raise
                else:
                    pass
                    #print("DataSet existiert schon:", Lastrow_ID)
            return True
        
        
        def InsertTelemetryData2(**kwargs):
            #print(kwargs)
            #{'line': 1, 'Lastrow_ID': 2355, 'TFD_ROW_ID': '198859', 'TFD_TFM_ID': ' 726', 'TFD_TIME_MS': ' 33190', 'TFD_CONS_FORCE': ' 362', 'TFD_ERROR_STATE': ' 8', 'TFD_TARGET_FORCE': ' 132', 'TFD_ALTITUDE': ' 425', 'TFD_RSSI': ' -85', 'TFD_PCKTSLOST': ' 78'}
            print(f"Data-row: {kwargs['line']} wurde geschrieben.. haha")
            CsvIntegrity.checkDataRow(self,dict_row=kwargs)
            # TFD_ROW_ID = a[0]
            # TFD_TFM_ID = a[1]
            # TFD_TIME_MS = a[2]
            # if int(TFD_TIME_MS) > 3000000000:
            #     print(f"TFD_TIME_MS ist größer als 3000000000, es handel sich um die CSV TFD_TFM_ID{TFD_TFM_ID}")
            #     return False
            # TFD_CONS_FORCE = a[3]
            # TFD_ERROR_STATE = a[4]
            # TFD_TARGET_FORCE = a[5]
            # TFD_ALTITUDE = a[6]
            # TFD_RSSI = a[7]
            # TFD_PCKTSLOST = a[8]
            
            
            

            

        def write_LocalCsvToDatabase(file):
            #for i in range(len(HostDestinationFiles)):
            #print("Die Datei wird geöffnet")            
            self.env.cr.savepoint()
            
            with open(file) as csv_file:
                csv_reader = csv.reader(csv_file, delimiter=';')
                line_count = 0
                lastRowID = 0
                csv_file = []
                for row in csv_reader:
                    if line_count == 0:
                        print("Das ist der Header:\n", row)
                        lastRowID = InsertHeader(row)
                        if lastRowID == False:
                            self.env.cr.rollback()
                            print("LastRow ID is False")
                            # wenn der Import des Header ein Error verursacht gibts ein Rollback und die  wird unterbrochen
                            # oder es muss eine Fehlerbeahndlung stat finden.
                            break
                        else:
                            line_count += 1               
                    else:
                        print("hier wird die Launch-ID weiter gereicht", lastRowID)
                        HeaderDateTime = row[1]                        
                        print("das ist der neue Importer row data:\n",row)
                        try:
                            InsertTelemetryData2(line=line_count,
                                                Lastrow_ID=lastRowID,
                                                TFD_ROW_ID = row[0],
                                                TFD_TFM_ID = row[1],
                                                TFD_TIME_MS = row[2],
                                                #if int(TFD_TIME_MS) > 3000000000:
                                                #    print(f"TFD_TIME_MS ist größer als 3000000000, es handel sich um die CSV TFD_TFM_ID{TFD_TFM_ID}")
                                                #    return False,
                                                TFD_CONS_FORCE = row[3],
                                                TFD_ERROR_STATE = row[4],
                                                TFD_TARGET_FORCE = row[5],
                                                TFD_ALTITUDE = row[6],
                                                TFD_RSSI = row[7],
                                                TFD_PCKTSLOST = row[8])
                            line_count += 1                                                        
                        except:
                            print(f"Data-row: {line_count}")
                            #csv_sensor_recordstream.append(row)
                            self.env.cr.rollback()
                    
                #print(csv_sensor_recordstream)                         
                #failedOrNot = InsertTelemetryData(dataSet=csv_sensor_recordstream, LastrowID=lastRowID)
                #csv_sensor_recordstream.clear()
                #if failedOrNot:
                #    print("Der import lief gut!")
                #    return True
                #else:
                #    print("CSV-File-import Fehler! ")
                #    return False
                
                
        def createSheduleActivity(**kwargs):
            try:
                launches =self.env("smartsense.launche")
                importShedule = self.env("smartsens.import_schedule_activity")
                importSheduleCount = len(importShedule)
                
                lastid = importShedule.create({"newFilesCount": kwargs["newfilesfound"],
                                    "countLaunchesBeforImport":importSheduleCount,
                                    "countLaunchesAfterImport": "",
                                    "filesCountOnRServer": kwargs["filesCountOnRServer"],
                                    "filesCountOnLServer": kwargs["FilesOnServer"],
                                    "status": "",
                                    "errorCode": "",
                                    "completed": "",
                                    "failed": "",
                                    "countAirfields":"",  
                                    "importActivity_id": ""}).id
                print("Versuch die letzte ID aus dem Create zu bekommen lastid")
            except:
                pass
                
        def main():
            all_samba_files, FTPcon, ftp_file_names, FilesOnServer = getMissingSambaFiles()# Alle Files-paths vom RemotePath ran holen
            print(FTPcon,all_samba_files)
            file_copy_counter = 0
            #print("FTPConObjekt wurde weiter gereicht")
            #print("All FTP-Files",ftp_file_paths)
            
            createSheduleActivity(newfilesfound=len(all_samba_files),
                                       filesCountOnRServer=len(ftp_file_names),
                                       FilesOnServer=len(FilesOnServer))
            
            
            while len(all_samba_files) != 0:
                file = all_samba_files.pop(0)
                print("Remote-File:",file)
                samba_file = sambaSourcePath.replace("**\*","")+ file
                #ftp_file = sambaSourcePath.replace("**\*","")+all_samba_files.pop(0)
                old_file_name = samba_file.rsplit("\\")[-1]
                data_path = FileDestinationPath4Transfer + "\\"
                data_path_completed = data_path + "completed" + "\\"
                data_path_failed = data_path + "failed" + "\\"
                now = datetime.now();dt_string = now.strftime("%d-%m-%Y_%H-%M-%S")
                completed_file_name = old_file_name.replace(".txt", "_" + "completed" + "_" + dt_string + ".txt")
                failed_file_name = old_file_name.replace(".txt", "_" + "failed" + "_" + dt_string + ".txt")
                try:
                    copy(samba_file, FileDestinationPath4Transfer)
                    print(samba_file.rsplit("\\")[-1], "\t wurde erfolgreich auf den Server kopiert!")
                    print(FileDestinationPath4Transfer+"\\"+ old_file_name)

                except FileNotFoundError:
                    FTPcon.copyFTP_file(filename=file)
                    print("file wurde vom FTP kopiert")
                    print("Beim Kopieren von:", old_file_name,"ist  ein Fehler aufgetreten!")
                file_copy_counter += 1

                if write_LocalCsvToDatabase(FileDestinationPath4Transfer + "\\" + old_file_name) == True:
                    for path in glob.glob(data_path+"*.txt", recursive=False):
                        if os.path.isfile(path): # Nur auf die Files
                            print("File moved too completed:",os.path.getmtime(path),path)
                            copy(path,data_path_completed+completed_file_name)
                            os.remove(path)
                else:
                    print("Fehler in den Daten entdeckt! Failed import")
                    for path in glob.glob(data_path+old_file_name, recursive=False):
                        if os.path.isfile(path): # Nur auf die Files
                            print("File moved too failed:",os.path.getmtime(path),path)
                            copy(path,data_path_failed+failed_file_name)
                            os.remove(path)
            print("FTP-Connection closing"); FTPcon.closeCon()
            print(f"Es wurden {file_copy_counter} kopiert")        
        main()



    # CountTimeStamp = fields.Integer(string="Counted TimeStamps",stored=True)
    # StreamID = fields.Char(string="Stream-ID", help="Jeder Stream hat seine eigene ID")

    #@api.depends("timeStamp")
    #def counttimeStamps(self):
    #    for r in self:
    #        print(r.CountTimeStamp)
    #        if not r.CountTimeStamp:
    #            r.CountTimeStamp = 0
    #        else:
                #r.CountTimeStamp = len(self.timeStamp)
    #            r.CountTimeStamp = 9000
    