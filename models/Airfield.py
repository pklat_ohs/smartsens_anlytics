from odoo import api, fields, models

class Airfield(models.Model):
    _name = "smartsense.analytics.airfield"
    _description = 'SmartSense Analytics Airfields'
    #airfieldName = fields.Char(string='Airfield Name',help="Wie heist das Airfield auf dem gestartet wird")
    name = fields.Char(string='Name',help="Wie heist das Airfield auf dem gestartet wird")
    altitude = fields.Float(string="Höhe des Airfields", help="Auf welchenr höhe befindet sich das Airfield")
    runwayDirection = fields.Float(string="Runway direction", help="In welche richtung zeigt das Startbahn")
    runWayLength = fields.Integer(string="Run Way Length", help="Wie lang ist die Startbahn", default=0)
    #Launch_id = fields. One2many("smartsense.launche", string="Launch-ID")



