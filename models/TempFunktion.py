def _saveEasyPlot(self):
    climbingRateY, tStampX = [3, 6, 2], [3, 7, 3]
    plt.plot(tStampX, climbingRateY)
    picObject = io.BytesIO()
    plt.savefig(picObject, format="png")
    picObject.seek(0)
    self.write({'plotClimbingRate': base64.encodebytes(picObject.getvalue())})


def _saveEasyPlot3(self):
    climbingRateY, tStampX = [3, 6, 2], [3, 7, 3]
    plt.plot(tStampX, climbingRateY)
    picObject = io.BytesIO()
    plt.savefig(picObject, format="png")
    picObject.seek(0)
    self.write({'plotClimbingRate3': base64.encodebytes(picObject.getvalue())})