from shutil import copyfile, copy, move
#from odoo_csv_tools.lib import mapper
#from odoo_csv_tools.lib.transform import Processor
from datetime import datetime,timedelta, date  # used to change the format of datetime field
# processor = Processor('client_file.csv', delimiter=';')
# from .DataImporterCSV import DataImporter
import psycopg2
import colorama
from colorama import Fore
from ..RemoteImporters import RemoteImport

class SyncCsvFileToDatabase(object):
        csv_sensor_recordstream = []
        csv_header = []
        sambaFilePaths = []
        HostDestinationFiles = []
        missingFiles = []
        # :TODO Get Name of the Folder from the Modul automatical and add it to the config 
        FileDestinationPath = r"C:\Program Files\Odoo14_Master\customAddons\SmartSenseAnalytics\models\Data\**\*"
        FileDestinationPath4Transfer = r"C:\Program Files\Odoo14_Master\customAddons\SmartSenseAnalytics\models\Data"
        sambaSourcePath = r'\\192.168.178.39\home\Workspace\Projects\SmartSenseAnalytics\SmartSensAnalytics\SmartSenseDataStore\**\*'
        
        def InsertHeader(self,row):
            " ['161', ' 2021-04-15 20:08', ' RheineTest', ' 41', ' 5', ' 131', ' 41', ' 0.997589', ' 0', ' 20936', ' 18.209', ' 50', ' 20', ' '] "
            # newDateTimeForOdoo = LaunchDateDateObjekt.strftime("%Y-%m-%d %H:%M:%S.%f") # ist mit Milliseconds
            # print("Das ist de Aufruf der InsertHeaderfunktion")
            TFM_ROW_ID = int(row[0])
            LaunchDate = row[1].split(" ")
            date = LaunchDate[1]
            Time = LaunchDate[-1]
            # print("Datetime aufgeteilt:",date,Time)
            # Generate Datetimobjects
            TimeObjekt = datetime.strptime(Time, "%H:%M")
            DateObjekt = datetime.strptime(date, "%Y-%m-%d")
            date = DateObjekt.strftime("%Y-%m-%d %H:%M:%S")
            # Time = TimeObjekt.strftime("%Y-%m-%d %H:%M:%S")
            Time = TimeObjekt.strftime("%Y-%m-%d %H:%M:%S")
            # Generate DatetimeStrings
            AirfieldName = row[2]
            AircraftType = row[3]
            TFM_FL_ID = row[4]  # FlugIDj
            #print(TFM_FL_ID)
            TFM_FMAX = row[5]  # Force Max
            #print(TFM_FMAX)
            TFM_WEIGHT = row[6]  # Gewicht Flugzeug
            #print("TFM_WEIGHT", TFM_WEIGHT)
            TFM_ACCELERATION = row[7]  # Accel
            #print(TFM_ACCELERATION)
            TFM_ALTITUDE = row[8]  # Flugplatzhöhe über dem Meeresboden
            TFM_TOTIMPULSE = row[9]  #
            #print(TFM_TOTIMPULSE)
            TFM_DURATION = row[10]
            TFM_PCKTSLOST = row[11]
            #print(TFM_PCKTSLOST)
            TFM_TEMPERATURE = row[12]
            #print(TFM_TEMPERATURE)

            # Tabellen die gebraucht werden
            recs = self.env['smartsense.analytics.airfield']
            launchRecs = self.env['smartsense.launche']
            winchtypes = self.env['smartsenese.winchtype']
            aircraftType = self.env['aircraft.type']

            # Searches die gebraucht werden um die Tabellenbeziehungen bei zu halten.
            print("es wird nach dem Launch gesucht")
            launchRecsSearch = launchRecs.search([("TFM_RowID", "=", f'{TFM_ROW_ID}')])
            print("Der FFM_RowID search:",launchRecsSearch)
            Airfieldsearch = recs.search([('name', '=', f'{AirfieldName}')])
            AircraftTypeSearch = aircraftType.search([('name', "=", f"{AircraftType}")])
            # Good to Know self.env.cr.rollback()
            aircraftTypeID = 0
            if Airfieldsearch.name is False:
                Airfieldsearch.create({'name': AirfieldName, 'altitude': TFM_ALTITUDE})
                self.env.cr.commit()
                AirfieldID = Airfieldsearch.id
            else:
                AirfieldID = Airfieldsearch.id
                # print("Die AirfieldID ist vorhanden:", AirfieldID)
            # wenn die der Record mit derTFM_Row id existiert muss diese nicht mehr erstellt werden
            if launchRecsSearch.TFM_RowID is False or launchRecsSearch.TFM_RowID is None or launchRecsSearch.TFM_RowID is 0:
                # print("Die condition wird diese object angewendet:", launchRecsSearch.TFM_RowID)
                launchRecsSearch.create({"TFM_RowID": TFM_ROW_ID,
                                         "forceMax": TFM_FMAX,
                                         "launchDate": date,
                                         "launchTime": Time,
                                         'airfield': AirfieldID,
                                         'packetsLost': TFM_PCKTSLOST,
                                         'temperatur': TFM_TEMPERATURE,
                                         'flightID': TFM_FL_ID,
                                         'acceleration': TFM_ACCELERATION,
                                         'totalImpulse': TFM_TOTIMPULSE,
                                         'duration': TFM_DURATION})
                self.env.cr.commit()
                lastROWID = int(launchRecs.search([])[-1].id)
                # print(f"Die ID des letzten launches den du erstellt hast {int(launchRecs.search([])[-1].id)}")
                return lastROWID
            else:
                print("Der Launch existiert schon")

            if AircraftTypeSearch.name is False:
                AircraftTypeSearch.create({"emptyAircraftWeight": TFM_WEIGHT, "name": AircraftType})
                self.env.cr.commit()
            else:
                aircraftTypeID = AircraftTypeSearch.id
                # print("Die aircraftTypeID ist vorhanden:", AirfieldID)

        def InsertTelemetryData(dataSet, LastrowID):
            Lastrow_ID = LastrowID
            """ Dataset from the CSV-Files """
            "Dataset: ['10793', ' 37', ' 27542', ' 546', ' 0', ' 663', ' 384', ' 0', ' 0', ' ']"
            print(f"Es werden Sensordaten-Sätze geschrieben ANFANG! mit der LAST_ID:{Lastrow_ID}")
            for a in dataSet:
                TFD_ROW_ID = a[0]
                TFD_TFM_ID = a[1]
                TFD_TIME_MS = a[2]
                if int(TFD_TIME_MS) > 3000000000:
                    print(f"TFD_TIME_MS ist größer als 3000000000, es handel sich um die CSV TFD_TFM_ID{TFD_TFM_ID}")
                    return False
                TFD_CONS_FORCE = a[3]
                TFD_ERROR_STATE = a[4]
                TFD_TARGET_FORCE = a[5]
                TFD_ALTITUDE = a[6]
                TFD_RSSI = a[7]
                TFD_PCKTSLOST = a[8]
                sensordata_stream = self.env['smartsens.sensordata.stream']
                # Wenn diese Daten schon vorhanden sind müssen hier keine imports stat finden.
                # sensordata_streamSearch = sensordata_stream.search([("Launch_id","like",self.i)])

                if Lastrow_ID is not None:
                    try:
                        sensordata_stream.create({"Launch_id": Lastrow_ID,
                                                  "timeStamp": TFD_TIME_MS,
                                                  "Tfm_RowID": TFD_ROW_ID,
                                                  "TFM_ID": TFD_TFM_ID,
                                                  "Altitude": TFD_ALTITUDE,
                                                  "Cons_Force": TFD_CONS_FORCE,
                                                  "Error_State": TFD_ERROR_STATE,
                                                  "TargetForce": TFD_TARGET_FORCE,
                                                  "PacketsLost": TFD_PCKTSLOST,
                                                  "RSSI": TFD_RSSI})
                        self.env.cr.commit()
                    except ValueError as e:            
                        
                        raise
                else:
                    pass
                    #print("DataSet existiert schon:", Lastrow_ID)
            return True

        def getFilesFromSourcePath():
            for f in glob.glob(sambaSourcePath, recursive=True):
                # print(f)
                sambaFilePaths.append(f)
                if os.path.isfile(f):
                    print(os.path.getmtime(f))
            return sambaFilePaths

        def getFilesFromDestinationPath():
            for f in glob.glob(FileDestinationPath, recursive=True):
                print(f)
                HostDestinationFiles.append(f)
            return HostDestinationFiles

        def getMissingSambaFiles():
            #all_samba_files = getFilesFromSourcePath()
            #print(all_samba_files)
            ftpCon = RemoteImport(username="web93f1",
                                  password="Smart#Sens#2020",
                                  host='server23.webgo24.de',
                                  work_dir="C:\Program Files\Odoo14_Master\customAddons\SmartSenseAnalytics\models\Data")
            ftp_file_names = ftpCon.ftpFileNames()
            print("FTP_Files")
            print(ftp_file_names)
            print("FileDestinationPath")
            print(FileDestinationPath)

            import time
            #time.sleep(2.4)
            completed_file_names = []
            failed_file_names = []

            error_file_names = []
            for path in glob.glob(FileDestinationPath, recursive=True):
                if os.path.isfile(path):  # Nur auf die Files
                    #print("All ServerFiles:", os.path.getmtime(path), path)
                    if "_completed_" in path:
                        completed_file_names.append(path.rsplit("\\")[-1].rsplit("_completed_")[-2]+".txt")
                        print("Returned completed File too old Name",path.rsplit("\\")[-1].rsplit("_completed_")[-2]+".txt")
                    if "_failed_" in path:
                        failed_file_names.append(path.rsplit("\\")[-1].rsplit("_failed_")[-2]+".txt")
                        print("Returned failed File too old Name",path.rsplit("\\")[-1].rsplit("_failed_")[-2]+".txt")
                    #else:
                    #    error_file_names.append(path.rsplit("\\")[-1])
                    #    print("Error while convert too the old Name", path.rsplit("\\")[-1])
            reverted_file_names = list(set(failed_file_names)) + list(set(completed_file_names))
            print(len(list(set(failed_file_names))),len(list(set(completed_file_names))))
            print(reverted_file_names)
            # Match Serverfiles with the Remotepath

            #missing_samba_remote_files = []
            missing_ftp_remote_files = []


            #for samba_file in all_samba_files:
            #    if samba_file.rsplit("\\")[-1] not in reverted_file_names:
            #        missing_samba_remote_files.append(samba_file.rsplit("\\")[-1])
            #        print("Die Datei Fehlt", samba_file)
            #    else:
            #        print("Die Samba-File existiert schon auf dem Server",samba_file.rsplit("\\")[-1])

            for ftp_file in ftp_file_names:
                if ftp_file not in reverted_file_names:
                    missing_ftp_remote_files.append(ftp_file)
                    print("Die Datei Fehlt", ftp_file)
                else:
                    print("Die FTP-File existiert schon auf dem Server", ftp_file)

            missing_files_from_remotepaths = missing_ftp_remote_files #missing_samba_remote_files+


            #time.sleep(20.4)
            print("Alle nicht vorhanden Dateien")
            print(missing_files_from_remotepaths)
            print("Anzahl der Fehlenden Daten auf dem Localen Dateisystem:",len(missing_files_from_remotepaths))

            return missing_files_from_remotepaths, ftpCon

        def write_LocalCsvToDatabase(file):
            #for i in range(len(HostDestinationFiles)):
            #print("Die Datei wird geöffnet")
            with open(file) as csv_file:
                csv_reader = csv.reader(csv_file, delimiter=';')
                line_count = 0
                for row in csv_reader:
                    if line_count == 0:
                        print("Das ist der Header:\n", row)
                        lastRowID = InsertHeader(row)
                        print("hier wird die ID weiter gereicht", lastRowID)
                        HeaderDateTime = row[1]
                        line_count += 1
                    else:
                        csv_sensor_recordstream.append(row)
                #print(csv_sensor_recordstream)
                failedOrNot = InsertTelemetryData(dataSet=csv_sensor_recordstream, LastrowID=lastRowID)
                csv_sensor_recordstream.clear()
                if failedOrNot:
                    print("Der import lief gut!")
                    return True
                else:
                    print("CSV-File-import Fehler! ")
                    return False

        def main():
            all_samba_files, FTPcon = getMissingSambaFiles()# Alle Files-paths vom RemotePath ran holen
            print(FTPcon,all_samba_files)
            file_copy_counter = 0
            print("FTPConObjekt wurde weiter gereicht")
            #print("All FTP-Files",ftp_file_paths)
            import time
            time.sleep(10)

            while len(all_samba_files) != 0:
                file = all_samba_files.pop(0)
                print("Remote-File:",file)
                samba_file = sambaSourcePath.replace("**\*","")+ file
                #ftp_file = sambaSourcePath.replace("**\*","")+all_samba_files.pop(0)
                old_file_name = samba_file.rsplit("\\")[-1]
                data_path = FileDestinationPath4Transfer + "\\"
                data_path_completed = data_path + "completed" + "\\"
                data_path_failed = data_path + "failed" + "\\"
                now = datetime.now();dt_string = now.strftime("%d-%m-%Y_%H-%M-%S")
                completed_file_name = old_file_name.replace(".txt", "_" + "completed" + "_" + dt_string + ".txt")
                failed_file_name = old_file_name.replace(".txt", "_" + "failed" + "_" + dt_string + ".txt")

                try:
                    copy(samba_file, FileDestinationPath4Transfer)
                    print(samba_file.rsplit("\\")[-1], "\t wurde erfolgreich auf den Server kopiert!")
                    print(FileDestinationPath4Transfer+"\\"+ old_file_name)

                except FileNotFoundError:
                    FTPcon.copyFTP_file(filename=file)
                    print("file wurde vom FTP kopiert")
                    print("Beim Kopieren von:", old_file_name,"ist  ein Fehler aufgetreten!")
                file_copy_counter += 1


                if write_LocalCsvToDatabase(FileDestinationPath4Transfer + "\\" + old_file_name) == True:
                    for path in glob.glob(data_path+"*.txt", recursive=False):
                        if os.path.isfile(path): # Nur auf die Files
                            print("File moved too completed:",os.path.getmtime(path),path)
                            copy(path,data_path_completed+completed_file_name)
                            os.remove(path)
                else:
                    print("Fehler in den Daten entdeckt! Failed import")
                    for path in glob.glob(data_path+old_file_name, recursive=False):
                        if os.path.isfile(path): # Nur auf die Files
                            print("File moved too failed:",os.path.getmtime(path),path)
                            copy(path,data_path_failed+failed_file_name)
                            os.remove(path)
            print("FTP-Connection closing"); FTPcon.closeCon()
            print(f"Es wurden {file_copy_counter} kopiert")        
        main()