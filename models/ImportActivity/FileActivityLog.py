from odoo import api, fields, models

class FileActivityLog(models.Model):
    _name = "smartsens.file_activity_log"
    _description = "File Activity Log"
    
    # name = fields.Char(name="Name",default="Airfieldname")  
    # modelbezeichnung = fields.Char(string="Model-bezeichnung")
    # tragweitenflaeche = fields.Float(string="Tragweiten-fläche")
    # aircraftype = fields.Many2one('aircraft.type', string='Winch Operator')
    # emptyAircraftWeight = fields.Float(string="Empty Aircraft weight", help="Gewicht des Flugzeugs ohne Besatzung Ladung")
    
    # id: PK,Unique,Integer # is set by default
    #importActivity_id = fields.One2many("smartsens.import_schedule_activity","id", string="Import Shedule Activity")
    #importActivity_id = fields.Many2one('smartsens.import_schedule_activity', string="Import-Activity ID")
    fileID = fields.Integer(string="File-ID",)
    filePathRemoteName: fields.Char(string="File Path on Remoteserver")
    filePathLocalName = fields.Char(string="File LocalPath")
    countDataFileLines = fields.Integer(string="How much Lines have the CSV-File")
    countDataDbLines = fields.Integer(string="How much Data-Lines on the Database")    
    status = fields.Selection([("Erfolgreich","completed"),
                               ("In Bearbeitung","in progress"),
                               ("Fehlgeschlagen","failed"),
                               ("Warnung","warning")])
    errorCode =  fields.One2many("smartsense.defined_import_errors","name",string="Error Code")
    csv_fileLog = fields.One2many("smartsense.csv_file_data_log","id")
    headerLog = fields.One2many("smartsens.csv_file_header_log","id",string="Header ID")
    