from odoo import api, fields, models

class DefinedImportErrors(models.Model):
    _name = "smartsense.defined_import_errors"
    _description = "Defined Import-Errors"
    
    # id:Unique,PK,Integer # by default
    errorCode = fields.Integer(string="Error-Code")
    name = fields.Char(name="Name",default="Name")
    bezeichnung = fields.Char(string="Bezeichnung", )
    definiton = fields.Char(string="Definition")
    
    # aircraftype = fields.Many2one('aircraft.type', string='Winch Operator')
    
    
