from odoo import api, fields, models

class CsvFileDataLog(models.Model):
    _name = "smartsense.csv_file_data_log"
    _description = "Csv-File Data-Logs"
    
    
    #'id:Integer:PK,Unique,Integer by default
    fileName = fields.Char(string="Filename")# related field to the file name
    dataLine = fields.Integer(string="Row Number")
    header = fields.Char(string="Header")
    errorCode = fields.One2many("smartsense.defined_import_errors","name")
    
