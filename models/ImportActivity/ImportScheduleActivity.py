from odoo import api, fields, models
from datetime import datetime

class ImportScheduleActivity(models.Model):
    _name = "smartsens.import_schedule_activity"
    _description = "Import_Activity_schedule"
    #_inherit = ["ir.cron"]
    
    
    activityDatetime = fields.Datetime(default=fields.Datetime.now,string="Datetime", help="Um welche Uhrzeit ist die Activity gestartet")
    newFilesCount = fields.Integer(help="")
    countLaunchesBeforImport = fields.Integer(help="")
    countLaunchesAfterImport = fields.Integer(help="")
    filesCountOnRServer = fields.Integer()
    filesCountOnLServer =  fields.Integer()
    status =  fields.Selection([("Erfolgreich","completed"),
                               ("In Bearbeitung","in progress"),
                               ("Fehlgeschlagen","failed"),
                               ("Warnung","warning")])
    errorCode: fields.One2many("smartsense.defined_import_errors","name")
    completed: fields.Boolean()
    failed = fields.Boolean()
    countAirfields= fields.Integer()
    importActivity_id = fields.One2many("smartsens.file_activity_log","id",string="ID_FileActivitylog")
    