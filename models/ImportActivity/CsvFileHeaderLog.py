from odoo import api, fields, models

class CsvFileHeaderLog(models.Model):
    _name = "smartsens.csv_file_header_log"
    _description = "CSV-File Header-Log"
    
    name = fields.Char(name="Name",default="Header Data")
