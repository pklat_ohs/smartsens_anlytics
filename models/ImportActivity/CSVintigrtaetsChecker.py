from ast import Pass, Raise
import csv, glob,os
from turtle import bgcolor
import sys
import logging
import threading #import Thread
import time
from .FileIntegrety import ErrorChecks
import logging
from datetime import datetime

class FTPServerFiles:
    """_summary_
    Eine Classe um Die FileSourcepaths aller Files zurück zurückzugeben.
    """
        
    def __init__(self,sourcePath = "C:\Program Files\Odoo14_Master\customAddons\SmartSenseAnalytics\models\Data\**"):
        self.sPath = sourcePath
        self.sPathFileList = []        
    
    def getAllFiles(self):
        """_summary_
        erstelle eine liste aus alles SourceFilePaths
        
        Returns:
            _type_: _description_
        """
        file_counter =0
        for path in glob.glob(self.sPath, recursive=True):
            if os.path.isfile(path):  # Nur auf die Files
                #print(path)
                self.sPathFileList.append(path)
                file_counter +=1
        #print(f"Es wurden {file_counter} gefunden")
        return  self.sPathFileList

class CsvIntegrity(ErrorChecks):
    def __init__(self, dPath="",headerCountPos=13, dataSollPositionenAnzahl=9):        
        self.headerSollPositionenAnzahl = headerCountPos+1        
        self.dataSollPositionenAnzahl = dataSollPositionenAnzahl        
        self.sPathFileList = []      
        self.dPath = dPath
        self.timeStampGAPCheck = 0
        self.Tagging = {"empty":"no data in the cell",
                        "NegativeValue":"NegativeValue",
                        "I/O error":"Es kann nicht gelsen oder geschrieben werden"
                        }
        self.logs = {"file":{"filename":"","filepath":""},
                     "error":{"header":[],
                              "row":[]}}        
        self.header_definition = ["TFM_ROW_ID:integer primary key autoincrement not null",
                                  "TFM_DATETIME:text not null",
                                  "TFM_AIRFIELD:text",
                                  "TFM_AIRCRAFT_TYPE:text",
                                  "TFM_FL_ID:integer",
                                  "TFM_FMAX:integer",
                                  "TFM_WEIGHT:integer",
                                  "TFM_ACCELERATION:real",
                                  "TFM_ALTITUDE:integer",
                                  "TFM_TOTIMPULSE:integer",
                                  "TFM_DURATION:integer",
                                  "TFM_PCKTSLOST:integer",
                                  "TFM_TEMPERATURE:real"]
        
        self.data_definiton = ["TFD_ROW_ID:integer primary key autoincrement not null, ",
                                "TFD_TFM_ID:integer not null",
                                "TFD_TIME_MS:integer",
                                "TFD_CONS_FORCE:integer",
                                "TFD_ERROR_STATE:integer",
                                "TFD_TARGET_FORCE:integer",
                                "TFD_ALTITUDE:integer",
                                "TFD_RSSI:integer",
                                "TFD_PCKTSLOST:integer"]
        
        """_summary_
        Ab hier kommen nur Daten die für die Logs verwendet werden!
        
        """
        # Metadata
        self.logFilePath = ""
        #self.Log 
        
        # Headerdata
        self.headerPositionsCountCheck = False
        self.logRemoveFirstEmptySpaceCheck = False
        # Rowdata
        
        """"""        

    def checkHeader(self,row):
        """_summary_
        Hier werden Headerchecks ausgeführt um den Header als Komplettet anzusehn
        Args:
            row (_type_): List            
                            TFM_ROW_ID:integer primary key autoincrement not null",
                            "TFM_DATETIME:text not null",
                            "TFM_AIRFIELD:text",
                            "TFM_AIRCRAFT_TYPE:text",                            
                            "TFM_FL_ID:integer",
                            "TFM_FMAX:integer",
                            "TFM_WEIGHT:integer",
                            "TFM_ACCELERATION:real",
                            "TFM_ALTITUDE:integer",
                            "TFM_TOTIMPULSE:integer",
                            "TFM_DURATION:integer",
                            "TFM_PCKTSLOST:integer",
                            "TFM_TEMPERATURE:real"]
        """        
        if ErrorChecks.check_HeaderPositionsCount(self, header_row=row) == True:
            print(bcolors.OKGREEN+"Header Positionen Count OK "+str(len(row)-1)+bcolors.ENDC)
        else:
            print("Header Positionen Count Failed, die aktuelle länge ist", str(len(row)))
        
        # Entferne aus jeder Celle die erste Empty Space
        row  = ErrorChecks.deleteFirstSpace(self,row)
        if row != None :
            print(bcolors.OKGREEN+"Entfernen des ersten Leerzeichens aus denn Datensätzen war erfolgreich"+bcolors.ENDC)
        else:
            print(bcolors.FAIL+"Das löschen des ersten Empty Space hat nicht funktioniert"+bcolors.ENDC)
            
            
        # Check alle Value im Header Consistens
        #
        if ErrorChecks.checkTFM_ROW_ID(self,headerTFM_ROW_ID_cell=row[0]) == True:
            print(bcolors.OKGREEN+"Check TFM_ROW_ID is OK"+bcolors.ENDC)
        else:
            print(bcolors.FAIL+"die TFM_ROW-ID ist nicht ok"+bcolors.ENDC)
        
            
        if ErrorChecks.checkTFM_DATETIME(self, cell=row[1]) == True:
            print(bcolors.OKGREEN+"Check TFM_DATETIME is OK"+bcolors.ENDC)
        else:
            print(bcolors.FAIL+"Mit dem Timestamp stimmt etwas nicht"+bcolors.ENDC)
            
        if ErrorChecks.checkTFM_AIRFIELD(self, cell=row[2]) == True:
            print(bcolors.OKGREEN+"checkTFM_AIRFIELD is OK"+bcolors.ENDC)
        else:
            print(bcolors.FAIL+"Mit dem Airfield stimmt etwas nicht"+row[2]+bcolors.ENDC)
            
        if ErrorChecks.checkTFM_AIRCRAFT_TYPE(self, cell=row[3]) == True:
            print(bcolors.OKGREEN+"checkTFM_AIRCRAFT_TYPE is OK"+bcolors.ENDC)
        else:
            print(bcolors.FAIL+"Mit dem AIRCRAFT_TYPE stimmt etwas nicht"+row[3]+bcolors.ENDC)
        
        if ErrorChecks.checkTFM_FL_ID(self, cell=row[4]) == True:
            print(bcolors.OKGREEN+"checkTFM_FL_ID is OK"+bcolors.ENDC)
        else:
            print(bcolors.FAIL+"Mit dem checkTFM_FL_ID stimmt etwas nicht"+row[3]+bcolors.ENDC)
            
        if ErrorChecks.checkTFM_FMAX(self, cell=row[5]) == True:
            print(bcolors.OKGREEN+"checkTFM_FMAX is OK"+bcolors.ENDC)
        else:
            print(bcolors.FAIL+"Mit dem checkTFM_FMAX stimmt etwas nicht"+row[3]+bcolors.ENDC)
            
        if ErrorChecks.checkTFM_WEIGHT(self, cell=row[6]) == True:
            print(bcolors.OKGREEN+"checkTFM_WEIGHT is OK"+bcolors.ENDC)
        else:
            print(bcolors.FAIL+"Mit dem checkTFM_WEIGHT stimmt etwas nicht"+row[3]+bcolors.ENDC)
        
        if ErrorChecks.checkTFM_ACCELERATION(self, cell=row[7]) == True:
            print(bcolors.OKGREEN+"checkTFM_ACCELERATION is OK"+bcolors.ENDC)
        else:
            print(bcolors.FAIL+"Mit dem checkTFM_ACCELERATION stimmt etwas nicht"+row[3]+bcolors.ENDC)
            
        if ErrorChecks.checkTFM_ALTITUDE(self, cell=row[8]) == True:
            print(bcolors.OKGREEN+"checkTFM_ALTITUDE is OK"+bcolors.ENDC)
        else:
            print(bcolors.FAIL+"Mit dem checkTFM_ALTITUDE stimmt etwas nicht"+row[3]+bcolors.ENDC)
            
        if ErrorChecks.checkTFM_TOTIMPULSE(self, cell=row[9]) == True:
            print(bcolors.OKGREEN+"checkTFM_TOTIMPULSE is OK"+bcolors.ENDC)
        else:
            print(bcolors.FAIL+"Mit dem checkTFM_TOTIMPULSE stimmt etwas nicht"+row[3]+bcolors.ENDC)
        
        if ErrorChecks.checkTFM_DURATION(self, cell=row[10]) == True:
            print(bcolors.OKGREEN+"checkTFM_DURATION is OK"+bcolors.ENDC)
        else:
            print(bcolors.FAIL+"Mit dem checkTFM_DURATION stimmt etwas nicht"+row[3]+bcolors.ENDC)
        
        if ErrorChecks.checkTFM_PCKTSLOST(self, cell=row[11]) == True:
            print(bcolors.OKGREEN+"checkTFM_PCKTSLOST is OK"+bcolors.ENDC)
        else:
            print(bcolors.FAIL+"Mit dem checkTFM_PCKTSLOST stimmt etwas nicht"+row[3]+bcolors.ENDC)
        
        if ErrorChecks.checkTFM_TEMPERATURE(self, cell=row[12]) == True:
            print(bcolors.OKGREEN+"checkTFM_TEMPERATURE is OK"+bcolors.ENDC)
        else:
            print(bcolors.FAIL+"Mit dem checkTFM_TEMPERATURE stimmt etwas nicht"+row[3]+bcolors.ENDC)
            
    
    def check_GAP_DataTFD_TIME_MS(self,row,actuallyLine:int, msTDM_TIM):
        
        if self.timeStampGAPCheck == 0:
            print("GAP is starting")
        else:         
            pass
    
            
    #def checkDataRow(self:object, row:list,actuallyLine:int) -> bool:
    def checkDataRow(self:object, dict_row:dict):
        #print(row) # ['116996', ' 461', ' 128', ' 361', ' 0', ' 300', ' -4', ' 0', ' 0', ' ']
        #productive
        #{'line': 1, 'Lastrow_ID': 2355, 'TFD_ROW_ID': '198859', 'TFD_TFM_ID': ' 726', 'TFD_TIME_MS': ' 33190', 'TFD_CONS_FORCE': ' 362', 'TFD_ERROR_STATE': ' 8', 'TFD_TARGET_FORCE': ' 132', 'TFD_ALTITUDE': ' 425', 'TFD_RSSI': ' -85', 'TFD_PCKTSLOST': ' 78'}
        from datetime import time
        #time.sleep(5)
        
        #for cell in row:    
        if ErrorChecks.checkDataTFD_ROW_ID(self, dict_row["TFD_ROW_ID"]) == True:
            print(bcolors.OKGREEN+"checkDataTFD_ROW_ID OK"+str(dict_row["TFD_ROW_ID"])+bcolors.ENDC)
            pass
        else:
            print(bcolors.FAIL+"checkDataTFD_ROW_ID Failed:"+str(dict_row["TFD_ROW_ID"])+bcolors.ENDC)

        if ErrorChecks.checkDataTFD_TFM_ID(self, dict_row["TFD_TFM_ID"]) == True:
            print(bcolors.OKGREEN+"checkDataTFD_TFM_ID OK"+str(dict_row["TFD_TFM_ID"])+bcolors.ENDC)
            pass
        else:
            print(bcolors.FAIL+"checkDataTFD_TFM_ID Failed:"+str(dict_row["TFD_TFM_ID"])+bcolors.ENDC)

        if ErrorChecks.checkDataTFD_TIME_MS(self, dict_row["TFD_TIME_MS"]) == True:
            print(bcolors.OKGREEN+"checkDataTFD_TIME_MS OK"+str(dict_row["TFD_TIME_MS"])+bcolors.ENDC)
            pass
        else:
            print(bcolors.FAIL+"checkDataTFD_TIME_MS Failed:"+str(dict_row["TFD_TIME_MS"])+bcolors.ENDC)

        if ErrorChecks.checkDataTFD_CONS_FORCE(self, dict_row["TFD_CONS_FORCE"]) == True:
            print(bcolors.OKGREEN+"checkDataTFD_CONS_FORCE OK"+str(dict_row["TFD_CONS_FORCE"])+bcolors.ENDC)
            pass
        else:
            print(bcolors.FAIL+"checkDataTFD_CONS_FORCE Failed:"+str(dict_row["TFD_CONS_FORCE"])+bcolors.ENDC)

        if ErrorChecks.checkDataTFD_ERROR_STATE(self, dict_row["TFD_ERROR_STATE"]) == True:
            print(bcolors.OKGREEN+"checkDataTFD_ERROR_STATE OK"+str(dict_row["TFD_ERROR_STATE"])+bcolors.ENDC)
            pass
        else:
            print(bcolors.FAIL+"checkDataTFD_ERROR_STATE Failed:"+str(dict_row["TFD_ERROR_STATE"])+bcolors.ENDC)

        if ErrorChecks.checkDataTFD_TARGET_FORCE(self, dict_row["TARGET_FORCE"]) == True:
            print(bcolors.OKGREEN+"checkDataTFD_TARGET_FORCE OK"+str(dict_row["TARGET_FORCE"])+bcolors.ENDC)
            pass
        else:
            print(bcolors.FAIL+"checkDataTFD_TARGET_FORCE Failed:"+str(dict_row["TFD_TARGET_FORCE"])+bcolors.ENDC)

        if ErrorChecks.checkDataTFD_ALTITUDE(self, dict_row["TFD_ALTITUDE"]) == True:
            print(bcolors.OKGREEN+"checkDataTFD_ALTITUDE OK"+str(dict_row["TFD_ALTITUDE"])+bcolors.ENDC)
            pass
        else:
            print(bcolors.FAIL+"checkDataTFD_ALTITUDE Failed:"+str(dict_row["TFD_ALTITUDE"])+bcolors.ENDC)

        if ErrorChecks.checkDataTFD_RSSI(self, dict_row["TFD_RSSI"]) == True:
            print(bcolors.OKGREEN+"checkDataTFD_RSSI OK"+str(dict_row["TFD_RSSI"])+bcolors.ENDC)
            pass
        else:
            print(bcolors.FAIL+"checkDataTFD_RSSI Failed:"+str(dict_row["TFD_RSSI"])+bcolors.ENDC)

        if ErrorChecks.checkDataTFD_PCKTSLOST(self, dict_row["TFD_PCKTSLOST"]) == True:
            print(bcolors.OKGREEN+"checkDataTFD_PCKTSLOST OK"+str(dict_row["TFD_PCKTSLOST"])+bcolors.ENDC)
            pass
        else:
            print(bcolors.FAIL+"checkDataTFD_PCKTSLOST Failed:"+str(dict_row["TFD_PCKTSLOST"])+bcolors.ENDC)
                
            
    def checkFile(self,filePath):
        print(filePath)
        print("Das ist der type Paths:",type(filePath))        
              
        if isinstance(filePath,list):
            print("Is instance")
            for Pathfile in filePath:
                try:
                    with open(Pathfile) as csv_file:
                            csv_reader = csv.reader(csv_file, delimiter=';')
                            line_count = 0
                            self.logFilePath = Pathfile # schreibe in die Log das momentanen File.
                            for row in csv_reader:
                                if line_count == 0:                            
                                    self.checkHeader(row=row)                            
                                    HeaderDateTime = row[1]
                                    line_count += 1
                                else:
                                    self.checkDataRow(row=row,actuallyLine=line_count)
                                    #print("Es ist eine Fehler beim auslesen der Line aufgetaucht")                                
                            
                except IOError as e:
                    errno, strerror = e.args
                    print("I/O error({0}): {1}".format(errno,strerror))
                    # e can be printed directly without using .args:
                    # print(e)
                except ValueError:
                    print("No valid integer in line.")
                except:
                    print("Unexpected error:", sys.exc_info()[0])
                    Raise
        else:        
            """_summary_
            Versucht eine File zu öffen wenn das Fehl schlägt wird dies in der Log fest gehalten

            Args:
                filePath (_type_): String
            """
            
            
            
            try:
                with open(filePath) as csv_file:
                        csv_reader = csv.reader(csv_file, delimiter=';')
                        line_count = 0
                        self.logFilePath = filePath # schreibe in die Log das momentanen File.
                        for row in csv_reader:
                            if line_count == 0:                            
                                self.checkHeader(row=row)                            
                                HeaderDateTime = row[1]
                                line_count += 1
                            else:
                                self.checkDataRow(row=row,actuallyLine=line_count)
                                #print("Es ist eine Fehler beim auslesen der Line aufgetaucht")
                                
                            
            except IOError as e:
                errno, strerror = e.args
                print("I/O error({0}): {1}".format(errno,strerror))
                # e can be printed directly without using .args:
                # print(e)
            except ValueError:
                print("No valid integer in line.")
            except:
                print("Unexpected error:", sys.exc_info()[0])
                raise
        
            
    def __del__(self):
        print("der Thread wurde geschlossen")
        
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
        
if __name__ == "__main__":
    #sourcePath = "C:\Program Files\Odoo14_Master\customAddons\SmartSenseAnalytics\models\Data\**"      
    """_summary_
    Initialisieren diesen mit den gewünschenten Soll-paramenter um eine Ist-Test (Check) auszuführen.
    CsvIntegrity()    
    """    
    CheckCSV = CsvIntegrity(headerCountPos=13,
                            dataSollPositionenAnzahl=9
                            )
    # Get all Files from the SourcePath    
    
    # Liste mit alle Paths erstellen 
    filesSourcePathslist = FTPServerFiles().getAllFiles()
    chunked_list = list()
    chunk_size = 10
    for i in range(0, len(filesSourcePathslist), chunk_size):
        chunked_list.append(filesSourcePathslist[i:i+chunk_size])    
    
    #for list in chunked_list:
    #    for file in list:
    #        CheckCSV.checkFile(filePath=file)

    
    # https://realpython.com/intro-to-python-threading/#working-with-many-threads
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%H:%M:%S")
    threads = list()
    for index in range(len(chunked_list)):
        logging.info("Main    : create and start thread %d.", index)
        #x = threading.Thread(target=thread_function, args=(index,))
        x = threading.Thread(target=CheckCSV.checkFile, args=(chunked_list[index],))
        threads.append(x)
        x.start()

    for index, thread in enumerate(threads):
        logging.info("Main    : before joining thread %d.", index)
        thread.join()
        logging.info("Main    : thread %d done", index)
        
        