import datetime

from numpy import datetime_as_string

class ErrorChecks(object):   
    def data_Type(self):
        pass       
    
    def checkIsNull(self:object,cell):
        pass

    def checkIntOrNot(self:object,cell):
        if int(cell) is not int:
            print("Die Celle kann nicht zum Int gecastet werden")
        
    def GAPCheck(self:object,DateTimeCell):
        # Datetime GAPcheck
        pass        
            
    def checkTFM_DATETIME(self:object,cell):        
        #TimeObjekt = datetime.strptime(Time, "%H:%M")
        cellDateObjekt = datetime.datetime.strptime(cell, "%Y-%m-%d %H:%M")
        #print(str(cellDateObjekt)) 
        #print(type(cellDateObjekt)) 
        try:
            if isinstance(cellDateObjekt,datetime.datetime):
                return True
        except:
            print("checkTFM_DATETIME konnte kein DatetimeObjekt erzeugen")
            return False
    
    def checkTFM_AIRFIELD(self:object,cell):
        try:
            if len(cell) >0:
                print(cell)
                return True
        except:
            print(cell)
            print("checkTFM_AIRFIELD konnte kein String erkennen")
            return False
        
    def checkTFM_AIRCRAFT_TYPE(self:object,cell:str) -> bool :
        # https://de.wikipedia.org/wiki/Liste_der_Flugzeugtypencodes
        
        try:
            if len(cell) > 2:
                print(bcolors.OKGREEN+"Es is eine Flugzeug angegeben"+bcolors.ENDC)
                return True
            else:
                print(bcolors.WARNING+"Warning es ist kein Flugzeugtype angegeben!:{cell}"+bcolors.ENDC)
                return True
                
        except:
            print("checkTFM_AIRCRAFT_TYPE irgendwas ist in die Hose gegangen",cell)
            return False
        
        
    def checkTFM_FL_ID(self:object,cell:str) -> bool:
        try:
            if isinstance(int(cell),int):
                return True
        except:
            print(str(__name__)+"mit dem Wert stimmt etwas nicht")
            return False
    
    def checkTFM_FMAX(self:object,cell:str) -> bool:
        try:
            if isinstance(int(cell),int):
                return True
        except:
            print(str(__name__)+"mit dem Wert stimmt etwas nicht")
            return False
        
    def checkTFM_WEIGHT(self:object,cell:str) -> bool:
        try:
            if isinstance(int(cell),int):
                return True
        except:
            print(str(__name__)+"mit dem Wert stimmt etwas nicht")
            return False
    def checkTFM_ACCELERATION(self:object,cell:str) -> bool:
        try:
            if isinstance(float(cell),float):
                return True
        except:
            print(str(__name__)+"mit dem Wert stimmt etwas nicht")
            return False
        
    def checkTFM_ALTITUDE(self:object,cell:str) -> bool:
        try:
            if isinstance(int(cell),int):
                return True
        except:
            print(str(__name__)+"mit dem Wert stimmt etwas nicht")
            return False
        
    def checkTFM_TOTIMPULSE(self:object,cell:str) -> bool:
        try:
            if isinstance(int(cell),int):
                return True
        except:
            print(str(__name__)+"mit dem Wert stimmt etwas nicht")
            return False

            
    def checkTFM_DURATION(self:object,cell:str) -> bool:
        try:
            if isinstance(float(cell),float):
                return True
        except:
            print(str(__name__)+"mit dem Wert stimmt etwas nicht")
            return False
        
    def checkTFM_PCKTSLOST(self:object,cell:str) -> bool:
        try:
            if isinstance(int(cell),int):
                return True
        except:
            print(str(__name__)+"mit dem Wert stimmt etwas nicht")
            return False
        
    def checkTFM_TEMPERATURE(self:object,cell:str) -> str:
        try:
            if isinstance(float(cell),float):
                return True
        except:
            print(str(__name__)+"mit dem Wert stimmt etwas nicht")
     
    
    def check_HeaderPositionsCount(self:object,header_row):
        
        """_summary_
        Check the Count of the list, if its ok than print is ok
        Args:
            header_row (_type_): list

        Returns:
            _type_: list
        """        
        if len(header_row) == self.headerSollPositionenAnzahl:
            self.headerPositionsCountCheck = True
            return True
        else:
            self.headerPositionsCountCheck = False
            return False    
    
    def checkTFM_ROW_ID(self:object,headerTFM_ROW_ID_cell):
        header_data = []      
        try:
            if isinstance(int(headerTFM_ROW_ID_cell),int):
                return True
        except ValueError:
            print("ValueError ",headerTFM_ROW_ID_cell)
            return False
    
    # Hier kommen die Methoden die dazu da sind, die DataRow aus den CSV-Files zu checken
    # Hier kommen die Methoden die dazu da sind, die DataRow aus den CSV-Files zu checken
    # Hier kommen die Methoden die dazu da sind, die DataRow aus den CSV-Files zu checken

    def checkDataTFD_ROW_ID(self:object,cell) -> bool:
        count = cell        
        try:
            if isinstance(int(cell),int):                
                return True
            elif int(cell) < 0:
                print(bcolors.FAIL+"checkDataTFD_ROW_ID is Negative"+str(cell)+bcolors.ENDC)
                return False
            else:
                print(bcolors.WARNING+"checkDataTFD_ROW_ID Is Not Instance of Type Int Warning"+str(cell)+bcolors.ENDC)
        except:            
            return False

    def checkDataTFD_TFM_ID(self:object,cell) -> bool:
        try:
            if isinstance(int(cell),int):                
                return True
            else:
                print(bcolors.WARNING+"checkDataTFD_TFM_ID Is Not Instance of Type Int Warning"+str(cell)+bcolors.ENDC)
        except:            
            return False

    def checkDataTFD_TIME_MS(self:object,cell) -> bool:
        try:
            if isinstance(int(cell),int):                
                return True
            else:
                print(bcolors.WARNING+"checkDataTFD_TIME_MS Is Not Instance of Type Int Warning"+str(cell)+bcolors.ENDC)
        except:            
            return False

    def checkDataTFD_CONS_FORCE(self:object,cell) -> bool:
        try:
            if isinstance(int(cell),int):                
                return True
            else:
                print(bcolors.WARNING+"checkDataTFD_CONS_FORCE Is Not Instance of Type Int Warning"+str(cell)+bcolors.ENDC)
        except:            
            return False

    def checkDataTFD_ERROR_STATE(self:object,cell) -> bool:
        try:
            if isinstance(int(cell),int):                
                return True
            else:
                print(bcolors.WARNING+"checkDataTFD_ERROR_STATE Is Not Instance of Type Int Warning"+str(cell)+bcolors.ENDC)
        except:            
            return False

    def checkDataTFD_TARGET_FORCE(self:object,cell) -> bool:
        try:
            if isinstance(int(cell),int):                
                return True
            else:
                print(bcolors.WARNING+"checkDataTFD_TARGET_FORCE Is Not Instance of Type Int Warning"+str(cell)+bcolors.ENDC)
        except:            
            return False

    def checkDataTFD_ALTITUDE(self:object,cell) -> bool:
        try:
            if isinstance(int(cell),int):                
                return True
            else:
                print(bcolors.WARNING+"checkDataTFD_ALTITUDE Is Not Instance of Type Int Warning"+str(cell)+bcolors.ENDC)
        except:            
            return False

    def checkDataTFD_RSSI(self:object,cell) -> bool:
        try:
            if isinstance(int(cell),int):                
                return True
            else:
                print(bcolors.WARNING+"checkDataTFD_RSSI Is Not Instance ofWType arn ing Int"+str(cell)+bcolors.ENDC)
        except:            
            return False

    def checkDataTFD_PCKTSLOST(self:object,cell) -> bool:
        try:
            if isinstance(int(cell),int):                
                return True
            else:
                print(bcolors.WARNING+"checkDataTFD_PCKTSLOST Is Not Instance of Type Int Warning"+str(cell)+bcolors.ENDC)
        except:            
            return False        
    
    def deleteFirstSpace(self:object,row):
        """_summary_
        Get a list with strings and delete omly the first Letter it its aa Space. 
        Args:
            row (_type_): list
        Returns:
            _type_: list
        """
        rowWithoutSpace = []
        #print(bcolors.OKCYAN+str(row)+bcolors.ENDC)
        try:
            for i in row:               
                #print("Der Value aus dem Header sind momentan so aus",i)           
                ii= i.replace(" ","",1)            
                #print("Und momentan sieht der Value so aus",ii)
                rowWithoutSpace.append(ii)                
            #print(bcolors.OKCYAN+str(rowWithoutSpace)+bcolors.ENDC)
            self.logRemoveFirstEmptySpaceCheck = True 
            return rowWithoutSpace        
        except:
            """{"file":{"filename":"","filepath":""},
                     "error":{"header":[],
                              "row":[]}}    """
            print("Es ist ein Error aufgetaucht bei abscheiden der erstel Empty Space in der DataFrame")
            #self.logs["error"]["header"].append()
            self.logRemoveFirstEmptySpaceCheck = False
            return None

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'